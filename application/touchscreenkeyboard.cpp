/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include "touchscreenkeyboard.h"

//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

TouchscreenKeyboard::TouchscreenKeyboard()
  :  mPressedKeys()
  , mSoundActive(128,false)
  , mToggleMode(false)
  , mActive(false)
{
}


//-----------------------------------------------------------------------------
//             SLOT: Receive an event from the touchscreen keyboard
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief SLOT: Receive an event from the touchscreen keyboard
/// \details The Qml layer calls this slot as soon as the status of the
/// keyboard changes. This function finds out which of the keys have been
/// pressed or released.
/// \param keys : The list of keys (in Midi norm) which are currently pressed.
///////////////////////////////////////////////////////////////////////////////

void TouchscreenKeyboard::receiveTouchpadKeyboardEvent (QVariant keys)
{
    QList<QVariant> list = keys.toList();
    QSet<int> actualKeys;
    for (QVariant var : list) actualKeys.insert(var.toInt());

    // Find released keys (take care of the loop over a shrinking set)
    bool entriesToDelete;
    do
    {
        entriesToDelete = false;
        for (int key : mPressedKeys) if (not actualKeys.contains(key))
        {
            if (not mToggleMode) switchNote(key,false);
            mPressedKeys.remove(key);
            entriesToDelete=true;
            break;
        }
    }
    while (entriesToDelete);

    // Find newly pressed keys
    for (int key : actualKeys) if (not mPressedKeys.contains(key))
    {
        if (mToggleMode) switchNote(key,not mSoundActive[key]);
        else switchNote(key,true);
        mPressedKeys.insert(key);
    }
}


//-----------------------------------------------------------------------------
//                            Switch note on or off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Switch note on or off.
/// \param key : Number of the key according to the Midi standard.
/// \param on : True if note is turned on, false if it is switched off.
///////////////////////////////////////////////////////////////////////////////

void TouchscreenKeyboard::switchNote(int key, bool on)
{
    // Remember that the key is pressed or released
    mSoundActive[key] = on;

    // Send a signal to the Qml layer for highlighting pressed keys
    highlightKey(key,on);

    // Send a Midi event (note on / off) at constant value;
    const qint8 volume = 90;
    if (mActive) emit sendTouchpadMidiEvent(
                QMidiMessage((on ? 0x90 : 0x80), (key&0x7f), volume, 0.0));
}


//-----------------------------------------------------------------------------
//                                  Clear
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Clear the keyboard memory and turn all notes off.
///////////////////////////////////////////////////////////////////////////////

void TouchscreenKeyboard::clear()
{
    // Erase memory
    mSoundActive.fill(false);

    // Turn all notes off
    for (quint8 command = 0xB0U; command < 0xC0U; command++)
        sendTouchpadMidiEvent (QMidiMessage(command, 123, 0, 0.0));
    for (int i=0; i<128; ++i) highlightKey(i,false);
}
