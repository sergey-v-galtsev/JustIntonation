<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="95"/>
        <source>Theoretical Physics III</source>
        <translation>Theoretische Physik III</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="96"/>
        <source>Faculty for Physics and Astronomy</source>
        <translation>Fakultät für Physik und Astronomie</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>University of</source>
        <translation>Universität</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>, Germany</source>
        <translation>, Deutschland</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="117"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="165"/>
        <source>is free software licensed under GNU GPLv3. The source code can be downloaded from Gitlab:</source>
        <translation>ist freie Software, die unter GNU GPLv3 lizensiert ist. Der Quelltext kann bei Gitlab heruntergeladen werden:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="208"/>
        <source>This software is based on the following open-source third-party components:</source>
        <translation>Die Software basiert auf folgenden quelloffenen Komponenten von Drittanbietern:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="275"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Wir möchten denjenigen danken, die zum Projekt beigetragen haben:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="278"/>
        <source>Translations: </source>
        <translation>Übersetzungen: </translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../application.cpp" line="538"/>
        <source>Grand Piano</source>
        <translation>Konzertflügel</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="539"/>
        <source>Organ</source>
        <translation>Orgel</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="540"/>
        <source>Harpsichord</source>
        <translation>Cembalo</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="541"/>
        <source>Unknown Instrument</source>
        <translation>Unbekanntes Instrument</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="543"/>
        <source>Artificial sound</source>
        <translation>Künstlicher Klang</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="544"/>
        <source>External device</source>
        <translation>Externes Gerät</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="812"/>
        <source>[Choose output device]</source>
        <translation>[Gerät wählen]</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="858"/>
        <source>No device connected</source>
        <translation>Kein Gerät angeschlossen</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../qml/AudioOutput.qml" line="76"/>
        <source>Sample rate:</source>
        <translation>Abtastrate:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="105"/>
        <source>Buffer size:</source>
        <translation>Puffergröße:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="120"/>
        <source>Show more help on audio settings</source>
        <translation>Weitere Hilfe zu den Audio-Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="148"/>
        <source>Packet size:</source>
        <translation>Paketgröße:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="179"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="182"/>
        <source>Reset audio settings to default values</source>
        <translation>Audioeinstellungen auf Standardwerte zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="189"/>
        <source>Apply Changes</source>
        <translation>Änderungen anwenden</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="190"/>
        <source>Apply the audio settings specified in this panel</source>
        <translation>Anwendung der in diesem Abschnitt gewähleten Audioeinstellungen</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="95"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
</context>
<context>
    <name>Examples</name>
    <message>
        <location filename="../qml/Examples.qml" line="45"/>
        <source>Example </source>
        <translation>Beispiel </translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="85"/>
        <source>Play previous MIDI example</source>
        <translation>Vorheriges MIDI-Beispiel starten</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="95"/>
        <source>Show copyright information about the MIDI file</source>
        <translation>Informationen zu den Urheberrechten der MIDI-Beispiele</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="104"/>
        <source>Play next MIDI example</source>
        <translation>Nächstes Midi-Beispiel starten</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="92"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
</context>
<context>
    <name>Expert</name>
    <message>
        <location filename="../qml/Expert.qml" line="111"/>
        <source>Expert Mode</source>
        <translation>Expertenmodus</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="85"/>
        <source>Go back to Simple Mode</source>
        <translation>Zurück zum einfachen Modus</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="89"/>
        <source>Help and General Information</source>
        <translation>Hilfe und Allgemeine Informationen</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="94"/>
        <source>Modify Preferences</source>
        <translation>Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="99"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Musik auf dem Touchscreen spielen</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="104"/>
        <source>Show Logfile</source>
        <translation>Zeige Log-Datei</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="168"/>
        <source>Midi Player</source>
        <translation>Midi-Player</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="180"/>
        <source>Midi Examples</source>
        <translation>Midi-Beispiele</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="193"/>
        <source>Instrument</source>
        <translation>Instrument</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="231"/>
        <source>Frequency:</source>
        <translation>Frequenz:</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="267"/>
        <source>Reset concert pitch to the standard value of 440 Hz</source>
        <translation>Kammerton A auf 440 Hz zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="304"/>
        <source>Tuning Mode</source>
        <translation>Stimmverfahren</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="314"/>
        <source>Temperament</source>
        <translation>Temperament</translation>
    </message>
</context>
<context>
    <name>FileHandler</name>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Please choose a Midi file</source>
        <translation>Wählen Sie eine Midi-Datei</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Midi files (*.mid *.midi *.MID)</source>
        <translation>Midi-Dateien (*.mid *.midi *.MID)</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="80"/>
        <source>Save custom temperament</source>
        <translation>Speichere benutzerdefiniertes Temperament</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>Temperament files</source>
        <translation>Temperament-Dateien</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="89"/>
        <source>Could not write to disk.</source>
        <translation>Datei konnte nicht gespeichert werden.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="115"/>
        <source>Load custom temperament</source>
        <translation>Lade benutzerdefiniertes Temperament</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="123"/>
        <source>Could not read from disk.</source>
        <translation>Die Datei kann nicht gelesen werden.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>Reading Error</source>
        <translation>Lesefehler</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>This is not a data file generated by JustIntonation</source>
        <translation>Dies ist offenbar keine von JustIntonation erzeugte Datei</translation>
    </message>
</context>
<context>
    <name>GettingStarted</name>
    <message>
        <location filename="../qml/GettingStarted.qml" line="28"/>
        <source>Getting started</source>
        <translation>Erste Schritte</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="63"/>
        <source>Welcome to &quot;Just Intonation&quot;.</source>
        <translation>Willkommen bei &quot;Just Intonation&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="72"/>
        <source>With this application you can experience how it would be to hear and play Music in just intonation, independent of scale!</source>
        <translation>Mit dieser Anwendung können Sie erleben, wie es wäre, Musik in reiner Intonation zu hören oder zu spielen, und zwar unabhängig von der Tonart!</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="73"/>
        <source>System Requirements</source>
        <translation>Systemvoraussetzungen</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="74"/>
        <source>The application runs on most desktop computers and on sufficiently powerful tablets and smartphones.</source>
        <translation>Die Anwendung läuft auf PCs und Apple-Rechnern sowie auf ausreichend leistungsfähigen Tablets und Smartphones.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="75"/>
        <source>It needs about 300MB of temporary memory (RAM).</source>
        <translation>Das Programm benötigt etwa 300MB temporären Speicherplatz (RAM).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="76"/>
        <source>In addition, if you want to download samples of realistic musical instruments, approximately 600MB of permanent storage (disk space) is needed.</source>
        <translation>Wenn Sie Klänge echter Musikinstrumente herunterladen wollen, werden zusätzlich ca. 600 MB permanenter Speicherplatz (Festplatte) benötigt.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="77"/>
        <source>Download Realistic Instruments</source>
        <translation>Realistische Instrumente herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="78"/>
        <source>On startup the application will ask you for permission to download samples of real instruments from our website.</source>
        <translation>Beim Start werden Sie um Erlaubnis gebeten, Klangdateien realistischer Instrumente von unserem Server herunterladen zu dürfen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="79"/>
        <source>Depending on the speed of your internet connection this may take some time.</source>
        <translation>Je nach Geschwindigkeit Ihrer Internetverbindung kann das einige Zeit in Anspruch nehmen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="80"/>
        <source>During the download you can already start to use the application.</source>
        <translation>Während des Downloads können Sie bereits die Anwendung benutzen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="81"/>
        <source>Listen to Music</source>
        <translation>Musik hören</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="82"/>
        <source>To hear music just tap the leftmost button with the note in the lower toolbar.</source>
        <translation>Um Musik zu hören tippen Sie auf den linken Knopf mit der Note lin der unteren Werkzeugleiste.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="83"/>
        <source>Toggle between just intonation and equal temperament and listen to the difference.</source>
        <translation>Schalten Sie zwischen reiner und gleichstufiger Stimmung um, um den Unterschied zu hören.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="84"/>
        <source>Listen to your own Music</source>
        <translation>Hören Sie Ihre eigene Musik</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="85"/>
        <source>Connect a Midi keyboard to your device.</source>
        <translation>Schließen Sie ein Midi-Keyboard an ihrem Rechner oder Mobilgerät an.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="86"/>
        <source>The application will recognize most Midi devices automatically.</source>
        <translation>In der Regel wird die Anwendung das Midi-Gerät automatisch erkennen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="87"/>
        <source>Just play on your keyboard and hear yourself playing in just intonation.</source>
        <translation>Spielen Sie einfach auf dem Keyboard und hören Sie sich selbst in reiner Intonation.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="88"/>
        <source>Midi keyboards can also be connected to mobile devices using a special adapter cable (see user manual).</source>
        <translation>Sie können auch Ihr Handy oder Tablet mit einem speziellen Adapter an ein Midi-Keyboard anschließen (siehe Benutzerhandbuch).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="89"/>
        <source>Expert Mode</source>
        <translation>Expertenmodus</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="90"/>
        <source>Tap the rightmost button in the lower toolbar to enter the Expert Mode.</source>
        <translation>Tippen Sie auf den rechten Knopf in der unteren Werkzeugleiste, um zum Expertenmodus zu gelangen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="91"/>
        <source>Here you can choose various temperaments.</source>
        <translation>Hier können Sie verschiedene Temperamente auswählen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="92"/>
        <source>Moreover, you can control a large variety of parameters.</source>
        <translation>Darüber hinaus können Sie verschiedene Parameter einstellen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="93"/>
        <source>It is also possible to monitor the pitch drift while you are playing.</source>
        <translation>Ebenso können Sie die Tonhöhenabweichung während des Spielens beobachten.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="94"/>
        <source>More Information</source>
        <translation>Weitere Informationen</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="95"/>
        <source>You want to learn more about Just Intonation and the history of temperaments?</source>
        <translation>Sie möchten mehr über reine Intonation und die historische Entwicklung von Temperamenten wissen?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="96"/>
        <source>You would like to know more about the features of the application and how it works internally?</source>
        <translation>Sie würden gerne mehr über die Funktionen der Anwendung und die interne Arbeitsweise erfahren?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="97"/>
        <source>Please download the User Manual and visit our website for more detailed information.</source>
        <translation>Lesen Sie das Benutzerhandbuch und besuchen Sie unsere Internetseite für weitere Informationen.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="98"/>
        <source>Thank you for your interest in Just Intonation.</source>
        <translation>Vielen Dank für Ihr Interesse.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="112"/>
        <source>Download User Manual</source>
        <translation>Benutzerhandbuch</translation>
    </message>
</context>
<context>
    <name>Instrument</name>
    <message>
        <location filename="../../instrument/instrument.cpp" line="80"/>
        <source>Generating artificial sound.</source>
        <translation>Erzeuge künstliche Töne.</translation>
    </message>
    <message>
        <location filename="../../instrument/instrument.cpp" line="96"/>
        <source>Loading instrument. Please wait...</source>
        <translation>Instrument wird geladen...</translation>
    </message>
</context>
<context>
    <name>Keyboard</name>
    <message>
        <location filename="../qml/Keyboard.qml" line="70"/>
        <source>Keyboard</source>
        <translation>Klaviatur</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="151"/>
        <source>Mouse</source>
        <translation>Maus</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="152"/>
        <source>Touchscreen</source>
        <translation>Touchscreen</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="163"/>
        <source>Single tone</source>
        <translation>Einzelton</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="164"/>
        <source>Triad</source>
        <translation>Dreiklang</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="134"/>
        <source>Equal</source>
        <translation>gleichstufig</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Just</source>
        <translation>rein</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Nonequal</source>
        <translation>Wahltemperament</translation>
    </message>
</context>
<context>
    <name>LearnMore</name>
    <message>
        <location filename="../qml/LearnMore.qml" line="29"/>
        <source>Learn more</source>
        <translation>Mehr erfahren</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="51"/>
        <source>Basic Help for Getting Started</source>
        <translation>Hilfe und erste Schritte</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="52"/>
        <source>Getting started</source>
        <translation>Erste Schritte</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="60"/>
        <source>Download User Manual from the Internet</source>
        <translation>Benutzerhandbuch vom Internet herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="61"/>
        <source>Download manual</source>
        <translation>Benutzerhandbuch</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="69"/>
        <source>Visit Project Website on the Internet</source>
        <translation>Internetauftritt des Projekts besuchen</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="71"/>
        <source>Visit website</source>
        <translation>Internetseite</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="79"/>
        <source>Download Scientific Documentation describing Details of the Tuning Method</source>
        <translation>Wissenschaftliche Dokumentation mit Details zum Stimmverfahren herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="81"/>
        <source>Tuning Method</source>
        <translation>Stimmverfahren</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="90"/>
        <source>Get more Information about this Application</source>
        <translation>Weitere Informationen über diese Anwendung</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="91"/>
        <source>About this App</source>
        <translation>Über diese App</translation>
    </message>
</context>
<context>
    <name>LogFile</name>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>A#</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>B</source>
        <translation>H</translation>
    </message>
</context>
<context>
    <name>Logfile</name>
    <message>
        <location filename="../qml/Logfile.qml" line="29"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="32"/>
        <source>Clear log file</source>
        <translation>Log-Datei löschen</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="38"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="39"/>
        <source>Copy log file to the clipboard</source>
        <translation>Kopiere die gesamte Log-Datei in die Zwischenablage</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="48"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Zurück an den Anfang</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="55"/>
        <source>Play / Pause</source>
        <translation>Play / Pause</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="66"/>
        <source>Close Logfile</source>
        <translation>Schließe Log-Datei</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="70"/>
        <source>Logfile</source>
        <translation>Log-Datei</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="55"/>
        <source>https://www.gnu.org/licenses/gpl.html</source>
        <translation>https://www.gnu.org/licenses/gpl-3.0.de.html</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="58"/>
        <source>http://www.just-intonation.org</source>
        <translation>http://www.just-intonation.org/de</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="170"/>
        <source>manual_en.pdf</source>
        <translation>manual_de.pdf</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="257"/>
        <source>Equal Temperament</source>
        <translation>Gleichstufige Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="258"/>
        <source>Just Intonation</source>
        <translation>Reine Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="260"/>
        <source>1/4 Meantone</source>
        <translation>1/4 mitteltönig</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="259"/>
        <source>Pythagorean tuning</source>
        <translation>Pythagoreische Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="261"/>
        <source>Werckmeister III</source>
        <translation>Werckmeister III</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="262"/>
        <source>Silbermann 1/6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="263"/>
        <source>Zarlino -2/7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="264"/>
        <source>User-defined</source>
        <translation>Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="392"/>
        <location filename="../qml/Main.qml" line="407"/>
        <source>Inactive</source>
        <translation>Inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="413"/>
        <source>[Choose Audio Device]</source>
        <translation>[Audiogerät wählen]</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="415"/>
        <source>[Choose]</source>
        <translation>[Wählen]</translation>
    </message>
</context>
<context>
    <name>Midi</name>
    <message>
        <location filename="../../midi.cpp" line="415"/>
        <location filename="../../midi.cpp" line="449"/>
        <location filename="../../midi.cpp" line="505"/>
        <source>Inactive</source>
        <translation>Inaktiv</translation>
    </message>
    <message>
        <location filename="../../midi.cpp" line="506"/>
        <source>[Choose Midi device]</source>
        <translation>[Midi-Gerät wählen]</translation>
    </message>
</context>
<context>
    <name>MidiInput</name>
    <message>
        <location filename="../qml/MidiInput.qml" line="69"/>
        <source> Automatic Mode</source>
        <translation> Automatischer Modus</translation>
    </message>
    <message>
        <location filename="../qml/MidiInput.qml" line="97"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>Im automatischen Modus werden neue Midi-Geräte automatisch erkannt</translation>
    </message>
</context>
<context>
    <name>MidiOutput</name>
    <message>
        <location filename="../qml/MidiOutput.qml" line="48"/>
        <source>Channel:</source>
        <translation>Kanal:</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="97"/>
        <source> Automatic Mode</source>
        <translation> Automatischer Modus</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="126"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>Im automatischen Modus werden neue Midi-Geräte automatisch erkannt</translation>
    </message>
</context>
<context>
    <name>MidiPlayer</name>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="109"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Zurück an den Anfang</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="117"/>
        <source>Play / Pause</source>
        <translation>Play / Pause</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="127"/>
        <source>Repeat mode</source>
        <translation>Wiederholungsmodus</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="135"/>
        <source>Open a Midi file</source>
        <translation>Midi-Datei öffnen</translation>
    </message>
</context>
<context>
    <name>NavigationManager</name>
    <message>
        <location filename="../qml/NavigationManager.qml" line="152"/>
        <source>Reset settings</source>
        <translation>Einstellungen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="153"/>
        <source>Do you want to reset all settings?</source>
        <translation>Wollen Sie die Einstellungen zurücksetzen?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="154"/>
        <source>All changes that you made will be lost and the settings will be reset to default values.</source>
        <translation>Alle Änderungen, die Sie bislang vorgenommen haben, gehen verloren und werden auf Standardwerte zurückgesetzt.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="164"/>
        <source>Help on choosing the audio parameters</source>
        <translation>Hilfe bei der Wahl der Audio-Parameter</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The sample rate has to be 44100.</source>
        <translation>Die Abtastrate muss 44100 Hz betragen.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The buffer size should be small.</source>
        <translation>Die Puffergröße sollte möglichst klein sein.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="166"/>
        <source>Buffer size = 0 means to use the default value. Packet size and buffer size should be the same. The buffer size controls the size of the internal audio buffer of your device. If you hear crackling sounds this value should be increased. Try to find the lowest possible value. The packet size controls the number of frames generated by the application in a single packet. In case of latency problems try to reduce this value.</source>
        <translation>Die Puffergröße stellt die Größe des internen Audiopuffers Ihres Geräts ein. Wenn der Ton ruckelt oder nicht funktioniert, sollten Sie diesen Wert erhöhen, ansonsten sollte der Wert so klein wie möglich gewählt werden. Die Paketgröße gibt an, wie viele Frames von der Anwendung maximal in einem Paket geliefert werden. Normalerweise sollte die Paketgröße der Puffergröße entsprechen. Bei Problemen mit der Latenz (Verzögerung beim Anschlagen der Tasten) kann man aber versuchen, diesen Wert zu reduzieren.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="173"/>
        <source>Confirm Download</source>
        <translation>Download bestätigen</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="174"/>
        <source>Would you like to hear realistic musical instruments?</source>
        <translation>Möchten Sie realistische Musikinstrumente hören?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="175"/>
        <source>In this case the application will silently download 600 MB of sampled sounds from the internet to your device.</source>
        <translation>Dafür müsste die Anwendung etwa 600MB Klangdateien vom Internet herunterladen.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="176"/>
        <source>If you allow the application to download samples from our server you can hear realistic instruments such as a piano, an organ, or a harpsichord. The samples are downloaded only once and saved locally. Please make sure that your device offers about 600 MB of free disk space. If you deny internet access, the application will only provide artificially generated sounds (triangular waves).</source>
        <translation>Wenn Sie der Anwendung gestatten, Samples von unserem Server herunterzuladen, können Sie realistische Instrumente wie z.B. ein Klavier, eine Orgel oder ein Cembalo hören. Die Samples werden nur einmal heruntergeladen und lokal gespeichert. Bitte vergewissern Sie sich voher, dass Ihr Gerät über mindestens 600 MB freien Speicherplatz verfügt. Wenn Sie den Internetzugriff ablehnen, wird die Anwendung lediglich künstliche Klänge zur Verfügung (Dreieckswellen) stellen.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="187"/>
        <source>No Internet Connection</source>
        <translation>Keine Internetverbindung</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="188"/>
        <source>Could not find a valid internet connection for downloading.</source>
        <translation>Keine funktionierende Internetverbindung für den Download gefunden.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="189"/>
        <source>Please use a fast internet connection (WLAN or wired).</source>
        <translation>Benutzen Sie bitte eine schnelle Internetverbindung (WLAN oder Kabel).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="190"/>
        <source>Retry</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="191"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/Preferences.qml" line="35"/>
        <source>Preferences</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="86"/>
        <source>External Midi Keyboard</source>
        <translation>Externes Midi-Keyboard</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="119"/>
        <source>External Midi Output Device</source>
        <translation>Externe Midi-Ausgabe</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="126"/>
        <source>System settings</source>
        <translation>Systemeinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="96"/>
        <source>Audio Output Device</source>
        <translation>Audio-Ausgang</translation>
    </message>
</context>
<context>
    <name>ProgressIndicator</name>
    <message>
        <location filename="../qml/ProgressIndicator.qml" line="132"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
</context>
<context>
    <name>RunGuard</name>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="168"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="169"/>
        <source>Just Intonation is already running.</source>
        <translation>&quot;Just Intonation&quot; wird bereits ausgeführt.</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="170"/>
        <source>Please close the application before restarting.</source>
        <translation>Bitte schließen Sie die Anwendung vor dem Neustart.</translation>
    </message>
</context>
<context>
    <name>Simple</name>
    <message>
        <location filename="../qml/Simple.qml" line="60"/>
        <source>Play a Midi demo or drop a Midi file here</source>
        <translation>Hören Sie ein Midi-Beispiel oder legen Sie eine Midi-Datei hier ab</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="86"/>
        <source>Enter Expert Mode</source>
        <translation>Zum Expertenmodus</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="143"/>
        <source>Instrument:</source>
        <translation>Instrument:</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="198"/>
        <source>Just Intonation</source>
        <translation>Reine Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="317"/>
        <source>Downloading Instruments</source>
        <translation>Instrumente herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="374"/>
        <source>Tap this button to hear a demo</source>
        <translation>Hier klicken für eine Demo</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="197"/>
        <source>Equal Temperament</source>
        <translation>Gleichstufige Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="76"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Musik auf dem Touchscreen spielen</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="81"/>
        <source>Modify Preferences</source>
        <translation>Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="71"/>
        <source>Help and General Information</source>
        <translation>Hilfe und Allgemeine Informationen</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="212"/>
        <source>Delayed Tuning</source>
        <translation>Verzögertes Stimmen</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="234"/>
        <source>Compensate Pitch Drift</source>
        <translation>Tonhöhendriftausgleich</translation>
    </message>
</context>
<context>
    <name>SystemSettings</name>
    <message>
        <location filename="../qml/SystemSettings.qml" line="40"/>
        <source>Welcome screen on startup</source>
        <translation>Begrüßung beim Start</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="48"/>
        <source>Allow download of samples</source>
        <translation>Sample-Download erlauben</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="61"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="102"/>
        <source>Restore default settings</source>
        <translation>Einstellungen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="103"/>
        <source>Reset all settings and restart the application</source>
        <translation>Alle Einstellungen auf Standardwerte zurücksetzen und neu starten</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="80"/>
        <source>Forced instrument download</source>
        <translation>Instrumente neu herunterladen</translation>
    </message>
</context>
<context>
    <name>Temperament</name>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Semitone</source>
        <translation>Halbton</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Major Second</source>
        <translation>Große Sekunde</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Minor Third</source>
        <translation>Kleine Terz</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Major Third</source>
        <translation>Große Terz</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Perfect Fourth</source>
        <translation>Quarte</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Tritone</source>
        <translation>Tritonus</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Perfect Fifth</source>
        <translation>Quinte</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Minor Sixth</source>
        <translation>Kleine Sexte</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Major Sixth</source>
        <translation>Große Sexte</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Minor Seventh</source>
        <translation>Kleine Septime</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Major Seventh</source>
        <translation>Große Septime</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Octave</source>
        <translation>Oktave</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="70"/>
        <source>Static Tuning</source>
        <translation>Statische Stimmung</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>A#</source>
        <translatorcomment>A# muss nur in deutscher Sprache übersetzt werden.</translatorcomment>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>B</source>
        <translatorcomment>B muss nur in deutscher Sprache als H übersetzt werden</translatorcomment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="106"/>
        <source>Subminor Seventh 7:4</source>
        <translation>Naturseptime 7:4</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="166"/>
        <source>Enter temperament name</source>
        <translation>Name des Temperaments</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="258"/>
        <source>Reset weights</source>
        <translation>Gewichte Reset</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="187"/>
        <source>Open a temperament file</source>
        <translation>Öffne Temperament-Datei</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="206"/>
        <source>Save a temperament file</source>
        <translation>Speichere Temperament-Datei</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="241"/>
        <source>Set all interval size deviations to zero (equal temperament)</source>
        <translation>Alle Intervallgrößenabweichungen auf Null setzen (gleichstufige Stimmung)</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="259"/>
        <source>Restore recommended standard values for the weights</source>
        <translation>Standardwerte für die Gewichtsfaktoren wiederherstellen</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="288"/>
        <source>Interval:</source>
        <translation>Intervall:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="296"/>
        <source>Deviation:</source>
        <translation>Abweichung:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="306"/>
        <source>Weight:</source>
        <translation>Gewicht:</translation>
    </message>
</context>
<context>
    <name>TextHeader</name>
    <message>
        <location filename="../qml/TextHeader.qml" line="108"/>
        <source>Modify Preferences</source>
        <translation>Einstellungen ändern</translation>
    </message>
    <message>
        <location filename="../qml/TextHeader.qml" line="120"/>
        <source>Close window</source>
        <translation>Fenster schließen</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../modules/system/translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
</context>
<context>
    <name>TuningMode</name>
    <message>
        <location filename="../qml/TuningMode.qml" line="43"/>
        <source>Tuning delay</source>
        <translation>Stimmverzögerung</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="78"/>
        <source>Memory</source>
        <translation>Zeitkorrelation</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="115"/>
        <source>Drift correction</source>
        <translation>Driftausgleich</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="215"/>
        <source>cent</source>
        <translation>Cent</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="279"/>
        <source>tempered</source>
        <translation>temperiert</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="289"/>
        <source>just</source>
        <translation>rein</translation>
    </message>
</context>
<context>
    <name>Voice</name>
    <message>
        <location filename="../../instrument/voice.cpp" line="64"/>
        <source>Please wait...</source>
        <translation>Bitte warten...</translation>
    </message>
    <message>
        <location filename="../../instrument/voice.cpp" line="149"/>
        <source>Reading scale</source>
        <translation>Töne einlesen</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../qml/Welcome.qml" line="52"/>
        <source>Hello!</source>
        <translation>Willkommen!</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="64"/>
        <source>This application gives you the opportunity to play in just intonation independent of scale.</source>
        <translation>Mit dieser Anwendung können Sie Musik in reiner Stimmung hören und spielen - unabhängig von der Tonart.</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="85"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="86"/>
        <source>Start Just Intonation</source>
        <translation>Anwendung starten</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="97"/>
        <source>Learn more</source>
        <translation>Mehr erfahren</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="98"/>
        <source>Visit our website to learn more about Just Intonation</source>
        <translation>Besuchen Sie unsere Webseite, um mehr zu erfahren</translation>
    </message>
</context>
</TS>
