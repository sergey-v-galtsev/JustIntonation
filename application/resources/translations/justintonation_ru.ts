<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="95"/>
        <source>Theoretical Physics III</source>
        <translation>Теоретическая физика III</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="96"/>
        <source>Faculty for Physics and Astronomy</source>
        <translation>Факультет физики и астрономии</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>University of</source>
        <translation>Университет Вюрцбурга</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>, Germany</source>
        <translation>, Германия</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="117"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="165"/>
        <source>is free software licensed under GNU GPLv3. The source code can be downloaded from Gitlab:</source>
        <translation>является бесплатным программным обеспечением, лицензированным под GNU GPLv3. Исходный код можно скачать с Gitlab:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="208"/>
        <source>This software is based on the following open-source third-party components:</source>
        <translation>Это программное обеспечение основанно на следующих Open Source-компонентах третьих лиц:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="275"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Мы благодарим всех, кто участвовал в разработке этого проекта:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="278"/>
        <source>Translations: </source>
        <translation>Переводы: </translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../application.cpp" line="538"/>
        <source>Grand Piano</source>
        <translation>Рояль</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="539"/>
        <source>Organ</source>
        <translation>Орган</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="540"/>
        <source>Harpsichord</source>
        <translation>Клавесин</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="541"/>
        <source>Unknown Instrument</source>
        <translation>Неизвестный инструмент</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="543"/>
        <source>Artificial sound</source>
        <translatorcomment>Звук воспроизведённый компьютером /Искусственный звук </translatorcomment>
        <translation>Искусственный</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="544"/>
        <source>External device</source>
        <translation>Внешнее устройство</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="812"/>
        <source>[Choose output device]</source>
        <translation>[Выберите устройство вывода]</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="858"/>
        <source>No device connected</source>
        <translation>Нет подключенных устройств</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../qml/AudioOutput.qml" line="76"/>
        <source>Sample rate:</source>
        <translation>Частота дискретизации:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="105"/>
        <source>Buffer size:</source>
        <translation>Размер буфера:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="120"/>
        <source>Show more help on audio settings</source>
        <translation>Показать больше справочной информации о настройках звука</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="148"/>
        <source>Packet size:</source>
        <translation>Размер пакета:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="179"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="182"/>
        <source>Reset audio settings to default values</source>
        <translation>Сброс настроек звука значения по умолчанию</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="189"/>
        <source>Apply Changes</source>
        <translation>Применить изменения</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="190"/>
        <source>Apply the audio settings specified in this panel</source>
        <translation>Активировать настройки звука, которые выбраны выше</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>Yes</source>
        <translation>да</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="95"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>Examples</name>
    <message>
        <location filename="../qml/Examples.qml" line="45"/>
        <source>Example </source>
        <translation>Пример </translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="85"/>
        <source>Play previous MIDI example</source>
        <translation>Воспроизведение предыдущего MIDI-пример</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="95"/>
        <source>Show copyright information about the MIDI file</source>
        <translation>Показать информацию об авторских правах о MIDI-файла</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="104"/>
        <source>Play next MIDI example</source>
        <translation>Воспроизвести следующий пример MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="92"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
</context>
<context>
    <name>Expert</name>
    <message>
        <location filename="../qml/Expert.qml" line="111"/>
        <source>Expert Mode</source>
        <translation>Экспертный Режим</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="85"/>
        <source>Go back to Simple Mode</source>
        <translation>Вернутся в простой режим</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="89"/>
        <source>Help and General Information</source>
        <translation>помощь и общая информация</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="94"/>
        <source>Modify Preferences</source>
        <translation>Открыть настройки</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="99"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Создание музыки на сенсорном экране клавиатуры</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="104"/>
        <source>Show Logfile</source>
        <translation>файла журнала дисплей</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="168"/>
        <source>Midi Player</source>
        <translation>MIDI-плеер</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="180"/>
        <source>Midi Examples</source>
        <translation>Примеры MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="193"/>
        <source>Instrument</source>
        <translation>Инструмент</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="231"/>
        <source>Frequency:</source>
        <translation>Частота:</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="267"/>
        <source>Reset concert pitch to the standard value of 440 Hz</source>
        <translation>Сбросить концерт шаг к стандартному значению 440 Гц</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="304"/>
        <source>Tuning Mode</source>
        <translation>Режим настройки</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="314"/>
        <source>Temperament</source>
        <translation>Темперация</translation>
    </message>
</context>
<context>
    <name>FileHandler</name>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Please choose a Midi file</source>
        <translation>Пожалуйста, выберите файл Midi</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Midi files (*.mid *.midi *.MID)</source>
        <translation>Midi файлы (*.mid *.midi *.MID)</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="80"/>
        <source>Save custom temperament</source>
        <translation>Сохранить индивидуальный темперамент</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>Temperament files</source>
        <translation>Файлы темперамента</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="89"/>
        <source>Could not write to disk.</source>
        <translation>Не удалось записать на диск.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="115"/>
        <source>Load custom temperament</source>
        <translation>Загрузить пользовательский темперамент</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="123"/>
        <source>Could not read from disk.</source>
        <translation>Файл не может быть загружен.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>Reading Error</source>
        <translation>Ошибка загрузки</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>This is not a data file generated by JustIntonation</source>
        <translation>Это не файл данных, созданный JustIntonation</translation>
    </message>
</context>
<context>
    <name>GettingStarted</name>
    <message>
        <location filename="../qml/GettingStarted.qml" line="28"/>
        <source>Getting started</source>
        <translation>Для начала</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="63"/>
        <source>Welcome to &quot;Just Intonation&quot;.</source>
        <translation>Добро пожаловать в «Just Intonation».</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="72"/>
        <source>With this application you can experience how it would be to hear and play Music in just intonation, independent of scale!</source>
        <translation>С помощью этой программы вы можете ощутить, что такое слушать или играть музыку с чистой интонацией, независимо от тональности!</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="73"/>
        <source>System Requirements</source>
        <translation>Системные требования</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="74"/>
        <source>The application runs on most desktop computers and on sufficiently powerful tablets and smartphones.</source>
        <translation>Программа работает на большинстве ПК и на достаточно производительных смартфонах и планшетных компьютерах.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="75"/>
        <source>It needs about 300MB of temporary memory (RAM).</source>
        <translation>Требуются около 300 MB оперативный памяти (ЗУПВ, RAM).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="76"/>
        <source>In addition, if you want to download samples of realistic musical instruments, approximately 600MB of permanent storage (disk space) is needed.</source>
        <translation>Если Вы хотите скачать звуки реальных музыкальных инструментов, трубуются дополнительно 600 MB постоянной памяти (жёсткий диск).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="77"/>
        <source>Download Realistic Instruments</source>
        <translation>Скачать реалистичные инструменты</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="78"/>
        <source>On startup the application will ask you for permission to download samples of real instruments from our website.</source>
        <translation>При запуске, программа запросит разрешение на скачивание семплов реальных инструментов с нашего сайта.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="79"/>
        <source>Depending on the speed of your internet connection this may take some time.</source>
        <translation>В зависимости от скорости Вашего интернет-соединения, это может длиться какое-то время.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="80"/>
        <source>During the download you can already start to use the application.</source>
        <translation>В то время как идёт загрузка, Вы уже можете начать пользоваться программой.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="81"/>
        <source>Listen to Music</source>
        <translation>Слушать музыку</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="82"/>
        <source>To hear music just tap the leftmost button with the note in the lower toolbar.</source>
        <translation>Чтобы услышать музыку, нажмите левую кнопку с нотой в нижней панели инструментов.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="83"/>
        <source>Toggle between just intonation and equal temperament and listen to the difference.</source>
        <translation>Переключайте между режимами «Чистая интонация» и «Равномерная темперация» чтобы услышать разницу.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="84"/>
        <source>Listen to your own Music</source>
        <translation>Слушать собственную музыку</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="85"/>
        <source>Connect a Midi keyboard to your device.</source>
        <translation>Подключите MIDI-клавиатуру к вашему устройству.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="86"/>
        <source>The application will recognize most Midi devices automatically.</source>
        <translation>Как правило, программа автоматически распознаёт MIDI-устройство.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="87"/>
        <source>Just play on your keyboard and hear yourself playing in just intonation.</source>
        <translation>Просто попробуйте поиграть на Вашей клавиатуре и Вы услышите что Вы играете с чистой интонацией.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="88"/>
        <source>Midi keyboards can also be connected to mobile devices using a special adapter cable (see user manual).</source>
        <translation>С помощью специального адаптера, Вы можете также подключить к MIDI-клавиатуре Ваш смартфон или планшетный компьютер (смотри руководство).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="89"/>
        <source>Expert Mode</source>
        <translation>Экспертный Режим</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="90"/>
        <source>Tap the rightmost button in the lower toolbar to enter the Expert Mode.</source>
        <translation>Чтобы попасть в экспертный режим, нажмите правую кнопку в нижней панели инструментов.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="91"/>
        <source>Here you can choose various temperaments.</source>
        <translation>Здесь Вы найдёте широкий выбор темпераций.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="92"/>
        <source>Moreover, you can control a large variety of parameters.</source>
        <translation>Кроме того, Вы можете работать с большим выбором параметров.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="93"/>
        <source>It is also possible to monitor the pitch drift while you are playing.</source>
        <translation>Также, Вы можете наблюдать отклонение (дрейф) высоты звука во время игры.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="94"/>
        <source>More Information</source>
        <translation>Дополнительная информация</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="95"/>
        <source>You want to learn more about Just Intonation and the history of temperaments?</source>
        <translation>Вы хотите узнать больше о чистой интоции и об истории темперации?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="96"/>
        <source>You would like to know more about the features of the application and how it works internally?</source>
        <translation>Вы хотели бы получить больше информации об особенностях этой программы и о её внутренних операциях?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="97"/>
        <source>Please download the User Manual and visit our website for more detailed information.</source>
        <translation>Для дальнейшей информации, скачайте инструкцию по эксплуатации и поситите наш сайт.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="98"/>
        <source>Thank you for your interest in Just Intonation.</source>
        <translation>Большое спасибо за Ваш интерес, проявленный к программе Just Intonation.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="112"/>
        <source>Download User Manual</source>
        <translation>Скачать инструкцию по эксплуатации</translation>
    </message>
</context>
<context>
    <name>Instrument</name>
    <message>
        <location filename="../../instrument/instrument.cpp" line="80"/>
        <source>Generating artificial sound.</source>
        <translation>Генерация звуков воспроизведённых компьютером.</translation>
    </message>
    <message>
        <location filename="../../instrument/instrument.cpp" line="96"/>
        <source>Loading instrument. Please wait...</source>
        <translation>Загрузка инструмента. &lt;br&gt;Подождите пожалуйста...</translation>
    </message>
</context>
<context>
    <name>Keyboard</name>
    <message>
        <location filename="../qml/Keyboard.qml" line="70"/>
        <source>Keyboard</source>
        <translation>клавиатура</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="151"/>
        <source>Mouse</source>
        <translation>мышь</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="152"/>
        <source>Touchscreen</source>
        <translation>сенсорный экран</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="163"/>
        <source>Single tone</source>
        <translation>Один тон</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="164"/>
        <source>Triad</source>
        <translation>Трезву́чие</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="134"/>
        <source>Equal</source>
        <translation>равномерный</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Just</source>
        <translation>Чистая</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Nonequal</source>
        <translation>нечистый</translation>
    </message>
</context>
<context>
    <name>LearnMore</name>
    <message>
        <location filename="../qml/LearnMore.qml" line="29"/>
        <source>Learn more</source>
        <translation>Узнать больше</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="51"/>
        <source>Basic Help for Getting Started</source>
        <translation>Основная помощь для начала работы</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="52"/>
        <source>Getting started</source>
        <translation>Для начала</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="60"/>
        <source>Download User Manual from the Internet</source>
        <translation>Скачать инструкцию по эксплуатации</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="61"/>
        <source>Download manual</source>
        <translation>Скачать инструкцию</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="69"/>
        <source>Visit Project Website on the Internet</source>
        <translation>посетить домашнюю страницу проекта</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="71"/>
        <source>Visit website</source>
        <translation>Домашняя страница</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="79"/>
        <source>Download Scientific Documentation describing Details of the Tuning Method</source>
        <translation>Скачать научно-технической документации (подробности алгоритма)</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="81"/>
        <source>Tuning Method</source>
        <translation>Алгоритм</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="90"/>
        <source>Get more Information about this Application</source>
        <translation>Получить более подробную информацию об этом приложении</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="91"/>
        <source>About this App</source>
        <translation>О программе</translation>
    </message>
</context>
<context>
    <name>LogFile</name>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>A#</source>
        <translation>A#</translation>
    </message>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>B</source>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>Logfile</name>
    <message>
        <location filename="../qml/Logfile.qml" line="29"/>
        <source>Clear</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="32"/>
        <source>Clear log file</source>
        <translation>Очистить лог-файл</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="38"/>
        <source>Copy</source>
        <translation>Скопировать</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="39"/>
        <source>Copy log file to the clipboard</source>
        <translation>Скопируйте файл журнала в буфер обмена</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="48"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Вернуться к началу</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="55"/>
        <source>Play / Pause</source>
        <translation>Плей / Пауза</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="66"/>
        <source>Close Logfile</source>
        <translation>Закрыть файл журнала</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="70"/>
        <source>Logfile</source>
        <translation>Журнальный файл</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="55"/>
        <source>https://www.gnu.org/licenses/gpl.html</source>
        <translation>https://www.gnu.org/licenses/gpl-3.0.ru.html</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="58"/>
        <source>http://www.just-intonation.org</source>
        <translation>http://www.just-intonation.org</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="170"/>
        <source>manual_en.pdf</source>
        <translation>manual_en.pdf</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="257"/>
        <source>Equal Temperament</source>
        <translation>Равномерная темперация</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="258"/>
        <source>Just Intonation</source>
        <translation>Чистая интонация</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="260"/>
        <source>1/4 Meantone</source>
        <translation>Среднетоновый строй на 1/4 коммы</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="259"/>
        <source>Pythagorean tuning</source>
        <translation>Пифагоров строй</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="261"/>
        <source>Werckmeister III</source>
        <translation>Werckmeister III</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="262"/>
        <source>Silbermann 1/6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="263"/>
        <source>Zarlino -2/7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="264"/>
        <source>User-defined</source>
        <translation>Определяемые пользователем</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="392"/>
        <location filename="../qml/Main.qml" line="407"/>
        <source>Inactive</source>
        <translation>Пассивный</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="413"/>
        <source>[Choose Audio Device]</source>
        <translation>[Выберите аудиоустройство]</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="415"/>
        <source>[Choose]</source>
        <translation>[выберите]</translation>
    </message>
</context>
<context>
    <name>Midi</name>
    <message>
        <location filename="../../midi.cpp" line="415"/>
        <location filename="../../midi.cpp" line="449"/>
        <location filename="../../midi.cpp" line="505"/>
        <source>Inactive</source>
        <translation>Пассивный</translation>
    </message>
    <message>
        <location filename="../../midi.cpp" line="506"/>
        <source>[Choose Midi device]</source>
        <translation>[Выберите MIDI-устройство]</translation>
    </message>
</context>
<context>
    <name>MidiInput</name>
    <message>
        <location filename="../qml/MidiInput.qml" line="69"/>
        <source> Automatic Mode</source>
        <translation> Автоматический режим</translation>
    </message>
    <message>
        <location filename="../qml/MidiInput.qml" line="97"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>В автоматическом режиме новые MIDI-устройства распознаются автоматически</translation>
    </message>
</context>
<context>
    <name>MidiOutput</name>
    <message>
        <location filename="../qml/MidiOutput.qml" line="48"/>
        <source>Channel:</source>
        <translation>канал:</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="97"/>
        <source> Automatic Mode</source>
        <translation> Автоматический режим</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="126"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>В автоматическом режиме новые MIDI-устройства распознаются автоматически</translation>
    </message>
</context>
<context>
    <name>MidiPlayer</name>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="109"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Вернуться к началу</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="117"/>
        <source>Play / Pause</source>
        <translation>Плей / Пауза</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="127"/>
        <source>Repeat mode</source>
        <translation>Режим Повтора</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="135"/>
        <source>Open a Midi file</source>
        <translation>Открыть MIDI-файл</translation>
    </message>
</context>
<context>
    <name>NavigationManager</name>
    <message>
        <location filename="../qml/NavigationManager.qml" line="152"/>
        <source>Reset settings</source>
        <translation>Сбросить настройки</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="153"/>
        <source>Do you want to reset all settings?</source>
        <translation>Вы хотите сбросить все настройки?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="154"/>
        <source>All changes that you made will be lost and the settings will be reset to default values.</source>
        <translation>Все ваши изменения будут потеряны. Настройки сбрасываются на значения по умолчанию.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="164"/>
        <source>Help on choosing the audio parameters</source>
        <translation>Как выбрать параметры</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The sample rate has to be 44100.</source>
        <translation>Частота дискретизации должна быть 44100 Гц.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The buffer size should be small.</source>
        <translation>Размер буфера должен быть настолько мал, насколько это возможно.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="166"/>
        <source>Buffer size = 0 means to use the default value. Packet size and buffer size should be the same. The buffer size controls the size of the internal audio buffer of your device. If you hear crackling sounds this value should be increased. Try to find the lowest possible value. The packet size controls the number of frames generated by the application in a single packet. In case of latency problems try to reduce this value.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="173"/>
        <source>Confirm Download</source>
        <translation>подтвердить скачать</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="174"/>
        <source>Would you like to hear realistic musical instruments?</source>
        <translation>Хотите ли Вы услышать реалистичные музыкальные инструменты?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="175"/>
        <source>In this case the application will silently download 600 MB of sampled sounds from the internet to your device.</source>
        <translation>В этом случае программа сама скачает с интернета на Ваше устройство звуковые семплы в размере 600 MB.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="176"/>
        <source>If you allow the application to download samples from our server you can hear realistic instruments such as a piano, an organ, or a harpsichord. The samples are downloaded only once and saved locally. Please make sure that your device offers about 600 MB of free disk space. If you deny internet access, the application will only provide artificially generated sounds (triangular waves).</source>
        <translation>Если Вы разрешите приложению скачать сэмплы с нашего сервера, то Вы сможите послушать реальные инструменты, как например: рояль, орган или клавесин. Приложение скачает их только один раз и сохранит их локально. Пожалуйста удостоверитесь, что Ваше устройство имеет в своём распоряжении минимум 600 Мбайт свободного пространства. Если Вы откажите в доступе к сети, приложение предоставит только искусственные звуки (треугольные волны).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="187"/>
        <source>No Internet Connection</source>
        <translation>Нет соединения с интернетом</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="188"/>
        <source>Could not find a valid internet connection for downloading.</source>
        <translation>Не удалось найти допустимое подключение к Интернету для загрузки.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="189"/>
        <source>Please use a fast internet connection (WLAN or wired).</source>
        <translation>Пожалуйста, используйте высокоскоростное подключение к Интернету (WLAN или проводной).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="190"/>
        <source>Retry</source>
        <translation>повторите попытку</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="191"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/Preferences.qml" line="35"/>
        <source>Preferences</source>
        <translation>Предпочтения</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="86"/>
        <source>External Midi Keyboard</source>
        <translation>Внешней MIDIкклавиатуры</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="119"/>
        <source>External Midi Output Device</source>
        <translation>Внешний MIDI-выходная</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="96"/>
        <source>Audio Output Device</source>
        <translation>Аудио выход</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="126"/>
        <source>System settings</source>
        <translation>Системные настройки</translation>
    </message>
</context>
<context>
    <name>ProgressIndicator</name>
    <message>
        <location filename="../qml/ProgressIndicator.qml" line="132"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
</context>
<context>
    <name>RunGuard</name>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="168"/>
        <source>Information</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="169"/>
        <source>Just Intonation is already running.</source>
        <translation>Программа «Just intonation» уже запущена.</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="170"/>
        <source>Please close the application before restarting.</source>
        <translation>Пожалуйста закройте программу перед перезагрузкой.</translation>
    </message>
</context>
<context>
    <name>Simple</name>
    <message>
        <location filename="../qml/Simple.qml" line="60"/>
        <source>Play a Midi demo or drop a Midi file here</source>
        <translation>Играть демо Midi или вы можете перетаскивать файл Midi здесь</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="86"/>
        <source>Enter Expert Mode</source>
        <translation>Войти в Экспертный Режим</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="143"/>
        <source>Instrument:</source>
        <translation>Инструмент:</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="198"/>
        <source>Just Intonation</source>
        <translation>Чистая интонация</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="317"/>
        <source>Downloading Instruments</source>
        <translation>Скачать звуковые образцы</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="374"/>
        <source>Tap this button to hear a demo</source>
        <translation>Нажмите эту кнопку, чтобы прослушать демо</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="197"/>
        <source>Equal Temperament</source>
        <translation>Равномерная темперация</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="76"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Создание музыки на сенсорном экране клавиатуры</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="81"/>
        <source>Modify Preferences</source>
        <translation>Открыть настройки</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="71"/>
        <source>Help and General Information</source>
        <translation>помощь и общая информация</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="212"/>
        <source>Delayed Tuning</source>
        <translation>Строй с задержкой</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="234"/>
        <source>Compensate Pitch Drift</source>
        <translation>Компенсировать &lt;br&gt;дрейф высоты звука</translation>
    </message>
</context>
<context>
    <name>SystemSettings</name>
    <message>
        <location filename="../qml/SystemSettings.qml" line="40"/>
        <source>Welcome screen on startup</source>
        <translation>Экран приветствия при &lt;br&gt;запуске программы</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="48"/>
        <source>Allow download of samples</source>
        <translation>Разрешить загрузку реалистичных &lt;br&gt;музыкальных инструментов</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="61"/>
        <source>Language</source>
        <translation>язык</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="103"/>
        <source>Reset all settings and restart the application</source>
        <translation>Сбросить все настройки и перезапустить приложение</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="102"/>
        <source>Restore default settings</source>
        <translation>Восстановить заводские настройки</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="80"/>
        <source>Forced instrument download</source>
        <translation>сила скачать инструменты</translation>
    </message>
</context>
<context>
    <name>Temperament</name>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Semitone</source>
        <translation>Полутон</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Major Second</source>
        <translation>Большая секунда</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Minor Third</source>
        <translation>Малая терция</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Major Third</source>
        <translation>Большая терция</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Perfect Fourth</source>
        <translation>Чистая кварта</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Tritone</source>
        <translation>Тритон</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Perfect Fifth</source>
        <translation>Чистая квинта</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Minor Sixth</source>
        <translation>Малая секста</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Major Sixth</source>
        <translation>Большая секста</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Minor Seventh</source>
        <translation>Малая септима</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Major Seventh</source>
        <translation>Большая септима</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Octave</source>
        <translation>Октава</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="166"/>
        <source>Enter temperament name</source>
        <translation>Введите имя темперамента</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="258"/>
        <source>Reset weights</source>
        <translation>Сбросить влияние</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="187"/>
        <source>Open a temperament file</source>
        <translation>Загрузите файл темперамента</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="206"/>
        <source>Save a temperament file</source>
        <translation>Сохранить файл темперамента</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="70"/>
        <source>Static Tuning</source>
        <translation>Статическая настройка</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>A#</source>
        <translation>Ля-диез (A#)</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>B</source>
        <translation>Си (В)</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="106"/>
        <source>Subminor Seventh 7:4</source>
        <translation>Малая септима 7:4</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="241"/>
        <source>Set all interval size deviations to zero (equal temperament)</source>
        <translation>Сброс к: Равномерная темперация</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="259"/>
        <source>Restore recommended standard values for the weights</source>
        <translation>Восстановить рекомендуемые стандартные значения для весовых коэффициентов</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="288"/>
        <source>Interval:</source>
        <translation>Интервал:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="296"/>
        <source>Deviation:</source>
        <translation>отклонение:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="306"/>
        <source>Weight:</source>
        <translation>влияние:</translation>
    </message>
</context>
<context>
    <name>TextHeader</name>
    <message>
        <location filename="../qml/TextHeader.qml" line="108"/>
        <source>Modify Preferences</source>
        <translation>Открыть настройки</translation>
    </message>
    <message>
        <location filename="../qml/TextHeader.qml" line="120"/>
        <source>Close window</source>
        <translation>Закрыть окно</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../modules/system/translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>автоматически</translation>
    </message>
</context>
<context>
    <name>TuningMode</name>
    <message>
        <location filename="../qml/TuningMode.qml" line="43"/>
        <source>Tuning delay</source>
        <translation>Настройка задержки</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="78"/>
        <source>Memory</source>
        <translation>Память</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="115"/>
        <source>Drift correction</source>
        <translation>Коррекция дрейфа</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="215"/>
        <source>cent</source>
        <translation>цент</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="279"/>
        <source>tempered</source>
        <translation>нестройный</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="289"/>
        <source>just</source>
        <translation>созвучный</translation>
    </message>
</context>
<context>
    <name>Voice</name>
    <message>
        <location filename="../../instrument/voice.cpp" line="64"/>
        <source>Please wait...</source>
        <translation>Подождите пожалуйста...</translation>
    </message>
    <message>
        <location filename="../../instrument/voice.cpp" line="149"/>
        <source>Reading scale</source>
        <translation>Считывание звукоряда</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../qml/Welcome.qml" line="52"/>
        <source>Hello!</source>
        <translation>Здравствуйте!</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="64"/>
        <source>This application gives you the opportunity to play in just intonation independent of scale.</source>
        <translation>С помощью этой программы Вы можете играть с чистой интонацией независимо от тональности.</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="85"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="86"/>
        <source>Start Just Intonation</source>
        <translation>Приложение запуска</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="97"/>
        <source>Learn more</source>
        <translation>Узнать больше</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="98"/>
        <source>Visit our website to learn more about Just Intonation</source>
        <translation>Посетите наш сайт, чтобы узнать больше об этом проекте</translation>
    </message>
</context>
</TS>
