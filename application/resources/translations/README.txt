To add a new language look up the country code
ISO 639-1

Then add entries in languages.qrc and application.pro



General information about languages 
-----------------------------------

Before starting register all QML files as sources 
in the project file within a lupdate_only section.
lupdate_only {
    SOURCES += qml/*.qml
}
This has to be done only once

How to add a new language:
--------------------------

1. Get ISO language code (here xx)

2. Handle .qm files from qt, see below

3. Register the new language in languages.qrc
    Open    JustIntonation/application/resources/translations/languages.qrc
    and register the (still nonexisting) ts file
    JustIntonation/application/resources/translations/justintonation_xx.qm

4. Register the new language in Qml:
    Open JustIntonation/application/resources/qml/Main.qml
    and search for "property variant languages".
    and change all entries in the paragraph accordingly.
    
5. Register the new language in application.pro
    See translations section
    
6. Run qmake

7. Run lupdate (upper linguist option in QtCreator)
    by calling Extras->Extern->Linguist->Update
    in order to create justintonation_xx.ts

8. Provide translation
    Open the ts file in the translations folder with
    the Qt Linguist and provide all the data....

9. Release them
    Do NOT call lrelease (Extras->Extern->Linguist->Release)
    This doesn't work for some reason (it looks for translations 
    in the top level only).
    Instead go to the JustIntionation/application directory and call
    /home/hinrichsen/Software/Qt/5.9/gcc_64/bin/lrelease application.pro
    
10. Save the flag
    Download flag from Wikipedia, save it in
    JustIntonation/application/resources/media/icons
    and add an entry in media.qrc one level above.
    
11. Integrate the flag in the welcome screen
    Edit Welcome.qml accordingly
    
