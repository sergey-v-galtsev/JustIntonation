<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/About.qml" line="95"/>
        <source>Theoretical Physics III</source>
        <translation>Physique Théorique III</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="96"/>
        <source>Faculty for Physics and Astronomy</source>
        <translation>Faculté de Physique et d&apos;Astronomie</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>University of</source>
        <translation>Université de</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="97"/>
        <source>, Germany</source>
        <translation>, Allemagne</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="117"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="165"/>
        <source>is free software licensed under GNU GPLv3. The source code can be downloaded from Gitlab:</source>
        <translation>est un logiciel gratuit sous GNU GPLv3. Le code source peut être téléchargé depuis Gitlab:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="208"/>
        <source>This software is based on the following open-source third-party components:</source>
        <translation>Ce logiciel est basé sur les composants tiers open source suivants:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="275"/>
        <source>We thank all those who have contributed to the project:</source>
        <translation>Nous remercions tous ceux qui ont contribué au projet:</translation>
    </message>
    <message>
        <location filename="../qml/About.qml" line="278"/>
        <source>Translations: </source>
        <translation>Traductions: </translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../../application.cpp" line="538"/>
        <source>Grand Piano</source>
        <translation>Grand Piano</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="539"/>
        <source>Organ</source>
        <translation>Orgue</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="540"/>
        <source>Harpsichord</source>
        <translation>Clavecin</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="541"/>
        <source>Unknown Instrument</source>
        <translation>Instrument inconnu</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="543"/>
        <source>Artificial sound</source>
        <translation>Son artificiel</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="544"/>
        <source>External device</source>
        <translation>Périphérique externe</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="812"/>
        <source>[Choose output device]</source>
        <translation>[Choisir un périphérique]</translation>
    </message>
    <message>
        <location filename="../../application.cpp" line="858"/>
        <source>No device connected</source>
        <translation>Aucun périphérique connecté</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../qml/AudioOutput.qml" line="76"/>
        <source>Sample rate:</source>
        <translation>Taux d&apos;échantillonnage:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="105"/>
        <source>Buffer size:</source>
        <translation>Taille du buffer:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="120"/>
        <source>Show more help on audio settings</source>
        <translation>Afficher plus d&apos;aide concernant les paramètres audio</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="148"/>
        <source>Packet size:</source>
        <translation>Taille du paquet:</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="179"/>
        <source>Reset</source>
        <translation>Réinit</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="182"/>
        <source>Reset audio settings to default values</source>
        <translation>Réinitialiser les paramètres audio aux valeurs par défaut</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="189"/>
        <source>Apply Changes</source>
        <translation>Appliquer modifications</translation>
    </message>
    <message>
        <location filename="../qml/AudioOutput.qml" line="190"/>
        <source>Apply the audio settings specified in this panel</source>
        <translation>Appliquer les paramètres audio spécifiés dans ce panneau</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>Yes</source>
        <translation>Oui</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="85"/>
        <source>OK</source>
        <translation>D&apos;accord</translation>
    </message>
    <message>
        <location filename="../qml/Dialog.qml" line="95"/>
        <source>No</source>
        <translation>Non</translation>
    </message>
</context>
<context>
    <name>Examples</name>
    <message>
        <location filename="../qml/Examples.qml" line="45"/>
        <source>Example </source>
        <translation>Exemple </translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="85"/>
        <source>Play previous MIDI example</source>
        <translation>Jouer l&apos;exemple MIDI précédent</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="92"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="95"/>
        <source>Show copyright information about the MIDI file</source>
        <translation>Afficher les informations sur les droits d&apos;auteur concernant le fichier MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Examples.qml" line="104"/>
        <source>Play next MIDI example</source>
        <translation>Jouer l&apos;exemple MIDI suivant</translation>
    </message>
</context>
<context>
    <name>Expert</name>
    <message>
        <location filename="../qml/Expert.qml" line="85"/>
        <source>Go back to Simple Mode</source>
        <translation>Revenir en mode simple</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="89"/>
        <source>Help and General Information</source>
        <translation>Aide et informations générales</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="94"/>
        <source>Modify Preferences</source>
        <translation>Modifier les préférences</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="99"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Jouer de la musique sur le clavier tactile</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="104"/>
        <source>Show Logfile</source>
        <translation>Afficher le fichier journal</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="111"/>
        <source>Expert Mode</source>
        <translation>Mode expert</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="168"/>
        <source>Midi Player</source>
        <translation>Lecteur MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="180"/>
        <source>Midi Examples</source>
        <translation>Exemples MIDI</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="193"/>
        <source>Instrument</source>
        <translation>Instrument</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="231"/>
        <source>Frequency:</source>
        <translation>Fréquence:</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="267"/>
        <source>Reset concert pitch to the standard value of 440 Hz</source>
        <translation>Réinitialiser l&apos;accordage de concert à la valeur standard de 440 Hz</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="304"/>
        <source>Tuning Mode</source>
        <translation>Mode Accordage</translation>
    </message>
    <message>
        <location filename="../qml/Expert.qml" line="314"/>
        <source>Temperament</source>
        <translation>Tempérament</translation>
    </message>
</context>
<context>
    <name>FileHandler</name>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Please choose a Midi file</source>
        <translation>Choisissez un fichier MIDI</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="60"/>
        <source>Midi files (*.mid *.midi *.MID)</source>
        <translation>Fichiers MIDI (* .mid * .midi * .MID)</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="80"/>
        <source>Save custom temperament</source>
        <translation>Enregistrer le tempérament personnalisé</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>Temperament files</source>
        <translation>Fichiers de tempérament</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="81"/>
        <location filename="../../system/filehandler.cpp" line="116"/>
        <source>All files</source>
        <translation>Tous les fichiers</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="89"/>
        <source>Could not write to disk.</source>
        <translation>Impossible d&apos;enregistrer le fichier.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="115"/>
        <source>Load custom temperament</source>
        <translation>Chargez le tempérament personnalisé</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="123"/>
        <source>Could not read from disk.</source>
        <translation>Impossible de lire sur le disque.</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>Reading Error</source>
        <translation>Impossible de charger</translation>
    </message>
    <message>
        <location filename="../../system/filehandler.cpp" line="132"/>
        <source>This is not a data file generated by JustIntonation</source>
        <translation>Ce n&apos;est pas un fichier de données généré par JustIntonation</translation>
    </message>
</context>
<context>
    <name>GettingStarted</name>
    <message>
        <location filename="../qml/GettingStarted.qml" line="28"/>
        <source>Getting started</source>
        <translation>Mise en route</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="63"/>
        <source>Welcome to &quot;Just Intonation&quot;.</source>
        <translation>Bienvenue à &quot;Just Intonation&quot; (Gamme Naturelle).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="72"/>
        <source>With this application you can experience how it would be to hear and play Music in just intonation, independent of scale!</source>
        <translation>Avec cette application, vous pouvez découvrir comment entendre et jouer de la musique avec une gamme naturelle, indépendamment de la clé!</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="73"/>
        <source>System Requirements</source>
        <translation>Configuration requise</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="74"/>
        <source>The application runs on most desktop computers and on sufficiently powerful tablets and smartphones.</source>
        <translation>L&apos;application s&apos;exécute sur la plupart des ordinateurs de bureau et sur des tablettes et des smartphones suffisamment puissants.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="75"/>
        <source>It needs about 300MB of temporary memory (RAM).</source>
        <translation>L&apos;application nécessite environ 300 Mo de mémoire temporaire (RAM).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="76"/>
        <source>In addition, if you want to download samples of realistic musical instruments, approximately 600MB of permanent storage (disk space) is needed.</source>
        <translation>En outre, si vous souhaitez télécharger des échantillons d&apos;instruments de musique réels, environ 600 Mo de mémoire permanente (espace disque) sont nécessaires.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="77"/>
        <source>Download Realistic Instruments</source>
        <translation>Télécharger des instruments réels</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="78"/>
        <source>On startup the application will ask you for permission to download samples of real instruments from our website.</source>
        <translation>Au démarrage, l&apos;application vous demandera l&apos;autorisation de télécharger des échantillons d&apos;instruments réels à partir de notre site Web.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="79"/>
        <source>Depending on the speed of your internet connection this may take some time.</source>
        <translation>Selon la vitesse de votre connexion Internet, cela peut prendre un certain temps.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="80"/>
        <source>During the download you can already start to use the application.</source>
        <translation>Au cours du téléchargement, vous pouvez déjà commencer à utiliser l&apos;application.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="81"/>
        <source>Listen to Music</source>
        <translation>Écouter de la musique</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="82"/>
        <source>To hear music just tap the leftmost button with the note in the lower toolbar.</source>
        <translation>Pour entendre de la musique, appuyez sur le bouton le plus à gauche avec la note dans la barre d&apos;outils inférieure.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="83"/>
        <source>Toggle between just intonation and equal temperament and listen to the difference.</source>
        <translation>Basculez entre l&apos;intonation parfaite et le tempérament égal et écoutez la différence.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="84"/>
        <source>Listen to your own Music</source>
        <translation>Écoutez votre propre musique</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="85"/>
        <source>Connect a Midi keyboard to your device.</source>
        <translation>Connectez un clavier MIDI à votre appareil.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="86"/>
        <source>The application will recognize most Midi devices automatically.</source>
        <translation>L&apos;application reconnaîtra automatiquement la plupart des appareils MIDI.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="87"/>
        <source>Just play on your keyboard and hear yourself playing in just intonation.</source>
        <translatorcomment>gamme naturelle</translatorcomment>
        <translation>Il suffit de jouer sur votre clavier et vous pouvez vous entendre jouer en gamme naturelle.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="88"/>
        <source>Midi keyboards can also be connected to mobile devices using a special adapter cable (see user manual).</source>
        <translation>Les claviers Midi peuvent également être connectés à des appareils mobiles à l&apos;aide d&apos;un câble adaptateur spécial (voir le manuel de l&apos;utilisateur en anglais).</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="89"/>
        <source>Expert Mode</source>
        <translation>Mode expert</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="90"/>
        <source>Tap the rightmost button in the lower toolbar to enter the Expert Mode.</source>
        <translation>Appuyez sur le bouton le plus à droite dans la barre d&apos;outils inférieure pour accéder au mode Expert.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="91"/>
        <source>Here you can choose various temperaments.</source>
        <translation>Ici, vous pouvez choisir différents tempéraments.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="92"/>
        <source>Moreover, you can control a large variety of parameters.</source>
        <translation>En outre, vous pouvez contrôler une grande variété de paramètres.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="93"/>
        <source>It is also possible to monitor the pitch drift while you are playing.</source>
        <translation>Il est également possible de surveiller la dérive de fréquence pendant que vous jouez.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="94"/>
        <source>More Information</source>
        <translation>Plus d&apos;information</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="95"/>
        <source>You want to learn more about Just Intonation and the history of temperaments?</source>
        <translation>Vous voulez en savoir plus sur la gamme naturelle et l&apos;histoire des tempéraments?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="96"/>
        <source>You would like to know more about the features of the application and how it works internally?</source>
        <translation>Vous souhaitez en savoir plus sur les fonctionnalités de l&apos;application et sur la façon dont elle fonctionne en interne?</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="97"/>
        <source>Please download the User Manual and visit our website for more detailed information.</source>
        <translation>Téléchargez le Manuel de l&apos;utilisateur et visitez notre site Web pour obtenir des informations plus détaillées.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="98"/>
        <source>Thank you for your interest in Just Intonation.</source>
        <translation>Merci de votre intérêt pour &quot;Just Intonation&quot;.</translation>
    </message>
    <message>
        <location filename="../qml/GettingStarted.qml" line="112"/>
        <source>Download User Manual</source>
        <translation>Télécharger le manuel (en anglais)</translation>
    </message>
</context>
<context>
    <name>Instrument</name>
    <message>
        <location filename="../../instrument/instrument.cpp" line="80"/>
        <source>Generating artificial sound.</source>
        <translation>Génération de sons artificiels.</translation>
    </message>
    <message>
        <location filename="../../instrument/instrument.cpp" line="96"/>
        <source>Loading instrument. Please wait...</source>
        <translation>Chargement de l&apos;instrument. Attendez...</translation>
    </message>
</context>
<context>
    <name>Keyboard</name>
    <message>
        <location filename="../qml/Keyboard.qml" line="70"/>
        <source>Keyboard</source>
        <translation>Clavier</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="134"/>
        <source>Equal</source>
        <translation>Égal</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Just</source>
        <translation>Naturelle</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="135"/>
        <source>Nonequal</source>
        <translation>Non égal</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="151"/>
        <source>Mouse</source>
        <translation>Souris</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="152"/>
        <source>Touchscreen</source>
        <translation>Écran tactile</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="163"/>
        <source>Single tone</source>
        <translation>Unique</translation>
    </message>
    <message>
        <location filename="../qml/Keyboard.qml" line="164"/>
        <source>Triad</source>
        <translation>Triade</translation>
    </message>
</context>
<context>
    <name>LearnMore</name>
    <message>
        <location filename="../qml/LearnMore.qml" line="29"/>
        <source>Learn more</source>
        <translation>Apprendre davantage</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="51"/>
        <source>Basic Help for Getting Started</source>
        <translation>Aide de base pour commencer</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="52"/>
        <source>Getting started</source>
        <translation>Mise en route</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="60"/>
        <source>Download User Manual from the Internet</source>
        <translation>Téléchargez le manuel utilisateur depuis Internet</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="61"/>
        <source>Download manual</source>
        <translation>Télécharger le manuel</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="69"/>
        <source>Visit Project Website on the Internet</source>
        <translation>Visitez le site Web du projet sur Internet</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="71"/>
        <source>Visit website</source>
        <translation>Visitez le site Web</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="79"/>
        <source>Download Scientific Documentation describing Details of the Tuning Method</source>
        <translation>Télécharger la documentation scientifique décrivant les détails de la méthode d&apos;accordage</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="81"/>
        <source>Tuning Method</source>
        <translation>Méthode d&apos;accordage</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="90"/>
        <source>Get more Information about this Application</source>
        <translation>Obtenez plus d&apos;informations sur cette application</translation>
    </message>
    <message>
        <location filename="../qml/LearnMore.qml" line="91"/>
        <source>About this App</source>
        <translation>À propos de cette application</translation>
    </message>
</context>
<context>
    <name>LogFile</name>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>A#</source>
        <translation>A#</translation>
    </message>
    <message>
        <location filename="../../system/logfile.cpp" line="77"/>
        <source>B</source>
        <translation>B</translation>
    </message>
</context>
<context>
    <name>Logfile</name>
    <message>
        <location filename="../qml/Logfile.qml" line="29"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="32"/>
        <source>Clear log file</source>
        <translation>Effacer le fichier journal</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="38"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="39"/>
        <source>Copy log file to the clipboard</source>
        <translation>Copiez le fichier journal dans le presse-papiers</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="48"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Retour au début</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="55"/>
        <source>Play / Pause</source>
        <translation>Lecture / Pause</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="66"/>
        <source>Close Logfile</source>
        <translation>Fermer fichier journal</translation>
    </message>
    <message>
        <location filename="../qml/Logfile.qml" line="70"/>
        <source>Logfile</source>
        <translation>Fichier journal</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="55"/>
        <source>https://www.gnu.org/licenses/gpl.html</source>
        <translation>https://www.gnu.org/licenses/gpl-3.0.fr.html</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="58"/>
        <source>http://www.just-intonation.org</source>
        <translation>http://www.just-intonation.org</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="170"/>
        <source>manual_en.pdf</source>
        <translation>manual_en.pdf</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="257"/>
        <source>Equal Temperament</source>
        <translation>Tempérament égal</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="258"/>
        <source>Just Intonation</source>
        <translation>Gamme naturelle</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="259"/>
        <source>Pythagorean tuning</source>
        <translation>Accord pythagoricien</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="260"/>
        <source>1/4 Meantone</source>
        <translation>Mésotonique 1/4</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="261"/>
        <source>Werckmeister III</source>
        <translation>Werckmeister III</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="262"/>
        <source>Silbermann 1/6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="263"/>
        <source>Zarlino -2/7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="264"/>
        <source>User-defined</source>
        <translation>Défini par l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="392"/>
        <location filename="../qml/Main.qml" line="407"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="413"/>
        <source>[Choose Audio Device]</source>
        <translation>[Choisir un périphérique audio]</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="415"/>
        <source>[Choose]</source>
        <translation>[Choisir]</translation>
    </message>
</context>
<context>
    <name>Midi</name>
    <message>
        <location filename="../../midi.cpp" line="415"/>
        <location filename="../../midi.cpp" line="449"/>
        <location filename="../../midi.cpp" line="505"/>
        <source>Inactive</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../../midi.cpp" line="506"/>
        <source>[Choose Midi device]</source>
        <translation>[Choisir un périphérique Midi]</translation>
    </message>
</context>
<context>
    <name>MidiInput</name>
    <message>
        <location filename="../qml/MidiInput.qml" line="69"/>
        <source> Automatic Mode</source>
        <translation> Mode automatique</translation>
    </message>
    <message>
        <location filename="../qml/MidiInput.qml" line="97"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>En mode automatique, les nouveaux périphériques MIDI sont reconnus automatiquement</translation>
    </message>
</context>
<context>
    <name>MidiOutput</name>
    <message>
        <location filename="../qml/MidiOutput.qml" line="48"/>
        <source>Channel:</source>
        <translation>Canal:</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="97"/>
        <source> Automatic Mode</source>
        <translation> Mode automatique</translation>
    </message>
    <message>
        <location filename="../qml/MidiOutput.qml" line="126"/>
        <source>In the automatic mode new Midi devices are recognized automatically</source>
        <translation>En mode automatique, les nouveaux périphériques MIDI sont reconnus automatiquement</translation>
    </message>
</context>
<context>
    <name>MidiPlayer</name>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="109"/>
        <source>Rewind: Go back to the beginning</source>
        <translation>Revenir au début</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="117"/>
        <source>Play / Pause</source>
        <translation>Lecture / Pause</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="127"/>
        <source>Repeat mode</source>
        <translation>Mode répétition</translation>
    </message>
    <message>
        <location filename="../qml/MidiPlayer.qml" line="135"/>
        <source>Open a Midi file</source>
        <translation>Ouvrir un fichier Midi</translation>
    </message>
</context>
<context>
    <name>NavigationManager</name>
    <message>
        <location filename="../qml/NavigationManager.qml" line="152"/>
        <source>Reset settings</source>
        <translation>Réinitialiser paramètres</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="153"/>
        <source>Do you want to reset all settings?</source>
        <translation>Voulez-vous réinitialiser tous les paramètres?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="154"/>
        <source>All changes that you made will be lost and the settings will be reset to default values.</source>
        <translation>Toutes les modifications que vous avez effectuées seront perdues et les paramètres seront réinitialisés aux valeurs par défaut.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="164"/>
        <source>Help on choosing the audio parameters</source>
        <translation>Aide pour choisir les paramètres audio</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The sample rate has to be 44100.</source>
        <translation>Le taux d&apos;échantillonnage doit être de 44100.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="165"/>
        <source>The buffer size should be small.</source>
        <translation>La taille du buffer devrait être faible.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="166"/>
        <source>Buffer size = 0 means to use the default value. Packet size and buffer size should be the same. The buffer size controls the size of the internal audio buffer of your device. If you hear crackling sounds this value should be increased. Try to find the lowest possible value. The packet size controls the number of frames generated by the application in a single packet. In case of latency problems try to reduce this value.</source>
        <translation>Taille du buffer = 0 signifie utiliser la valeur par défaut. La taille du paquet et la taille du buffer doivent être identiques. La taille du buffer contrôle la taille du buffer audio interne de votre appareil. Si vous entendez des sons crépitants, cette valeur devrait être augmentée. Essayez de trouver la valeur la plus basse possible. La taille du paquet contrôle le nombre d&apos;échantillons générées par l&apos;application dans un seul paquet. En cas de problèmes de latence, essayez de réduire cette valeur.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="173"/>
        <source>Confirm Download</source>
        <translation>Confirmer Télécharger</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="174"/>
        <source>Would you like to hear realistic musical instruments?</source>
        <translation>Souhaitez-vous entendre des instruments de musique réalistes?</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="175"/>
        <source>In this case the application will silently download 600 MB of sampled sounds from the internet to your device.</source>
        <translation>Dans ce cas, l&apos;application téléchargera silencieusement 600 Mo de sons échantillonnés depuis Internet sur votre appareil.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="176"/>
        <source>If you allow the application to download samples from our server you can hear realistic instruments such as a piano, an organ, or a harpsichord. The samples are downloaded only once and saved locally. Please make sure that your device offers about 600 MB of free disk space. If you deny internet access, the application will only provide artificially generated sounds (triangular waves).</source>
        <translation>Si vous autorisez l&apos;application à télécharger des échantillons de notre serveur, vous pouvez entendre des instruments réels comme un piano, un orgue ou un clavecin. Les échantillons sont téléchargés une seule fois et enregistrés localement. Assurez-vous que votre appareil offre environ 600 Mo d&apos;espace disque libre. Si vous refusez l&apos;accès à Internet, l&apos;application ne fournira que des sons générés artificiellement (ondes triangulaires).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="187"/>
        <source>No Internet Connection</source>
        <translation>Pas de connexion Internet</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="188"/>
        <source>Could not find a valid internet connection for downloading.</source>
        <translation>Impossible de trouver une connexion Internet valide pour le téléchargement.</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="189"/>
        <source>Please use a fast internet connection (WLAN or wired).</source>
        <translation>Utilisez une connexion Internet rapide (WLAN ou câble).</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="190"/>
        <source>Retry</source>
        <translation>Recommencez</translation>
    </message>
    <message>
        <location filename="../qml/NavigationManager.qml" line="191"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../qml/Preferences.qml" line="35"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="86"/>
        <source>External Midi Keyboard</source>
        <translation>Clavier MIDI externe</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="96"/>
        <source>Audio Output Device</source>
        <translation>Périphérique de sortie audio</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="119"/>
        <source>External Midi Output Device</source>
        <translation>Dispositif de sortie Midi externe</translation>
    </message>
    <message>
        <location filename="../qml/Preferences.qml" line="126"/>
        <source>System settings</source>
        <translation>Paramètres du système</translation>
    </message>
</context>
<context>
    <name>ProgressIndicator</name>
    <message>
        <location filename="../qml/ProgressIndicator.qml" line="132"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>RunGuard</name>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="168"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="169"/>
        <source>Just Intonation is already running.</source>
        <translation>&quot;Just Intonation&quot; est déjà en cours d&apos;exécution.</translation>
    </message>
    <message>
        <location filename="../../modules/system/runguard.cpp" line="170"/>
        <source>Please close the application before restarting.</source>
        <translation>Veuillez fermer l&apos;application avant de redémarrer.</translation>
    </message>
</context>
<context>
    <name>Simple</name>
    <message>
        <location filename="../qml/Simple.qml" line="60"/>
        <source>Play a Midi demo or drop a Midi file here</source>
        <translation>Jouez une démo MIDI ou déposez un fichier MIDI ici</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="71"/>
        <source>Help and General Information</source>
        <translation>Aide et informations générales</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="76"/>
        <source>Play Music on a Touchscreen Keyboard</source>
        <translation>Jouer de la musique sur un clavier tactile</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="81"/>
        <source>Modify Preferences</source>
        <translation>Modifier les préférences</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="86"/>
        <source>Enter Expert Mode</source>
        <translation>Entrez le mode Expert</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="143"/>
        <source>Instrument:</source>
        <translation>Instrument:</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="197"/>
        <source>Equal Temperament</source>
        <translation>Tempérament égal</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="198"/>
        <source>Just Intonation</source>
        <translation>Gamme naturelle</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="212"/>
        <source>Delayed Tuning</source>
        <translation>Accordage retardé</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="234"/>
        <source>Compensate Pitch Drift</source>
        <translation>Compenser la dérive de fréquence</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="317"/>
        <source>Downloading Instruments</source>
        <translation>Téléchargement d&apos;instruments</translation>
    </message>
    <message>
        <location filename="../qml/Simple.qml" line="374"/>
        <source>Tap this button to hear a demo</source>
        <translation>Cliquez sur ce bouton pour écouter une démo</translation>
    </message>
</context>
<context>
    <name>SystemSettings</name>
    <message>
        <location filename="../qml/SystemSettings.qml" line="40"/>
        <source>Welcome screen on startup</source>
        <translation>Afficher l&apos;écran d&apos;accueil</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="48"/>
        <source>Allow download of samples</source>
        <translation>Téléchargement d&apos;échantillons</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="61"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="80"/>
        <source>Forced instrument download</source>
        <translation>Téléchargement d&apos;instrument forcé</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="102"/>
        <source>Restore default settings</source>
        <translation>Restaurer les paramètres par défaut</translation>
    </message>
    <message>
        <location filename="../qml/SystemSettings.qml" line="103"/>
        <source>Reset all settings and restart the application</source>
        <translation>Réinitialiser tous les paramètres et redémarrer l&apos;application</translation>
    </message>
</context>
<context>
    <name>Temperament</name>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Semitone</source>
        <translation>Seconde mineure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Major Second</source>
        <translation>Seconde majeure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="36"/>
        <source>Minor Third</source>
        <translation>Tierce mineure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Major Third</source>
        <translation>Tierce majeure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Perfect Fourth</source>
        <translation>Quarte juste</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="37"/>
        <source>Tritone</source>
        <translation>Tritone</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Perfect Fifth</source>
        <translation>Quinte juste</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Minor Sixth</source>
        <translation>Sixte mineure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="38"/>
        <source>Major Sixth</source>
        <translation>Sixte majeure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Minor Seventh</source>
        <translation>Septième mineure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Major Seventh</source>
        <translation>Septième majeure</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="39"/>
        <source>Octave</source>
        <translation>Octave</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="70"/>
        <source>Static Tuning</source>
        <translation>Accordage statique</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>A#</source>
        <translation>A#</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="88"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="106"/>
        <source>Subminor Seventh 7:4</source>
        <translation>Septième naturelle 7:4</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="166"/>
        <source>Enter temperament name</source>
        <translation>Nom du tempérament</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="258"/>
        <source>Reset weights</source>
        <translation>Réinitialiser poids</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="187"/>
        <source>Open a temperament file</source>
        <translation>Ouvrez un fichier de tempérament</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="206"/>
        <source>Save a temperament file</source>
        <translation>Enregistrer un fichier de tempérament</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="241"/>
        <source>Set all interval size deviations to zero (equal temperament)</source>
        <translation>Réglez tous les écarts de taille d&apos;intervalle à zéro (tempérament égal)</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="259"/>
        <source>Restore recommended standard values for the weights</source>
        <translation>Restaurer les valeurs standard recommandées pour les facteurs de poids</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="288"/>
        <source>Interval:</source>
        <translation>Intervalle:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="296"/>
        <source>Deviation:</source>
        <translation>Déviation:</translation>
    </message>
    <message>
        <location filename="../qml/Temperament.qml" line="306"/>
        <source>Weight:</source>
        <translation>Poids:</translation>
    </message>
</context>
<context>
    <name>TextHeader</name>
    <message>
        <location filename="../qml/TextHeader.qml" line="108"/>
        <source>Modify Preferences</source>
        <translation>Modifier les préférences</translation>
    </message>
    <message>
        <location filename="../qml/TextHeader.qml" line="120"/>
        <source>Close window</source>
        <translation>Fermer la fenêtre</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../../modules/system/translator.cpp" line="133"/>
        <source>Automatic</source>
        <translation>Automatique</translation>
    </message>
</context>
<context>
    <name>TuningMode</name>
    <message>
        <location filename="../qml/TuningMode.qml" line="43"/>
        <source>Tuning delay</source>
        <translation>Accordage retardé</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="78"/>
        <source>Memory</source>
        <translation>Mémoire</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="115"/>
        <source>Drift correction</source>
        <translation>Compensation de dérive</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="215"/>
        <source>cent</source>
        <translation>cent</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="279"/>
        <source>tempered</source>
        <translation>tempéré</translation>
    </message>
    <message>
        <location filename="../qml/TuningMode.qml" line="289"/>
        <source>just</source>
        <translation>naturelle</translation>
    </message>
</context>
<context>
    <name>Voice</name>
    <message>
        <location filename="../../instrument/voice.cpp" line="64"/>
        <source>Please wait...</source>
        <translation>Patientez...</translation>
    </message>
    <message>
        <location filename="../../instrument/voice.cpp" line="149"/>
        <source>Reading scale</source>
        <translation>Lecture de la gamme</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../qml/Welcome.qml" line="52"/>
        <source>Hello!</source>
        <translation>Bienvenue!</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="64"/>
        <source>This application gives you the opportunity to play in just intonation independent of scale.</source>
        <translation>Cette application vous donne la possibilité de jouer dans la gamme naturelle, indépendante de la clé.</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="85"/>
        <source>Start</source>
        <translation>Démarrez</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="86"/>
        <source>Start Just Intonation</source>
        <translation>Démarrez l&apos;application &quot;Just Intonation&quot;</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="97"/>
        <source>Learn more</source>
        <translation>En savoir plus</translation>
    </message>
    <message>
        <location filename="../qml/Welcome.qml" line="98"/>
        <source>Visit our website to learn more about Just Intonation</source>
        <translation>Visitez notre site Web pour en savoir plus sur &quot;Just Intonation&quot;</translation>
    </message>
</context>
</TS>
