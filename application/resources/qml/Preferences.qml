/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 2.4

Item {
    id: preferences
    objectName: "preferences"

    property int scaling: Math.max(parent.width,parent.height)*0.02;

    property double scale: landscape ? width/2 : width
    property double fontsize: landscape ? window.width*0.03 : window.fontsize*1.2

    TextHeader {
        id: header
        showPreferencesButton: false
        text: qsTr("Preferences")
    }

    Item {
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: preferences.right
        anchors.bottom: preferences.bottom
        anchors.margins: preferences.scale*0.005

        Flickable {
            id: area
            flickableDirection: Flickable.VerticalFlick
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: landscape ? width*0.725 : parent.height
            contentWidth: parent.width
            contentHeight: landscape ? 1.0*parent.width : 3.0*parent.width
            clip: true

//            anchors.rightMargin: 10
//            ScrollBar.vertical: ScrollBar {
//                 parent: area.parent
//                 anchors.top: area.top
//                 anchors.left: area.right
//                 anchors.bottom: area.bottom
//             }


            property double dx: width*0.005
            property double w: landscape ? width/2-dx*1.3 : width-dx
            property double h: w*0.39
            property double hext: h+2*dx

            Grid {
                anchors.fill: parent
                anchors.margins: area.dx
                columns: landscape ? 2 : 1
                spacing: area.dx

                Item {
                    id: item1
                    width: area.w
                    height: area.h*3.76    // This is the height in portrait mode ****

                    // MIDI INPUT
                    XBox {
                        id: firstbox
                        width: parent.width
                        height: area.h
                        title: qsTr("External Midi Keyboard")
                        fontsize: preferences.fontsize
                        MidiInput {
                             anchors.fill: parent.subarea
                        }
                    }


                    // AUDIO OUTPUT
                    XBox {
                        title: qsTr("Audio Output Device")
                        width: area.w
                        height: area.h*2.7
                        anchors.top: firstbox.bottom;
                        anchors.topMargin: area.dx
                        fontsize: preferences.fontsize
                        AudioOutput {
                             anchors.fill: parent.subarea
                        }
                    }
                }

                Item {
                    width: area.w
                    height: item1.height

                    // MIDI OUTPUT
                    XBox {
                        id: secondbox
                        width: parent.width
                        height: area.h
                        //anchors.top: firstbox.bottom;
                        //anchors.topMargin: area.dx
                        title: qsTr("External Midi Output Device")
                        fontsize: preferences.fontsize
                        MidiOutput {
                             anchors.fill: parent.subarea
                        }
                    }
                    XBox {
                        title: qsTr("System settings")
                        width: area.w
                        height: area.h*2.7
                        anchors.top: secondbox.bottom;
                        anchors.topMargin: area.dx
                        fontsize: preferences.fontsize
                        SystemSettings {
                            id: systemssettings
                            anchors.fill: parent.subarea
                        }
                    }
                }
            }
        }
        Rectangle {
            id: separator2
            visible: landscape
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: area.bottom
            height: width*0.01
            gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#ffffff"
                }
                GradientStop {
                    position: 1
                    color: "#000000"
                }
            }
        }
        Image {
            visible: landscape
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: separator2.bottom
            //anchors.bottom: parent.bottom
            source: "images/headerbackground.jpg"
        }
    }

}
