/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.4
import Qt.labs.settings 1.0

Item {
    id: midiinput

    readonly property int iconsize: parent.height*0.55

    Image {
        id: image
        anchors.right: parent.right
        anchors.rightMargin: parent.width*0.02
        width: iconsize
        height: iconsize
        anchors.verticalCenter: parent.verticalCenter
        fillMode: Image.PreserveAspectFit
        source: "icons/midiin.png"
        smooth: true
        mipmap: true
    }

    Item {
        id: field
        anchors.top: midiinput.top
        anchors.right: image.left
        anchors.left: midiinput.left
        anchors.bottom: midiinput.bottom
        anchors.topMargin: midiinput.height*0.05
        anchors.bottomMargin: midiinput.height*0.05
        anchors.rightMargin: midiinput.width*0.03
        anchors.leftMargin: midiinput.width*0.03

        Text {
            id: currentDeviceTextfield
            anchors.top: field.top
            anchors.topMargin: field.height*0.05
            anchors.horizontalCenter: field.horizontalCenter
            font.bold: true
            color: "darkblue"
            font.family: window.font
            font.pixelSize: parent.height*0.18
            text: window.selectedMidiInputDevice
        }

        XCheckBox {
            id: checkbox
            checked: window.midiInputAutomaticMode
            height: midiinput.height*0.27
            text: qsTr(" Automatic Mode")
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.3
            anchors.horizontalCenter: field.horizontalCenter
            fontsize: parent.height*0.23
            onClicked: {
                midiInputAutomaticMode = checked;
                onAutomaticMidiInputModeChanged(midiInputAutomaticMode)
            }
        }

        XCombo {
            id: availableDevices
            fontsize: parent.height*0.22
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height*0.33
            visible: ! checkbox.checked
            model: window.availableMidiInputDevices
            onCurrentIndexChanged: {
                if (currentIndex>0) {
                    onSelectedMidiInputDeviceNameChanged
                            (currentIndex == 1 ? "" : currentText)
                }
            }
        }
        XText {
            text: qsTr("In the automatic mode new Midi devices are recognized automatically")
            horizontalAlignment: Text.AlignHCenter
            anchors.left: field.left
            anchors.right: field.right
            anchors.leftMargin: parent.width*0.03
            anchors.rightMargin: parent.width*0.03
            y: field.height*0.65
            visible: checkbox.checked
            font.pixelSize: parent.height*0.12
        }
    }
}
