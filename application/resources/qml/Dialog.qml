/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

Item {
    id: dialog
    property alias title: header.text
    property alias text: dialogtext.text
    property alias details: dialogdetails.text
    property int type: 0 // 0: yes/no 1: OK
    property alias yes: yes
    property alias no: no
    property bool ok: false

    TextHeader {
        id: header
        text: "Dialog Title"
    }

    Item {
        id: middlepanel
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.width*0.06
        anchors.rightMargin: parent.width*0.06
        anchors.topMargin: parent.height*0.03
        anchors.bottomMargin: parent.height*0.3

        XText {
            id: dialogtext
            anchors.fill: parent
            anchors.bottomMargin: parent.height * 0.6
            text: "Dialog Text"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: window.fontsize
            spacingFactor: 1.2
        }

        XText {
            id: dialogdetails
            anchors.fill: parent
            anchors.topMargin: parent.height * 0.48
            text: "Dialog Details"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: window.fontsize*0.5 + window.height*0.008
            spacingFactor: 1.1
        }
    }

    Row
    {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.1
        property int itemWidth: scalefactor*0.45
        property int itemHeight: scalefactor*0.12
        spacing: parent.width * (1-parent.height/(parent.height+parent.width))*0.4

        XButton {
            id: yes
            width: parent.itemWidth
            height: parent.itemHeight
            fontsize: width*0.15
            text: type==0 ? qsTr("Yes") : qsTr("OK")
            onClicked: { stack.pop(); ok=true; }
        }

        XButton {
            id: no
            visible: type==0
            width: parent.itemWidth
            height: parent.itemHeight
            fontsize: width*0.15
            text: qsTr("No")
            onClicked: { stack.pop(); ok=false; }
        }
    }
}
