/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

Item {
    id: learnmore
    objectName: "learnmore"

    TextHeader {
        showPreferencesButton: false
        id: header
        text: qsTr("Learn more")
    }

    readonly property int iconheight: height*0.08 + width*0.02

    Item {
        anchors.fill: parent
        anchors.topMargin: header.height

        Column
        {
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.37 - parent.height*0.15
            anchors.right: parent.right
            spacing: parent.height * 0.05

            // GETTING STARTED

            LearnMoreEntry {
                source: "icons/help.png"
                onClicked: stack.push(gettingstarted)
                tooltip: qsTr("Basic Help for Getting Started")
                text: qsTr("Getting started")
            }

            // DOWNLOAD MANUAL

            LearnMoreEntry {
                source: "icons/documentation.png"
                onClicked: downloadManual()
                tooltip: qsTr("Download User Manual from the Internet")
                text: qsTr("Download manual")
            }


            // VISIT WEBSITE

            LearnMoreEntry {
                source: "icons/website.png"
                tooltip: qsTr("Visit Project Website on the Internet")
                onClicked: Qt.openUrlExternally(homepageUrl)
                text: qsTr("Visit website")
            }


            // DOWNLOAD SCIENTIFIC DETAILS

            LearnMoreEntry {
                source: "icons/paper.png"
                tooltip: qsTr("Download Scientific Documentation describing Details of the Tuning Method")
                onClicked: Qt.openUrlExternally("https://arxiv.org/abs/1706.04338")
                text: qsTr("Tuning Method")
            }


            // GET INFO ABOUT THIS APP

            LearnMoreEntry {
                source: "icons/info.png"
                onClicked: stack.push(about)
                tooltip: qsTr("Get more Information about this Application")
                 text: qsTr("About this App")
            }
        }
    }
}
