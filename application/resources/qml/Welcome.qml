/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0
import QtQuick.Controls.Styles 1.4

// Welcome page, shown on startup

Item {
    id: welcome
    objectName: "welcome"

    Header {
        id: header
        anchors.left: parent.left
        anchors.right: parent.right
        height: scalefactor*0.3
    }

    Item {
        id: middlepanel
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: parent.width*0.06
        anchors.rightMargin: parent.width*0.06
        anchors.topMargin: parent.height*0.03
        anchors.bottomMargin: parent.height*0.3

        Text {
            anchors.fill: parent
            anchors.bottomMargin: parent.height/2
            text: qsTr("Hello!")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.WordWrap
            font.family: window.font
            font { family: "Times"; capitalization: Font.AllUppercase }
            font.pixelSize: window.fontsize * 1.2
        }

        XText {
            anchors.fill: parent
            anchors.topMargin: parent.height/3
            text: qsTr("This application gives you the opportunity to play in just intonation independent of scale.")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: window.fontsize*0.8
            spacingFactor: 1.2
        }
    }

    Row
    {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: window.height*0.17
        property int itemWidth: scalefactor*0.5
        property int itemHeight: scalefactor*0.14
        spacing: parent.width * (1-parent.height/(parent.height+parent.width))*0.2

        XButton {
            width: parent.itemWidth
            height: parent.itemHeight
            fontsize: width*0.14
            text: qsTr("Start")
            tooltip: qsTr("Start Just Intonation")
            onClicked: {
                if (firstStart) stack.replace(confirmDownload)
                else stack.replace(simple);
            }
        }

        XButton {
            width: parent.itemWidth
            height: parent.itemHeight
            fontsize: width*0.14
            text: qsTr("Learn more")
            tooltip: qsTr("Visit our website to learn more about Just Intonation")
            onClicked: stack.push(learnmore)
        }
    }
}

