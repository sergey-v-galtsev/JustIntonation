/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.4

StackView {
    id: stack
    initialItem: showWelcome ? welcome : simple

    property alias progressIndicator: progressIndicator

    function messageNoInternet() { stack.push(noInternet); }

    // Implement fading transition
    delegate: StackViewDelegate {
        function transitionFinished(properties)
        {
            properties.exitItem.opacity = 1
        }

        pushTransition: StackViewTransition {
            PropertyAnimation {
                target: enterItem
                property: "opacity"
                from: 0
                to: 1
            }
            PropertyAnimation {
                target: exitItem
                property: "opacity"
                from: 1
                to: 0
            }
        }
    }

    focus: true
    Keys.onPressed: {
        var name = stack.currentItem.objectName
        //console.log(name)
        switch(event.key) {
        case Qt.Key_Escape:         { if (name == "welcome") replace (simple); else pop(); break; }
        case Qt.Key_MediaPlay:      { togglePlayPause(); break; }
        case Qt.Key_MediaPrevious:  { rewind(); break; }
        case Qt.Key_F1:             { if (name != "learnmore") push (learnmore); else pop(); break; }
        case Qt.Key_F9:             { if (name != "preferences") push (preferences); break; }
        case Qt.Key_E:              { if (name != "expert") push (expert); else pop(); break; }
        case Qt.Key_K:              { if (name != "keyboard") push (keyboard); else pop(); break; }
        case Qt.Key_L:              { if (name != "logfile") push (logfile); else pop(); break; }
        case Qt.Key_I:              { if (name != "about") push (about); else pop(); break; }
        case Qt.Key_Space:          { if (name == "welcome") replace (simple); else togglePlayPause();  break; }
        case Qt.Key_Return:         { if (name == "welcome") replace (simple);
                                      else if (name == "simple") toggleJI(); break }
        case Qt.Key_Backspace:
        case Qt.Key_Delete:         { resetPitchProgression(); break; }
        case Qt.Key_Right:          { browseExamples(1); break; }
        case Qt.Key_Left:           { browseExamples(-1); break; }
        case Qt.Key_Up:             { changeTargetFrequency(0.2); break; }
        case Qt.Key_Down:           { changeTargetFrequency(-0.2); break; }
        }
    }

    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            //console.log("Android back key")
            if (stack.currentItem.objectName !== "simple") {
                event.accepted = true;
                pop()
            }
            else {
                console.log("Android back key ignored");
                event.accepted = true;
            }
        }
    }


    Component {
        id: welcome
        Welcome {
        }
    }

    Component {
        id: learnmore
        LearnMore {
        }
    }

    Component {
        id: gettingstarted
        GettingStarted {
        }
    }

    Component {
        id: about
        About {
        }
    }

    Component {
        id: simple
        Simple {
        }
    }

    Component {
        id: keyboard
        Keyboard {
        }
    }

    Component {
        id: logfile
        Logfile {
        }
    }

    Component {
        id: expert
        Expert {
        }
    }

    Component {
        id: preferences
        Preferences {
        }
    }

    Component {
        id: resetPrefDialog
        Dialog {
            title: qsTr("Reset settings")
            text: qsTr("Do you want to reset all settings?")
            details: qsTr("All changes that you made will be lost and the settings will be reset to default values.")
            yes.onClicked:  resetSettings()
        }
    }

    Component {
        id: audioHelpDialog
        Dialog {
            type: 1
            //anchors.fill: parent
            title: qsTr("Help on choosing the audio parameters")
            text: qsTr("The sample rate has to be 44100.")+"<br>"+qsTr("The buffer size should be small.")+"<br>"
            details: qsTr("Buffer size = 0 means to use the default value. Packet size and buffer size should be the same. The buffer size controls the size of the internal audio buffer of your device. If you hear crackling sounds this value should be increased. Try to find the lowest possible value. The packet size controls the number of frames generated by the application in a single packet. In case of latency problems try to reduce this value.")
        }
    }

    Component {
        id: confirmDownload
        Dialog {
            title: qsTr("Confirm Download")
            text:  qsTr("Would you like to hear realistic musical instruments?")
            details: "<strong>" + qsTr("In this case the application will silently download 600 MB of sampled sounds from the internet to your device.")
                     + "</strong><br><br>" + qsTr("If you allow the application to download samples from our server you can hear realistic instruments such as a piano, an organ, or a harpsichord. The samples are downloaded only once and saved locally. Please make sure that your device offers about 600 MB of free disk space. If you deny internet access, the application will only provide artificially generated sounds (triangular waves).")
            yes.onClicked: {
                firstStart = false; allowDownload = true; downloadedInstrument = 0; startDownload(true); stack.replace(simple); }
            no.onClicked:  {
                firstStart = false; allowDownload = false; stack.replace(simple); }
        }
    }

    Component {
        id: noInternet
        Dialog {
            title: qsTr("No Internet Connection");
            text: qsTr("Could not find a valid internet connection for downloading.")
            details: qsTr("Please use a fast internet connection (WLAN or wired).")
            yes.text: qsTr("Retry")
            no.text: qsTr("Cancel")
            yes.onClicked: { stack.pop(); downloadedInstrument = 0; startDownload(true);  }
            no.onClicked: { stack.pop(); }
        }
    }

    Component {
        id: progressIndicator
        ProgressIndicator {
        }
    }


//    Component.onCompleted: push(welcome)
}
