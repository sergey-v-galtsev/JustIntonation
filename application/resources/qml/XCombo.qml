/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Private 1.0

ComboBox {
    id: scaledcombo
    property double fontsize: 10
    // activeFocusOnPress: true

    // Disable mouse wheel events on XCombo boxes
    MouseArea {
        anchors.fill: parent
        onWheel: {
            // do nothing
        }
        onPressed: {
            // propogate to ComboBox
            mouse.accepted = false;
        }
        onReleased: {
            // propogate to ComboBox
            mouse.accepted = false;
        }
    }


    style: ComboBoxStyle {
        id: combostyle
        background: Item {
            id: rectCategory
            Rectangle {
                id: rect
                anchors.fill: parent
                anchors.margins: -parent.height*0.05
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "white" }
                    GradientStop { position: 1 ; color: "black" }
                }
                radius: window.radius*1.2
            }
            Rectangle {
                anchors.fill: parent
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                    GradientStop { position: 1 ; color: control.pressed ? "#888" : "#aaa" }
                }
                border.color: "#777"
                radius: window.radius
            }

            Image {
                 id: image
                 source: "icons/down.png"
                 anchors.bottom: parent.bottom
                 anchors.right: parent.right
                 anchors.rightMargin: height*0.45
                 anchors.bottomMargin: height*0.45
                 fillMode: Image.PreserveAspectFit
                 height: parent.height*0.5
            }
        }
        label: Text {
            verticalAlignment: Text.AlignVCenter
            //horizontalAlignment: Text.AlignHCenter
            x: (scaledcombo.width-height-contentWidth)/2
            font.family: window.font
            color: "black"
            text: control.currentText
            font.pixelSize: scaledcombo.fontsize
            //wrapMode: Text.WordWrap
        }

        property Component __dropDownStyle: MenuStyle {
            id: mystyle
            __menuItemType: "comboboxitem"


            frame: Rectangle {              // background
                color: "white"
                border.width: 2
                radius: window.radius*0.5
            }


            itemDelegate.label:             // an item text
                Text {
                width: parent.contentWidth
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: fontsize
                font.family: window.font
                color: styleData.selected ? "white" : "black"
                text: styleData.text
            }

            itemDelegate.background: Rectangle {
                radius: window.radius*0.5
                color: styleData.selected ? "darkBlue" : "transparent"
            }
        }
    }
    onHeightChanged: font.pixelsize = fontsize
}
