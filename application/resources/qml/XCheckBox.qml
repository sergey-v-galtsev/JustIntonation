/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

CheckBox {
    id: scaledcheckbox
    height: 50
    property int fontsize: height*0.6

    style: CheckBoxStyle {

        indicator: Item {
            implicitWidth: scaledcheckbox.height*0.8
            implicitHeight: scaledcheckbox.height*0.8
            Rectangle {
                anchors.fill: parent
                anchors.margins: -parent.height*0.05
                radius: scaledcheckbox.height*0.5
                gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: "#eee"
                    }
                    GradientStop {
                        position: 1
                        color: "#000"
                    }
                }
            }
            Rectangle {
                anchors.fill: parent
                radius: scaledcheckbox.height*0.5
                border.color: control.activeFocus ? "black" : "gray"
                border.width: 1
            }
            Rectangle {
                visible: control.checked
                color: "darkred"
                border.color: "lightgray"
                radius: scaledcheckbox.height*0.5*0.91
                anchors.margins: parent.height*0.03
                anchors.fill: parent
            }
        }
        label: XText {
            spacingFactor: 1.0
            verticalAlignment: Text.AlignVCenter
            color: "black"
            opacity: scaledcheckbox.checked ? 1 : 0.2
            text: scaledcheckbox.text
            font.pixelSize: fontsize
        }
        spacing: parent.height*0.4
    }
}
