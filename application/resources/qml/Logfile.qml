/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property double fontsize: landscape ? parent.width*0.02 : parent.width*0.06
    readonly property string headline: "TIME  --------- MESSAGE"

    Item {
        id: logfileheadline
        width: parent.width
        height: parent.width * (landscape ? 0.06 : 0.15)


        Image {
            anchors.fill: parent
            source: "images/toptoolbar.jpg"
        }
        Row {
            id: leftrow
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.margins: parent.height*0.2
            spacing: parent.height*(landscape ?  0.5:0.1)
            XButton {
                id: clearbutton
                height: parent.height
                width: height*1.8
                text: qsTr("Clear")
                fontsize: height*0.4
                onClicked: textfield.text = headline
                tooltip: qsTr("Clear log file")
            }
            XButton {
                id: copybutton
                height: parent.height
                width: height*1.8
                text: qsTr("Copy")
                tooltip: qsTr("Copy log file to the clipboard")
                fontsize:  height*0.4
                onClicked: { textfield.selectAll(); textfield.copy(); }
            }

            XToolButton {
                source: "icons/rewind.png"
                onClicked: window.rewind()
                radius: 2
                tooltip: qsTr("Rewind: Go back to the beginning")
            }
            XToolButton {
                width: height
                source: playing ? "icons/pause.png" : "icons/play.png"
                onClicked: window.togglePlayPause()
                radius: 2
                tooltip: qsTr("Play / Pause")
            }

        }


        XToolButton {
            id: iconrow
            anchors.right: parent.right
            height: parent.height
            source: "icons/cancel.png"
            tooltip: qsTr("Close Logfile")
            onClicked: stack.pop()
        }
        Text {
            text: qsTr("Logfile")
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: leftrow.right
            anchors.right: iconrow.left
            font.family: window.font
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: landscape ? parent.width*0.03 : parent.width*0.03
        }
    }
    Rectangle {
        id: separator
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: logfileheadline.bottom
        height: logfileheadline.height*0.1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }
    TextArea {
        id: textfield
        textFormat: TextEdit.RichText
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: separator.bottom
        anchors.margins: parent.width*0.008
        text: headline
        font.family: window.font
        font.pixelSize: landscape ? parent.width*0.017 : parent.width*0.025
        property string line: window.newLogMessage
        onLineChanged: newLogMessage == "" ? textfield.text=headline : textfield.append(newLogMessage)
    }

    Component.onCompleted: activateLogFile(true)
    Component.onDestruction: activateLogFile(false)
}
