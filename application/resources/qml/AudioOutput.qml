/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
    id: audiooutput

    property int scalefactor: Math.min(parent.height, parent.width*0.4)
    property int fontsize: parent.width*0.06
    property int hspace: width*0.03
    property int itemsize: width*0.1
    property int columnspacing: parent.height*0.0525

    readonly property var buffersizeModel: [0,32,64,128,256,512,1024,2048,4096,8192,16384,32768]
    readonly property var packetsizeModel: [32,64,128,256,512,1024,2048,4096,8192]

    Column
    {
        anchors.fill: parent
        spacing: columnspacing
        anchors.rightMargin: hspace
        anchors.leftMargin: hspace
        anchors.topMargin: parent.height*0.03
        XText {
            id: currentDevice
            anchors.left: parent.left
            anchors.right: parent.right
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "darkblue"
            font.pixelSize: audiooutput.fontsize*0.85
            font.family: window.font
            text: window.currentAudioDevice
            height: audiooutput.fontsize*1.7
        }
        XCombo {
            id: availableDevices
            anchors.left: parent.left
            anchors.right: parent.right
            height: itemsize
            model: window.audioDeviceList
            fontsize: audiooutput.fontsize
            property int currentAudioDeviceIndex: window.currentAudioDeviceIndex
            onCurrentAudioDeviceIndexChanged: currentIndex = currentAudioDeviceIndex
            onCurrentIndexChanged: selectedAudioDeviceIndex = currentIndex
        }
        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            height: itemsize
            Text {
                id: textchoose
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: sampleRates.verticalCenter
                text: qsTr("Sample rate:")
                font.family: window.font
                color: "black"
                font.pixelSize: audiooutput.fontsize
            }
            XCombo {
                id: sampleRates
                width: fontsize*4.5
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.65
                anchors.right: parent.right
                height: itemsize
                fontsize: audiooutput.fontsize
                model: window.sampleRateList
                currentIndex: currentSampleRateIndex
                property int currentSampleRateIndex: window.currentSampleRateIndex
                onCurrentSampleRateIndexChanged: currentIndex = currentSampleRateIndex
                onCurrentIndexChanged: selectedSampleRateIndex = currentIndex
            }
        }
        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            height: itemsize
            Text {
                id: buffersizetext
                anchors.left: parent.left
                anchors.verticalCenter: audiohelp.verticalCenter
                text: qsTr("Buffer size:")
                font.family: window.font
                color: "black"
                font.pixelSize: audiooutput.fontsize
            }
            XButton {
                id: audiohelp
                text: "?"
                height: itemsize
                width: parent.height*1.414
                fontsize: audiooutput.fontsize
                anchors.left: buffersizetext.right
                anchors.leftMargin: parent.width*0.02
                anchors.top: parent.top
                onClicked: stack.push(audioHelpDialog)
                tooltip: qsTr("Show more help on audio settings")
            }

            XCombo {
                id: buffersize
                width: fontsize*4.5
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.65
                anchors.right: parent.right
                height: itemsize
                fontsize: audiooutput.fontsize
                model: audiooutput.buffersizeModel
                property int currentAudioBufferSize: window.currentAudioBufferSize
                onCurrentAudioBufferSizeChanged:
                    currentIndex = model.indexOf(currentAudioBufferSize)
                onCurrentIndexChanged: if (currentIndex >= 0)
                                           selectedAudioBufferSize = model[currentIndex]
            }
        }
        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            height: itemsize
            Text {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.verticalCenter: packetsize.verticalCenter
                text: qsTr("Packet size:")
                font.family: window.font
                color: "black"
                font.pixelSize: audiooutput.fontsize
            }
            XCombo {
                id: packetsize
                width: fontsize*4.5
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.65
                anchors.right: parent.right
                height: itemsize
                fontsize: audiooutput.fontsize
                model: audiooutput.packetsizeModel
                property int currentAudioPacketSize: window.currentAudioPacketSize
                onCurrentAudioPacketSizeChanged:
                    currentIndex = model.indexOf(currentAudioPacketSize)
                onCurrentIndexChanged: if (currentIndex >= 0)
                                       selectedAudioPacketSize = model[currentIndex]
            }
        }
        Item {
            anchors.right: parent.right
            anchors.left: parent.left
            height: itemsize
            XButton {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.rightMargin: parent.width*0.72
                height: itemsize
                text: qsTr("Reset")
                fontsize: audiooutput.fontsize
                onClicked: resetAudioParameters()
                tooltip: qsTr("Reset audio settings to default values")
            }
            XButton {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.31
                height: itemsize
                text: qsTr("Apply Changes")
                tooltip: qsTr("Apply the audio settings specified in this panel")
                fontsize: audiooutput.fontsize
                onClicked: {
                    console.log("Apply audio changes: ",availableDevices.index," sr-index =",
                                selectedSampleRateIndex, " packetsize =",
                                packetsize.model[packetsize.currentIndex], " buffersize =",
                                buffersize.model[buffersize.currentIndex]);
                    selectAudioParameters(selectedAudioDeviceIndex,
                                          selectedSampleRateIndex,
                                          buffersize.model[buffersize.currentIndex],
                                          packetsize.model[packetsize.currentIndex]);
                    currentAudioPacketSize = packetsize.model[packetsize.currentIndex];
                    currentAudioBufferSize = buffersize.model[buffersize.currentIndex];
                }
            }
        }
    }
}
