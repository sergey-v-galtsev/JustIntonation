/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

// Show a header line with a tile, the logo, and an exit button.

Item {
    property alias text: headline.text
    property alias tooltip: closebutton.tooltip
    property alias closebutton: closebutton
    property bool showPreferencesButton: true

    width: parent.width
    height: Math.max(parent.width*0.035+parent.height*0.07,
                     parent.height*0.1);


    Image {
        anchors.fill: parent
        source: "images/toptoolbar.jpg"
    }

    Rectangle {
        id: sep1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: parent.height*0.08
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

    Rectangle {
        id: sep2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: parent.height*0.08
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }


    Image {
        width: parent.height*0.8
        height: width
        x: parent.height*0.05
        y: parent.height*0.1
        source: "qrc:/logo/logoframed_128x128.png"
    }

    Text {
        id: headline
        text: "Headline undefined"
        anchors.fill: parent
        anchors.bottom: parent.top
        anchors.leftMargin: parent.height
        anchors.rightMargin: parent.height
        anchors.horizontalCenter: parent.horizontalCenter
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        font.family: window.font
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: Math.min(parent.height*0.23 + parent.width*0.02,
                                 1.1*parent.width/text.length)
    }

    XToolButton {
        id: preferencesbutton
        visible: showPreferencesButton
        height: parent.height*0.8
        width: height
        x: parent.width-2*height
        y: parent.height*0.1
        tooltip: qsTr("Modify Preferences")
        source: "icons/settings.png"
        onClicked: stack.push(preferences)
    }

    XToolButton {
        id: closebutton
        height: parent.height*0.8
        width: height
        x: parent.width-height
        y: parent.height*0.1
        source: "qrc:/icons/cancel.png"
        tooltip: qsTr("Close window")
        onClicked: stack.pop()
    }
}
