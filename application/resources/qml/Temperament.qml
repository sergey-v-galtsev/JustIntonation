/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Item {
    id: temperament
    objectName: "temperament"

    // Provisorisch:
    property int vsize: width*0.08
    property int margin: width*0.015

    property alias index: temperamentComboBox.currentIndex
    property alias staticTuning: staticTuningCheckBox.checked

    readonly property var intervals:
        [qsTr("Semitone"),qsTr("Major Second"), qsTr("Minor Third"),
         qsTr("Major Third"),qsTr("Perfect Fourth"), qsTr("Tritone"),
         qsTr("Perfect Fifth"),qsTr("Minor Sixth"), qsTr("Major Sixth"),
         qsTr("Minor Seventh"), qsTr("Major Seventh"),qsTr("Octave")]

    anchors.fill: parent
    XCombo {
        id: temperamentComboBox
        anchors.left: temperament.left
        anchors.right: temperament.right
        anchors.top: temperament.top
        anchors.margins: margin
        height: vsize*1.4
        fontsize: expert.fontsize
        model: temperamentNames
        property int localIndex : temperamentIndex
        currentIndex: localIndex
        onCurrentIndexChanged: {
            temperamentIndex = currentIndex
        }
        onLocalIndexChanged: {
            currentIndex = localIndex
        }
    }

    XCheckBox {
        id: staticTuningCheckBox
        visible: temperamentIndex>0
        anchors.left: parent.left
        anchors.leftMargin: margin
        anchors.top: temperamentComboBox.bottom
        anchors.topMargin: parent.height*0.02
        width: parent.width * 0.74
        height: vsize*1.2
        text: qsTr("Static Tuning");
        fontsize: expert.fontsize
        checked: false
        onCheckedChanged: setStaticTuning (checked,staticTuningOffset)
    }

    XCombo {
        id: referenceTone
        anchors.right: parent.right
        anchors.rightMargin: margin
        anchors.verticalCenter: staticTuningCheckBox.verticalCenter
        //anchors.top: temperamentComboBox.bottom
        //anchors.topMargin: parent.height*0.022
        width: parent.width*0.25
        height: vsize
        fontsize: expert.fontsize
        enabled: staticTuningCheckBox.checked
        opacity: staticTuningCheckBox.checked ? 1 : 0.2
        model: ["C","C#","D","D#","E","F","F#","G","G#","A",qsTr("A#"),qsTr("B")]
        currentIndex: staticTuningOffset
        onCurrentIndexChanged: {
            staticTuningOffset = currentIndex
            setStaticTuning (staticTuningCheckBox.checked,currentIndex)
        }
    }

    XCheckBox {
        id: seventhCheckBox
        anchors.left: parent.left
        anchors.leftMargin: margin
        anchors.top: staticTuningCheckBox.bottom
        //anchors.topMargin: parent.height*0.02
        enabled: temperamentIndex==1 && staticTuningCheckBox.checked
        visible: enabled
        width: parent.width * 0.74
        height: vsize*1.2
        text: qsTr("Subminor Seventh 7:4");
        fontsize: expert.fontsize
        checked: subminorSeventh
        onCheckedChanged: {
            subminorSeventh = checked
            if (visible) setTemperament(1)
        }
    }

//    Rectangle {
//        anchors.left: parent.left
//        anchors.right: parent.right
//        anchors.rightMargin: parent.width*0.42
//        anchors.leftMargin: margin
//        anchors.top: staticTuningCheckBox.bottom
//        anchors.topMargin: vsize*0.2
//        radius: vsize*0.2
//        height: vsize
//        color: "white"
//        border.color: "#333"
//        border.width: vsize*0.05

//        TextField {
//            style: TextFieldStyle {
//                textColor: "black"
//                background: Item{}
//            }
//            y:-vsize*0.3
//            //x: parent.height*0.15
//            //y: parent.height*0.15
//            //height: parent.height*0.7
//            width: parent.width-parent.height*0.3
//            font.family: window.font
//            font.pointSize: vsize*0.8

//        }
//    }

    TextField {
        id: customTemparamentTextfield
        visible: temperamentIndex==customTemperamentIndex
        style: TextFieldStyle {
            textColor: "black"
            background: Rectangle {
                radius: vsize*0.2
                y:-4
                border.color: "#333"
                border.width: vsize*0.05
            }
        }

        height: vsize
        font.family: window.font
        font.pixelSize: height*0.65
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: parent.width*0.42
        anchors.leftMargin: margin
        anchors.top: staticTuningCheckBox.bottom
        anchors.topMargin: vsize*0.2+3
        placeholderText: qsTr("Enter temperament name")
        property string name : customTemperamentName
        text: customTemperamentName
        onTextChanged: customTemperamentName = text
        onNameChanged: text = customTemperamentName
    }

    // Open temperament file (in custom mode only)
    XToolButton {
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.6
        anchors.top: staticTuningCheckBox.bottom
        anchors.topMargin: vsize*0.1
        visible: temperamentIndex==customTemperamentIndex
        height: vsize*1.4
        width: vsize*1.4
        source: "icons/tango-open.png"
        onClicked:  {
            signalLoadCustomTemperament();
        }
        radius: 2
        tooltip: qsTr("Open a temperament file")
    }

    // Save temperament file (in custom mode only)
    XToolButton {
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.72
        anchors.top: staticTuningCheckBox.bottom
        height: vsize*1.4
        width: vsize*1.4
        visible: temperamentIndex==customTemperamentIndex
        source: "icons/tango-save.png"
        onClicked: {
            var t = customTemperamentName;
            var c = customCents.toString();
            var w = weights.toString();
            signalSaveCustomTemperament (t,c,w)
        }
        radius: 2
        tooltip: qsTr("Save a temperament file")
    }


//    XButton {
//        // Reset to just intonation (in custom mode only)
//        anchors.left: parent.left
//        anchors.right: parent.right
//        anchors.rightMargin: parent.width*0.75
//        anchors.leftMargin: margin
//        anchors.verticalCenter: seventhCheckBox.verticalCenter
//        visible: temperamentIndex==customTemperamentIndex
//        height: vsize
//        text: qsTr("Copy JI")
//        tooltip: qsTr("Copy the default interval sizes of Just Intonation");
//        fontsize: expert.fontsize
//        onClicked: {
//            subminorSeventh = false;
//            setTemperament(1); customCents=cents;
//            setTemperament(customTemperamentIndex)
//        }
//    }


    // Reset to ET (in custom mode only)
    XButton {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.rightMargin: margin
        anchors.leftMargin: parent.width*0.84
        anchors.top: staticTuningCheckBox.bottom
        anchors.topMargin: vsize*0.2
        visible: temperamentIndex==customTemperamentIndex
        height: vsize
        text: "¢=0"
        tooltip: qsTr("Set all interval size deviations to zero (equal temperament)")
        fontsize: expert.fontsize
        onClicked: {
            setTemperament(0); customCents=cents;
            setTemperament(customTemperamentIndex)
        }
    }

    // Reset weights (not visible in custom mode)
    XButton {
        anchors.left: parent.left
        anchors.leftMargin: parent.width/2
        anchors.right: referenceTone.right
        visible: temperamentIndex!=customTemperamentIndex
        anchors.top: staticTuningCheckBox.bottom
        anchors.topMargin: vsize*0.2
        height: vsize
        text: qsTr("Reset weights")
        tooltip: qsTr("Restore recommended standard values for the weights")
        fontsize: expert.fontsize
        onClicked: {
            for (var i=0; i<12; ++i)
                centsettings.itemAt(i).weight = initialWeights[i]
            //sendAllCentsAndWeights()
        }
    }

    Item {
        id: headline
        anchors.top: seventhCheckBox.bottom
        anchors.left: temperament.left
        anchors.right: temperament.right
        anchors.leftMargin: margin
        anchors.rightMargin: margin
        anchors.topMargin: parent.height*0.02
        height: vsize
        Rectangle {
            opacity: 0.1
            anchors.fill: parent
            color: "black"
            radius: window.radius
        }

        Text {
            height: parent.height
            x:  margin*2
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Interval:")
            font.family: window.font
            font.pixelSize: expert.fontsize*0.7
        }
        Text {
            height: parent.height
            x:  parent.width*0.4
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Deviation:")+" [¢]"
            font.family: window.font
            font.pixelSize: expert.fontsize*0.7
        }
        Text {
            height: parent.height
            verticalAlignment: Text.AlignVCenter
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.rightMargin: margin*7
            text: qsTr("Weight:")
            font.family: window.font
            horizontalAlignment: Text.AlignRight
            font.pixelSize: expert.fontsize*0.7
        }
    }

    Column {
        anchors.top: headline.bottom
        anchors.left: temperament.left
        anchors.right: temperament.right
        anchors.bottom: temperament.bottom
        anchors.margins: margin
        spacing: margin
        Repeater {
            id: centsettings
            model: 12
            Item {
                id: centsettingitem
                height: parent.height/12-margin
                width: temperament.width - margin*2
                property alias weight : slider.value

                Rectangle {
                    color: "black"
                    opacity: 0.1
                    width: parent.width
                    height: parent.height
                    radius: window.radius
                }
                Text {
                    text: intervals[index]
                    verticalAlignment: Text.AlignVCenter
                    font.pixelSize: expert.fontsize*0.7
                    font.family: window.font
                    height: parent.height
                    x: margin * 2
                }
                XSpinBox {
                    id: spinbox
                    minimumValue: -100
                    maximumValue: 100
                    stepSize: 0.01
                    value: cents[index]
                    decimals: 2
                    fontsize: height*0.75
                    enabled: temperamentComboBox.currentIndex == temperamentComboBox.count-1 ? true : false
                    x: parent.width*0.4
                    height: parent.height
                    width: parent.width*0.27
                    onValueChanged: {
                        if (temperamentComboBox.currentIndex == temperamentComboBox.count-1) {
                            cents[index]=customCents[index]=value
                            signalCentsChanged(index+1,value)
                        }
                    }
                }
                XSlider {
                    id: slider
                    x: parent.width*0.7
                    height: parent.height*0.6
                    anchors.verticalCenter: parent.verticalCenter
                    width: parent.width*0.3
                    value: weights[index]
                    onValueChanged: {
                        weights[index] = value
                        signalWeightsChanged(index+1,value)
                    }
                }
            }
        }
    }
}
