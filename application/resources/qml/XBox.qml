/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

Item {
    id: xbox
    property real fontsize: 10
    property alias title: text1.text
    property alias subarea: subarea

    Rectangle {
        anchors.fill: parent
        anchors.margins: fontsize*0.1
        border.color: "black"
        color: "gray"
        opacity: 0.25
        radius: fontsize/3
        z:-5
    }
    Text {
        id: text1
        text: "undefined"
        x: xbox.fontsize*0.5
        y: 0
        height: fontsize*1.1
        font.family: window.font
        verticalAlignment: Text.AlignVCenter
        font.pixelSize: xbox.fontsize*0.83
    }
    Rectangle {
        id: subarea
        anchors.fill: parent
        anchors.topMargin: fontsize*1.1
        anchors.leftMargin: fontsize*0.25
        anchors.bottomMargin: fontsize*0.25
        anchors.rightMargin: fontsize*0.25
        radius: fontsize/3
        border.color: "black"
        color: "gray"
        opacity: 0.21
    }
}
