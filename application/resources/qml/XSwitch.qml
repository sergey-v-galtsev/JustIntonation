/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

// This file describes a horizontally sliding on/off switch

Item {
    height: 40
    width: height*10

    property int buttonsize: window.fontsize
    property int value
    property string leftText: "Off"
    property string rightText: "On"
    property int fontsize: window.fontsize

    function toggle() { value = (value > 0 ? 0 : 1) }

    Switch {
        id: switch1
        width: buttonsize * 2.0
        height: buttonsize * 1.3
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: value < 2 ? 1 : 0.5
        style: SwitchStyle {
            groove: Item {
                implicitWidth: switch1.width
                implicitHeight:switch1.height*0.8
                Rectangle {
                    anchors.fill: parent
                    anchors.margins: -parent.height*0.05
                    radius: switch1.height*0.5
                    gradient: Gradient {
                        GradientStop {
                            position: 0
                            color: "#eee"
                        }
                        GradientStop {
                            position: 1
                            color: "#000"
                        }
                    }
                }
                Rectangle {
                    anchors.fill: parent
                    radius: switch1.height*0.5
                    color: "lightgray"
                    border.color: "gray"
                    border.width: switch1.height*0.03
                }
            }
            handle: Rectangle {
                implicitWidth: switch1.width*0.5
                implicitHeight: switch1.height*0.8
                radius: parent.height*0.5
                color: "darkred"
                border.color: "darkgrey"
                border.width: switch1.height*0.03
            }
        }
    }

    onValueChanged: switch1.checked = value;
    Component.onCompleted:  switch1.checked = value;

    MouseArea {
        id: mouseArea1
        anchors.fill:switch1
        onClicked: { toggle(); }
    }

    MouseArea {
        id: mouseArea2
        anchors.left: parent.left
        anchors.right: mouseArea1.left
        anchors.top: mouseArea1.top
        anchors.bottom: mouseArea1.bottom
        onClicked: value = 0

        XText {
            spacingFactor: 1.0
            id: text1
            text: leftText
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "black"
            opacity: value === 0 ?  1 : 0.2
            font.pixelSize: fontsize
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        }
    }

    MouseArea {
        id: mouseArea3
        anchors.left: mouseArea1.right
        anchors.right: parent.right
        anchors.top: mouseArea1.top
        anchors.bottom: mouseArea1.bottom
        onClicked: { if (value===0) value = 1;  }

        XText {
            id: text2
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            font.pixelSize: fontsize
            text: value < 2 ? rightText : temperamentNames[value]
            color: "black"
            opacity: value === 1 ?  1 : 0.2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    // This is a trick to redraw the switch upon rescaling
    Timer {
        id: timer1
        interval: 300
        running: false
        repeat: false
        onTriggered: { switch1.checked = 0; switch1.checked = value; }
    }
    onWidthChanged: if (value > 0) timer1.start()

}
