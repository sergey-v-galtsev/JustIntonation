/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.0

// This page shows some information about the application and its contributors

Item {
    id: about
    objectName: "about"

    property bool portrait: parent.height/parent.width>1.0
    property int buttonheight: portrait ? parent.height*0.04 : parent.height*0.05
    property int fontsize: (parent.height+0.5*parent.width)*(portrait ? 0.023:0.032)

    GridLayout {
        id: gridLayout1
        anchors.fill: parent
        anchors.margins: 2
        rows: 2
        columns: about.portrait ? 1:2

        Rectangle {
            id: groupBox1
            border.width: 1
            color: "transparent"
            Layout.fillHeight: true
            Layout.fillWidth: true

            XToolButton {
                visible: portrait
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: fontsize*0.5
                width: fontsize*3.5
                height: width
                source: "qrc:/icons/cancel.png"
                onClicked: stack.pop()
            }

            Image {
                id: image1
                asynchronous: true
                smooth: true
                anchors.top: parent.top
                anchors.topMargin: parent.height*0.07
                anchors.horizontalCenter: parent.horizontalCenter
                width: fontsize*3
                height: width
                source: "qrc:/logo/logoframed_256x256.png"
            }

            Image {
                id: image2
                asynchronous: true
                smooth: true
                mipmap: true
                anchors.top: image1.bottom
                anchors.topMargin: parent.height*0.04
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: fontsize*3.6
                source: "qrc:/images/header.png"
            }
            XText {
                id: text4
                text: "Karolin Stange\nChristoph Wick\nHaye Hinrichsen"
                horizontalAlignment: Text.AlignHCenter
                anchors.top: image2.bottom
                anchors.topMargin: parent.height*0.03
                anchors.left: parent.left
                anchors.right: parent.right
                font.pixelSize: 0.7*fontsize
            }
            XText {
                id: text5
                text: qsTr("Theoretical Physics III")+"\n"+
                      qsTr("Faculty for Physics and Astronomy")+"\n"+
                      qsTr("University of") + " Würzburg" + qsTr(", Germany")
                horizontalAlignment: Text.AlignHCenter
                anchors.top: text4.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 0.5*fontsize
            }
        }

        Rectangle {
            id: groupBox2
            border.width: 1
            color: "transparent"
            Layout.fillHeight: true
            Layout.fillWidth: true

            XText {
                id: text10
                text: qsTr("Version") + " 1.3.2"
                anchors.top: parent.top
                height: fontsize*2.5
                verticalAlignment: Text.AlignVCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: (landscape ? 0.7 : 1.1) *fontsize
            }
            Rectangle {
                id: separator
                anchors.top: text10.bottom
                width: parent.width
                height: fontsize*0.15
                gradient: Gradient {
                    GradientStop {
                        position: 0
                        color: "#ffffff"
                    }
                    GradientStop {
                        position: 1
                        color: "#000000"
                    }
                }
            }


            Flickable {
                anchors.top: separator.bottom
                anchors.topMargin: fontsize*0.3
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                contentHeight: 2*height
                contentWidth: width
                clip: true
                Column {
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: fontsize*0.5

                    XText {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.leftMargin: fontsize*0.5
                        anchors.rightMargin: fontsize*0.5
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: fontsize*0.5
                        text: "<b>Just Intonation</b>" + " " +
                              qsTr("is free software licensed under GNU GPLv3. The source code can be downloaded from Gitlab:")
                        verticalAlignment: Text.AlignVCenter
                        wrapMode: Text.WordWrap
                    }

                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: about.width*0.03

                        XButton {
                            text: "GPLv3"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally(licenseUrl)
                        }
                        XButton {
                            text: "Gitlab"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally(gitlabUrl)
                        }
                        XButton {
                            text: "Doxygen"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally(doxygenUrl)
                        }

                    }

                    XText {
                        id: text11
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.leftMargin: fontsize*0.5
                        anchors.rightMargin: fontsize*0.5
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: fontsize*0.5
                        verticalAlignment: Text.AlignVCenter
                        text: qsTr("This software is based on the following open-source third-party components:")
                    }
                    Row {
                        id: row3
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: about.width*0.03

                        XButton {
                            text: "Qt"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("http://www.qt.io")
                         }
                        XButton {
                            text: "RtMidi"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("https://www.music.mcgill.ca/~gary/rtmidi/")
                         }
                        XButton {
                            text: "PGMidi"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("https://github.com/petegoodliffe/PGMidi")
                        }
                    }
                    Row {
                        id: row5
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: about.width*0.03


                        XButton {
                            text: "Eigen"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("http://eigen.tuxfamily.org")
                        }
                        XButton {
                            text: "USB-Midi"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("https://github.com/kshoji/USB-MIDI-Driver")
                        }
                        XButton {
                            text: "QtMidi"
                            width: groupBox2.width*0.25
                            height: about.buttonheight
                            fontsize: window.fontsize*0.52
                            onClicked: Qt.openUrlExternally("https://gitlab.com/tp3/qtmidi")
                        }
                    }

                    XText {
                        id: text14
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.leftMargin: fontsize*0.5
                        anchors.rightMargin: fontsize*0.5
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: about.fontsize*0.45
                        text: qsTr("We thank all those who have contributed to the project:")+
                              "<br>Prof. Ross W. Duffin, Florian Frank, Thomas Herwig, Herbert Kraus, Sophia Sandler" +
                              "<br>Hochschule für Musik Würzburg <br> Musikwissenschaft Uni Würzburg" +
                              "<br>" + qsTr("Translations: ") + " Ilja Rausch, Olivier Large, Ignacio Reyes"
                        //verticalAlignment: Text.AlignVCenter
                        //height: fontsize*3
                    }
                    XText {
                        id: text13
                        anchors.right: parent.right
                        anchors.left: parent.left
                        anchors.leftMargin: fontsize*0.5
                        anchors.rightMargin: fontsize*0.5
                        //anchors.topMargin: window.height*0.04
                        textFormat: Text.StyledText
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: fontsize*0.3
                        text: "Just Intonation is <b>free software</b>, licensed under the terms of the GNU General Public License <b>GPLv3</b>. The source code of the project can be downloaded from GitLab (see above). This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
                    }
                 }
            }
            XToolButton {
                id: closebutton2
                source: "qrc:/icons/cancel.png"
                visible: ! portrait
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: fontsize*0.3
                width: fontsize*2
                height: width
                onClicked: stack.pop()
            }
        }
    }
}
