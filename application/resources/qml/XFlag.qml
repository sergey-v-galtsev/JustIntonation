/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtGraphicalEffects 1.0

Item {
    id: xflag
    property alias source: img.source
    property alias mousearea: mousearea
    readonly property real radiusscale: 0.1
    Image {
        id: img
        anchors.fill: parent

        layer.enabled: true
        scale: (mousearea.pressed ? 1.2 : 1)
        layer.effect: OpacityMask {
             maskSource: Rectangle {
                 width: img.width
                 height: img.height
                 radius: img.width*radiusscale
             }
         }
    }

    Rectangle
    {
        id: rect
        width: img.width
        height: img.height
        border.color: "gray"
        border.width: width*0.03
        radius: width*radiusscale
        color: "transparent"
        scale: (mousearea.pressed ? 1.2 : 1)
    }

    MouseArea {
        id: mousearea
        anchors.fill: parent
    }

}
