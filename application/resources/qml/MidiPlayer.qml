/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.0
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.4


Rectangle {
    id: midiplayer
    objectName: "midiplayer"

    height: Math.min(parent.height, parent.width*0.4)

    property int lastSliderValue: 0
    property int midiprogress: window.midiprogress

    onMidiprogressChanged: {
        lastSliderValue = midiprogress;
        sliderHorizontal1.value = midiprogress;
    }

    DropArea {
        id: droparea
        anchors.fill: parent
        onDropped: {
            if (drop.hasUrls) window.openFile(drop.urls[0]);
        }
    }

    color: droparea.containsDrag ? "green" : "transparent"

    Text {
        id: midifilename
        anchors.top: parent.top
        anchors.topMargin: parent.height*0.02
        width: parent.width
        height: parent.height*0.1
        font.family: window.font
        font.pixelSize: parent.height*0.12
        text: window.midifilename
        horizontalAlignment: Text.AlignHCenter
    }

    Dial {
        id: dial1
        width: parent.height*0.9
        anchors.rightMargin: parent.height*0.05
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: midifilename.bottom
        value: 0.5
        onValueChanged: setTempoFactor(Math.pow(value+0.5,-3))
    }

    XSlider {
        id: sliderHorizontal1
        anchors.topMargin: parent.height*0.1
        anchors.bottomMargin: parent.height*0.1
        anchors.leftMargin: parent.width*0.03
        anchors.rightMargin: parent.width*0.03
        anchors.right: dial1.left
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        height: parent.height*0.2
        stepSize: 1
        maximumValue: 100
        onPressedChanged: {
            if (value != lastSliderValue)
            {
                lastSliderValue = value;
                window.midiprogress = value;
                progressChangedManually(midiprogress);
            }
        }
    }

    Row {
        anchors.left: parent.left
        anchors.right: sliderHorizontal1.right
        anchors.top: midifilename.bottom
        anchors.bottom: sliderHorizontal1.top
        anchors.margins: parent.height*0.07
        spacing: parent.width*0.03

        XToolButton {
            boundary: false
            source: "icons/rewind.png"
            onClicked: window.rewind()
            radius: 2
            tooltip: qsTr("Rewind: Go back to the beginning")
        }
        XToolButton {
            width: height
            boundary: false
            source: playing ? "icons/pause.png" : "icons/play.png"
            onClicked: window.togglePlayPause()
            radius: 2
            tooltip: qsTr("Play / Pause")
        }
        XToolButton {
            boundary: false
            property bool repeating: false
            width: height
            source: "icons/repeat.png"
            opacity: repeating ? 1:0.2
            onClicked: { repeating = !repeating; setRepeatMode(repeating); }
            radius: 2
            tooltip: qsTr("Repeat mode")
        }
        XToolButton {
            boundary: false
            width: height
            source: "icons/open.png"
            onClicked: openFileOpenDialog(window.width, window.height)
            radius: 2
            tooltip: qsTr("Open a Midi file")
        }
    }
}
