/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0

Item {
    id: expert
    objectName: "expert"

    property double totalwidth: landscape ? Math.min(window.width, (window.height-window.width*0.06)*1.875) : window.width
    property double panelwidth: landscape ? totalwidth/3 : totalwidth
    property double fontsize: panelwidth*0.06
    property alias staticTuning: temperament.staticTuning

    Rectangle {
        id: separator1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: expertheadline.height*0.1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

    Item {
        id: expertheadline
        width: parent.width
        height: parent.width * (landscape ? 0.06 : 0.15)
        anchors.top: separator1.bottom

        Image {
            anchors.fill: parent
            source: "images/toptoolbar.jpg"
        }

        Image {
            id: experticon
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.leftMargin: parent.height*0.2
            anchors.topMargin: parent.height*0.05
            width: height
            smooth: true
            mipmap: true
            source: "icons/expertview.png"
            fillMode: Image.PreserveAspectFit
            clip: false
        }

        Row {
            id: iconrow
            anchors.right: parent.right
            height: parent.height
            layoutDirection: Qt.RightToLeft
            XToolButton {
                source: "icons/cancel.png"
                tooltip: qsTr("Go back to Simple Mode")
                onClicked: stack.pop()
            }
            XToolButton {
                tooltip: qsTr("Help and General Information")
                source: "icons/info.png"
                onClicked: stack.push(learnmore)
            }
            XToolButton {
                tooltip: qsTr("Modify Preferences")
                source: "icons/settings.png"
                onClicked: stack.push(preferences)
            }
            XToolButton {
                tooltip: qsTr("Play Music on a Touchscreen Keyboard")
                source: "icons/keyboard.png"
                onClicked: stack.push(keyboard)
            }
            XToolButton {
                tooltip: qsTr("Show Logfile")
                source: "icons/log.png"
                onClicked: stack.push(logfile)
            }
        }

        Text {
            text: qsTr("Expert Mode")
            anchors.left: experticon.right
            anchors.right: iconrow.left
            height: parent.height
            font.family: window.font
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: fontsize
            visible: landscape
        }
    }

    Rectangle {
        id: separator
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: expertheadline.bottom
        height: expertheadline.height*0.1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

    Flickable {
        id: area
        anchors.top: separator.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: totalwidth
        height: landscape ? width*0.527 : parent.height
        flickableDirection: Flickable.VerticalFlick
        contentWidth: parent.width
        contentHeight: landscape ? height : width*5
        clip: true

        property double w: panelwidth
        property double h: w*0.4

        Grid {
            anchors.top: parent.top
            anchors.left: parent.left
            width: parent.width*0.5
            //anchors.margins: area.w * 0.5
            columns: landscape ? 3 : 1

            Column { // LEFTMOST COLUMN

               XBox {
                   width: area.w
                   height: area.h
                    title: qsTr("Midi Player")
                    fontsize: expert.fontsize

                    MidiPlayer {
                        id: midiplayer
                        anchors.fill: parent.subarea
                    }
                }

                XBox {
                     width: area.w
                     height: area.h
                     title: qsTr("Midi Examples")
                     fontsize: expert.fontsize

                     Examples {
                         id: examples
                         anchors.fill: parent.subarea
                     }

                }

                XBox { // Instrument
                    width: area.w
                    height: area.h
                    title: qsTr("Instrument")
                    fontsize: expert.fontsize

                    Column {
                        anchors.fill: parent.subarea
                        anchors.leftMargin: parent.width*0.02
                        anchors.rightMargin: parent.height*0.09
                        anchors.topMargin: parent.height*0.06
                        spacing: parent.height*0.09
                        XCombo {
                            id: instrumentselector
                            width: parent.width
                            fontsize: expert.fontsize*1.2
                            height: width*0.13
                            model: window.instrumentmodel
                            property int index: window.instrumentindex
                            property bool selected: false
                            currentIndex: index
                            onActivated: selected = true
                            onCurrentIndexChanged: {
                                if (selected) {
                                    index = currentIndex;
                                    window.selectInstrument(index)
                                }
                                else currentIndex = index;
                                selected = false;
                            }
                            onIndexChanged: {
                                if (index != window.instrumentindex) window.instrumentindex = index;
                                if (index != currentIndex) currentIndex = index;
                            }
                        }
                        Item {
                            id: firstrow
                            width: parent.width
                            height: width*0.13
                            Text {
                                id: frequencytext
                                text: qsTr("Frequency:")
                                font.family: window.font
                                verticalAlignment: Text.AlignVCenter
                                font.pixelSize: expert.fontsize

                                height: parent.height
                            }

                            XSpinBox {
                                id: frequencychooser
                                anchors.right: parent.right
                                anchors.rightMargin: parent.width*0.25
                                anchors.left: frequencytext.right
                                anchors.leftMargin: parent.width*0.02
                                height: parent.height
                                fontsize: expert.fontsize
                                suffix: " Hz"
                                minimumValue: lowerTargetFrequency
                                maximumValue: upperTargetFrequency
                                decimals: 1
                                stepSize: 0.1
                                value: targetFrequency
                                property double frequency: targetFrequency
                                onFrequencyChanged: value = frequency
                                onValueChanged: { targetFrequency = value; }
                            }

                            XButton {
                                anchors.top: frequencychooser.top
                                anchors.bottom: frequencychooser.bottom
                                anchors.right: parent.right
                                anchors.left: frequencychooser.right
                                anchors.leftMargin: parent.width*0.02
                                text: "← 440"
                                fontsize: expert.fontsize
                                onClicked: frequencychooser.value = 440
                                tooltip: qsTr("Reset concert pitch to the standard value of 440 Hz")
                            }
                        }
                    }
                }


                Item {
                    width: area.w
                    height: area.w *(landscape ? 0.38 : 0)
                    Rectangle {
                        id: adbox
                        anchors.fill: parent
                        anchors.margins: fontsize*0.1
                        border.color: "black"
                        color: "gray"
                        opacity: 0.25
                        radius: fontsize/3
                        z:-5
                    }
                    Image {
                        anchors.fill: adbox
                        anchors.margins: parent.height*0.05
                        source: "images/header.png"
                        smooth: true
                        mipmap: true
                        MouseArea {
                            anchors.fill: parent
                            onClicked: stack.push(about)
                        }
                    }
                }
            }

            XBox {
                width: area.w
                height: area.h * 3.95
                title: qsTr("Tuning Mode")
                fontsize: expert.fontsize
                TuningMode {
                    id: tuningMode
                    anchors.fill: parent.subarea
                }
            }
            XBox {
                width: area.w
                height: area.h * 3.95
                title: qsTr("Temperament")
                fontsize: expert.fontsize

                Temperament {
                    id: temperament
                    anchors.fill: parent.subarea
                }
            }
        }
    }

    Rectangle {
        id: separator2
        visible: landscape
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: area.bottom
        height: expertheadline.height*0.1
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }
    Image {
        visible: landscape
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: separator2.bottom
        //anchors.bottom: parent.bottom
        source: "images/headerbackground.jpg"
    }
}
