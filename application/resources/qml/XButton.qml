/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Button {
    property int fontsize: width/5
    width: 200
    height: 50
    text: "undefined"
    scale: pressed ? 1.05 : 1
    style: ButtonStyle {
        label: Text {
            renderType: Text.NativeRendering
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family: window.font
            font.pixelSize: fontsize
            color: "black"
            text: control.text
        }
        background: Item {
            Rectangle {
                anchors.fill: parent
                radius: window.radius*1.1
                anchors.margins: -parent.height*0.05
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }

            }
            Rectangle {
                anchors.fill: parent
                border.width: control.activeFocus ? 2 : 1
                border.color: "#888"
                radius: window.radius
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? "#aaa" : "#eee" }
                    GradientStop { position: 1 ; color: control.pressed ? "#ddd" : "#aaa" }
                }
            }
        }
    }
}


