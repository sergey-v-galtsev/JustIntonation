/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

// A keyboard for touchscreens, particularly suitable for tablets

Item {
    id: keyboardform
    objectName: "keyboard"

    readonly property int numberOfKeys: 61
    readonly property int numberOfWhiteKeys: 36
    readonly property int numberOfBlackKeys: numberOfKeys - numberOfWhiteKeys
    readonly property int midiOffset: 36

    readonly property double whiteKeyLength: landscape ? height * 0.65 : height * 0.5
    readonly property double blackKeyLength: whiteKeyLength * 0.6
    readonly property double whiteKeyWidth: whiteKeyLength * 0.18
    readonly property double blackKeyFraction: 0.6
    readonly property double blackKeyWidth: whiteKeyWidth * blackKeyFraction
    readonly property variant blackKeyPositions: [0.6,1.8,3.55,4.7,5.85]
    readonly property variant diatonicIndices: [0,2,4,5,7,9,11]
    readonly property variant chromaticIndices: [1,3,6,8,10]

    // List holding the indices of pressed key
    property var indices: []
    property bool triad: false
    property bool touchscreen: mobile

    property int lastNonEqualTemperament: 1
    property int temperament : temperamentIndex
    onTemperamentChanged: if (temperament>0) lastNonEqualTemperament = temperament

    property int highlightFlag: window.highlightFlag
    onHighlightFlagChanged: {
        var key = Math.floor(highlightFlag/2) - midiOffset
        if (key >= 0 && key < numberOfKeys) {
            var pressed = (highlightFlag & 1 != 0)
            var oct = Math.floor(key/12)
            var ind = key % 12
            var white = diatonicIndices.indexOf(ind)
            if (white>=0) whiteKeys.itemAt(white+7*oct).pressed = pressed
            else {
                var black = chromaticIndices.indexOf(ind)
                if (black>=0) blackKeys.itemAt(black+5*oct).pressed = pressed
            }
        }
    }

    TextHeader {
        id: header
        showPreferencesButton: parent.height < 2.2*parent.width
        text: qsTr("Keyboard")
        closebutton.onClicked: clearTouchscreenKeyboard()
    }


    Item {
        id: upperpart
        anchors.fill: parent
        anchors.bottomMargin: whiteKeyLength
        anchors.topMargin: header.height
        readonly property int vgap: landscape ? upperpart.height*0.15 :
                                                (upperpart.height-4.5*window.fontsize)*0.2

        Grid {
            id: uppergrid
            flow: Grid.TopToBottom
            anchors.top: parent.top
            anchors.topMargin: parent.vgap
            anchors.horizontalCenter: parent.horizontalCenter
            columns: landscape ? 2 : 1
            rows: landscape ? 2 : 4
            readonly property int switchheight: landscape ? window.fontsize*0.7 : window.fontsize
            readonly property int switchwidth: landscape ? window.fontsize*9 : window.width*0.95
            columnSpacing: (keyboardform.width/keyboardform.height-0.95)*keyboardform.width*0.2
            rowSpacing: parent.vgap


            Item {
                width: parent.switchwidth
                height: parent.switchheight
                XCombo {
                    id: instrumentselector
                    fontsize: window.fontsize * (landscape ? 0.6 : 1)
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: -0.2*parent.height
                    width: parent.width * (landscape ? 1:0.8)
                    anchors.horizontalCenter: parent.horizontalCenter
                    model: window.instrumentmodel
                    property int index: window.instrumentindex
                    property bool selected: false
                    currentIndex: index
                    onActivated: selected = true
                    onCurrentIndexChanged: {
                        if (selected) {
                            index = currentIndex;
                            window.selectInstrument(index)
                        }
                        else currentIndex = index;
                        selected = false;
                    }
                    onIndexChanged: {
                        if (index != window.instrumentindex) window.instrumentindex = index;
                        if (index != currentIndex) currentIndex = index;
                    }
                }

            }

            XSwitch {
                buttonsize: parent.switchheight
                height: parent.switchheight
                width: parent.switchwidth
                fontsize: buttonsize
                leftText: qsTr("Equal")
                rightText: keyboardform.lastNonEqualTemperament === 1 ? qsTr("Just") : qsTr("Nonequal")
                property int index : temperamentIndex
                value : keyboardform.temperament
                onValueChanged: {
                    if (value===0) temperamentIndex = 0
                    else temperamentIndex = keyboardform.lastNonEqualTemperament
                }
                onIndexChanged: value = (index > 0 ? 1 : 0)
            }


            XSwitch {
                buttonsize: parent.switchheight
                height: parent.switchheight
                width: parent.switchwidth
                fontsize: buttonsize
                leftText: qsTr("Mouse")
                rightText: qsTr("Touchscreen")
                value: mobile
                onValueChanged: { setToggleMode(value === 0); touchscreen = (value !== 0); }
                Component.onCompleted: { setToggleMode(value === 0); touchscreen = (value !== 0); }
            }

            XSwitch {
                buttonsize: parent.switchheight
                height: parent.switchheight
                width: parent.switchwidth
                fontsize: buttonsize
                leftText: qsTr("Single tone")
                rightText: qsTr("Triad")
                value: triad
                onValueChanged: { triad = value; clearTouchscreenKeyboard(); }
            }
        }

    }

    Rectangle {
        id: lowerpart
        color: "black"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: upperpart.bottom
        anchors.bottom: parent.bottom

        Flickable {
            anchors.fill: parent
            anchors.leftMargin: contentWidth < parent.width ? (parent.width-contentWidth)/2 : 0
            anchors.bottomMargin: parent.height*0.8
            contentHeight: lowerpart.height
            contentWidth: numberOfWhiteKeys*whiteKeyWidth
            flickableDirection: Flickable.HorizontalFlick

            // Position in the center
            contentX: contentWidth / 2 - width / 2


            // Touch area processing the taps

            MultiPointTouchArea {
                anchors.fill: parent
                anchors.topMargin: parent.height*0.2
                maximumTouchPoints: 6
                mouseEnabled: true
                property var actualindices: []
                onTouchUpdated: {
                    actualindices = []
                    for (var i in touchPoints) {
                        var point = touchPoints[i];
                        var relativeX = point.x/whiteKeyWidth
                        var whiteKeyIndex = Math.floor(relativeX)
                        var octaveIndex = Math.floor(whiteKeyIndex / 7)
                        var index = 12*octaveIndex + diatonicIndices[whiteKeyIndex % 7]
                        if (point.y < blackKeyLength-parent.height*0.2) {
                            var blackKeyIndex = Math.floor(relativeX/1.4) % 5
                            var dist = (relativeX - 7*octaveIndex) - blackKeyPositions[blackKeyIndex]
                            if (dist > 0 && dist < blackKeyFraction) index = 12*octaveIndex + chromaticIndices[blackKeyIndex]
                        }
                        if (triad) {
                            actualindices.push(index + midiOffset + 4)
                            actualindices.push(index + midiOffset + 7)
                            actualindices.push(index + midiOffset + 12)
                        }
                        actualindices.push(index + midiOffset)
                    }

                    // Send signal
                    if (touchscreen || indices.length !== actualindices.length)
                    {
                        if ( indices != actualindices)
                        {
                            indices = actualindices;
                            sendTouchscreenKeyboardEvent(indices)
                        }
                    }
                }
            }

            // White piano keys

            Row {
                anchors.fill: parent
                Repeater {
                    id: whiteKeys
                    model: numberOfWhiteKeys
                    Item {
                        height: lowerpart.height
                        width: whiteKeyWidth
                        property bool pressed: false

                        Rectangle {
                            gradient: Gradient {
                                GradientStop { position: 0.0; color: "white" }
                                GradientStop { position: 1.0; color: pressed ? "#ddd" : "white" }
                            }
                            border.color: "black"
                            border.width: (pressed ? 0.02 : 0.01) * width
                            anchors.fill: parent
                            anchors.bottomMargin: pressed ? 0 : whiteKeyLength*0.01
                            radius: whiteKeyWidth*0.1
                        }
                        Text {
                            y: parent.height - fontsize - whiteKeyLength*0.04
                            width: parent.width
                            height: fontsize
                            visible: index%7==0
                            font.family: window.font
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                            font.pixelSize: fontsize*0.7
                            color: "gray"
                            text: "C" + (index/7 + 2)
                        }

                    }
                }
            }

            // Black piano keys

            Repeater {
                id: blackKeys
                model: numberOfBlackKeys
                Item {
                    height: blackKeyLength
                    width: blackKeyWidth
                    property bool pressed: false
                    x: (blackKeyPositions[index%5]+7*Math.floor(index/5))*whiteKeyWidth
                    Rectangle {
                        anchors.fill: parent
                        color: "darkgray"
                        border.color:  "black"
                        border.width: whiteKeyWidth*(pressed ? 0.04:0.02)
                        radius: whiteKeyWidth*0.08
                    }

                    Rectangle {
                        anchors.fill: parent
                        anchors.bottomMargin: parent.height*(pressed ? 0.01 :0.035)
                        anchors.leftMargin: parent.width*0.04
                        anchors.rightMargin: parent.width*0.04
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: "#222" }
                            GradientStop { position: 1.0; color: pressed ? "black" : "#222" }
                        }
                        border.color:  pressed ? "gray" : "lightgray"
                        border.width: whiteKeyWidth*0.04
                        radius: whiteKeyWidth*0.2
                    }

                }
            }
        }

        // Black bar seperating claviature from upper part

        Rectangle {
            id: blackbar
            color: "black"
            anchors.left: parent.left
            anchors.right: parent.right
            y: -parent.height * 0.02
            height: parent.height * 0.085  // determines the width of
        }

        XText {
            anchors.fill: blackbar
            y: -parent.height * 0.0
            color: "lightgray"
            text: "↔"
            font.family: window.font
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: blackbar.height
        }
    }
}

