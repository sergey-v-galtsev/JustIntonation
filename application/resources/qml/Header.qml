/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

Image {
    id: header
    width:500
    source: "images/headerbackground.jpg"
    readonly property double aspectratio: headerimage.sourceSize.height/headerimage.sourceSize.width
    height: 0.7*width*aspectratio

    Image {
        id: headerimage
        smooth: true
        mipmap: true
        source: "images/header.png"
        fillMode: Image.PreserveAspectFit
        clip: false
        anchors.fill: parent
        anchors.rightMargin: parent.width * 0.18
    }

    Item {
        id: logobox
        anchors.left: headerimage.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: parent.width*0.007

        Image {
            id: logoimage
            source: "logo/logoframed_256x256.png"
            asynchronous: true
            smooth: true
            anchors.fill: parent
            anchors.margins: parent.width*0.03
            fillMode: Image.PreserveAspectFit
            clip: false
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onContainsMouseChanged: {
                if (containsMouse) logoimage.source = "logo/logoinverted_256x256.png";
                else logoimage.source = "logo/logoframed_256x256.png";
            }
        }

    }

    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: header.bottom
        height: header.height*0.04
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }
    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: header.top
        height: header.height*0.04
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#ffffff"
            }
            GradientStop {
                position: 1
                color: "#000000"
            }
        }
    }

}
