/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3

Item {
    id: gettingstarted
    readonly property int scaling: Math.max(parent.width,parent.height)*0.02;

    TextHeader {
        id: header
        text: qsTr("Getting started")
    }

    Item {
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: parent.width*0.05
        anchors.rightMargin: parent.width*0.05
        anchors.topMargin: header.height*0.12

        Flickable {
            id: flickable1
            anchors.fill: parent
            flickableDirection: Flickable.VerticalFlick
            contentWidth: parent.width
            contentHeight: 3400*scaling*scaling/parent.width
            clip: true

            Column {
                id: col
                width: flickable1.width
                height: flickable1.height*1.5
                spacing: scaling

                Text {
                    width: col.width
                    font.family: window.font
                    font.bold: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: scaling*1.6
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    text: qsTr("Welcome to \"Just Intonation\".")
                }
                XText {
                    width: col.width
                    font.family: window.font
                    font.pixelSize: scaling*1.3
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    spacingFactor: 1.15
                    text: qsTr("With this application you can experience how it would be to hear and play Music in just intonation, independent of scale!")
                          +"<br><br><b>"+qsTr("System Requirements")+":</b><br>"+
                          qsTr("The application runs on most desktop computers and on sufficiently powerful tablets and smartphones.")+" "+
                          qsTr("It needs about 300MB of temporary memory (RAM).")+" "+
                          qsTr("In addition, if you want to download samples of realistic musical instruments, approximately 600MB of permanent storage (disk space) is needed.")
                          +"<br><br><b>"+qsTr("Download Realistic Instruments")+"</b><br>"+
                          qsTr("On startup the application will ask you for permission to download samples of real instruments from our website.")+" "+
                          qsTr("Depending on the speed of your internet connection this may take some time.")+" "+
                          qsTr("During the download you can already start to use the application.")
                          +"<br><br><b>"+qsTr("Listen to Music")+"</b><br>"+
                          qsTr("To hear music just tap the leftmost button with the note in the lower toolbar.")+" "+
                          qsTr("Toggle between just intonation and equal temperament and listen to the difference.")
                          +"<br><br><b>"+qsTr("Listen to your own Music")+"</b><br>"+
                          qsTr("Connect a Midi keyboard to your device.")+" "+
                          qsTr("The application will recognize most Midi devices automatically.")+" "+
                          qsTr("Just play on your keyboard and hear yourself playing in just intonation.")+" "+
                          qsTr("Midi keyboards can also be connected to mobile devices using a special adapter cable (see user manual).")
                          +"<br><br><b>"+qsTr("Expert Mode")+"</b><br>"+
                          qsTr("Tap the rightmost button in the lower toolbar to enter the Expert Mode.")+" "+
                          qsTr("Here you can choose various temperaments.")+" "+
                          qsTr("Moreover, you can control a large variety of parameters.")+" "+
                          qsTr("It is also possible to monitor the pitch drift while you are playing.")
                          +"<br><br><b>"+qsTr("More Information")+"</b><br>"+
                          qsTr("You want to learn more about Just Intonation and the history of temperaments?")+" "+
                          qsTr("You would like to know more about the features of the application and how it works internally?")+" "+
                          qsTr("Please download the User Manual and visit our website for more detailed information.")+" "+
                          qsTr("Thank you for your interest in Just Intonation.")
                }
                // some space
                Rectangle {
                    height: scaling*4
                    width: 10
                    color: "transparent"
                }

                XButton {
                    fontsize:  scaling*1.4
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width/1.8
                    height:  scaling*2.5
                    text: qsTr("Download User Manual")
                    onClicked: downloadManual()
                }

            }

        }
    }
}
