/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.0
import QtQuick.Extras 1.4

Item {
    id: tuningmode


    Item {
        id: panel
        anchors.fill: parent
        anchors.margins: parent.width*0.02

        Item {
            id: firstrow
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            height: 2*fontsize
            Text {
                id: delaytext
                text: qsTr("Tuning delay")
                font.family: window.font
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: expert.fontsize
                height: parent.height
            }
            XSlider{
                id: tuningDelaySlider
                valueVisible: true
                digits: 1
                unit: "s"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: delaytext.right
                anchors.leftMargin: parent.width*0.04
                height: parent.height/2
                minimumValue: 0
                maximumValue: 3
                stepSize: 0.1
                property double tuningDelay: window.tuningDelay
                value: tuningDelay
                onValueChanged: window.tuningDelay = value
                onTuningDelayChanged: value = tuningDelay
            }
        }
        Item {
            id: secondrow
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: firstrow.bottom
            height: 2*fontsize
            opacity: expert.staticTuning ? 0.4 : 1

            Text {
                id: memorytext
                text: qsTr("Memory")
                font.family: window.font
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: expert.fontsize
                height: parent.height
            }
            XSlider {
                id: memorySlider
                enabled: ! expert.staticTuning
                valueVisible: true
                digits: 1
                unit: "s"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: memorytext.right
                anchors.leftMargin: parent.width*0.04
                height: parent.height/2
                property double mem: memoryLength
                minimumValue: 0
                maximumValue: 5
                stepSize: 0.1
                value: memoryLength
                onValueChanged: memoryLength = value
                onMemChanged: value = memoryLength
            }

        }
        Item {
            id: thirdrow
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: secondrow.bottom
            height: 2*fontsize
            opacity: expert.staticTuning ? 0.4 : 1

            Text {
                id: drifttext
                text: qsTr("Drift correction")
                font.family: window.font
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: expert.fontsize
                height: parent.height
            }
            XSlider {
                id: lambdaSlider
                enabled: ! expert.staticTuning
                valueVisible: true
                digits: 0
                factor: 100
                unit: "%"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.left: drifttext.right
                anchors.leftMargin: parent.width*0.04
                height: parent.height/2
                property double lambda: driftCorrectionParameter
                value: lambda
                onValueChanged: driftCorrectionParameter = value
                onLambdaChanged: value = lambda
            }
        }

        Item {
            id: gauge
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: thirdrow.bottom
            anchors.topMargin: parent.width*0.1
            height: parent.width*0.82
            width: height

            Rectangle {
                anchors.fill: parent
                anchors.margins: -parent.height*0.01
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }
                radius: parent.height/2
            }

            Rectangle {
                anchors.fill: parent
                color: "lightgray"
                radius: height/2
            }

            CircularGauge {
                id: circularGauge
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                width: parent.width*0.9
                height: width
                minimumValue: -50
                maximumValue: 50
                value: pitchDeviation

                Behavior on value {
                    NumberAnimation {
                        duration: 100
                    }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: if (! expert.staticTuning) resetPitchProgression()
                }


                style: CircularGaugeStyle {
                     labelInset: outerRadius * 0.2

                     tickmarkLabel: Text {
                         font.pixelSize: Math.max(6, outerRadius * 0.1)
                         text: styleData.value
                         color: Math.abs(styleData.value) < 20 ? "darkgreen" : Math.abs(styleData.value) >= 30? "red" : "#ff8c00"
                         antialiasing: true
                     }

                     tickmark: Rectangle {
                         visible: styleData.value < 80 || styleData.value % 10 == 0
                         implicitWidth: outerRadius * 0.03
                         antialiasing: true
                         implicitHeight: outerRadius * 0.08
                         color: "black"
                     }

                     minorTickmark: Rectangle {
                         visible: styleData.value < 80 || styleData.value % 10 == 0
                         implicitWidth: outerRadius * 0.01
                         antialiasing: true
                         implicitHeight: outerRadius * 0.04
                         color: "#111"
                     }
                 }

                 Text {
                     id: indexText
                     text: qsTr("cent")
                     anchors.horizontalCenter: parent.horizontalCenter
                     anchors.bottom: circularGauge.bottom
                     font.family: window.font
                     font.pixelSize: expert.fontsize * 0.7
                     color: "black"
                 }
            }
        }
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: gauge.bottom
            anchors.leftMargin: parent.width*0.08
            anchors.rightMargin: parent.width*0.08
            anchors.topMargin: parent.width*0.06
            height: parent.width*0.08
            radius: fontsize*0.5
            anchors.margins: -parent.height*0.05
            gradient: Gradient {
                GradientStop { position: 0 ; color: "#fff" }
                GradientStop { position: 1 ; color: "#000" }
            }

            ProgressBar {
                id: distortiongauge
                anchors.fill: parent
                anchors.margins: parent.width*0.01
                orientation: Qt.Horizontal
                minimumValue: 0
                enabled: ! expert.staticTuning
                opacity: (enabled ? 1 : 0.1)
                value: 100*window.tension
                maximumValue: 100
                Behavior on value { NumberAnimation { duration: 10 }}

                style: ProgressBarStyle {

                    background: Item {
                        Rectangle {
                            anchors.fill: parent
                            radius: fontsize*0.5
                            color: "black"
                            border.color: "black"
                            border.width: 1
                        }
                    }

                    progress: Rectangle {
                        id: progress
                        visible: enabled
                        implicitWidth: fontsize*0.8
                        property double fraction: distortiongauge.value/ distortiongauge.maximumValue
                        border.color: "darkgray"
                        color: Qt.rgba(fraction < 0.5 ? 1 : 2-2*fraction, 0.3*(fraction), 0,1)
                        radius: fontsize*0.5
                    }
                }
            }
            XText {
                anchors.left: parent.left
                anchors.leftMargin: parent.width*0.07
                anchors.bottom: distortiongauge.bottom
                anchors.bottomMargin: height*0.2
                text: qsTr("tempered")
                color: "white"
                font.bold: true
                font.pixelSize: expert.fontsize * 0.75
            }
            XText {
                anchors.right: parent.right
                anchors.rightMargin: parent.width*0.07
                anchors.bottom: distortiongauge.bottom
                anchors.bottomMargin: height*0.2
                text: qsTr("just")
                color: "white"
                font.bold: true
                font.pixelSize: expert.fontsize * 0.75
            }
        }



        XText {
            id: intervalsizes
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: gauge.bottom
            anchors.bottom: panel.bottom
            anchors.topMargin: panel.height*0.08
            text: intervalString
            color: "darkblue"
            font.family: window.font
            font.pixelSize: expert.fontsize*0.67
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

    }
}
