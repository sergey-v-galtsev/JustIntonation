/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                  Scale
//=============================================================================

#ifndef SCALE_H
#define SCALE_H

#include <QVector>
#include <QIODevice>

#include "wave.h"

class Voice;

///////////////////////////////////////////////////////////////////////////////
/// \brief Scale - a set of PCM waves with a characteristic intensity and length.
/// \details This class describes a scale of sounds of an instrument. It
/// contains a vector of up to 128 Wave objects, enumerated from
/// 0 to 127 according to the MIDI norm. Not all these objects contain an
/// actual sound. For example, a piano scale runs only from 21 to 108.
/// Nevertheless it is assumed that all 128 wave objects exist.
///
/// A given instrument can have different scales. For example, one could have
/// a forte scale and a piano scale as well as a scale for release sounds.
///////////////////////////////////////////////////////////////////////////////

class Scale
{
public:

    Scale();

    Wave &getWave(int &key);

    bool write (QIODevice &iodevice, Voice* voice = nullptr);
    bool read  (QIODevice &iodevice, Voice* voice = nullptr);

    void printInfo();

    void generateArtificialSound (Voice* voice = nullptr, int samplerate = 44100);

    void cancel() { mCancellationRequested = true; }
    
    bool insert (const int key,
                 const QVector<qint32> &L, const QVector<qint32> &R,
                 const bool release, const double amplification);



private:

    QVector<Wave> mWaves;           ///< This is the actual vector of waves
    static constexpr int N = 128;   ///< Maximal number of waves in a scale
    const int ctag = 0x78563412;    ///< Tag to recognize a scale in a file
    bool mCancellationRequested;    ///< Cancel loading
};

#endif // SCALE_H
