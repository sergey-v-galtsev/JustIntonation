/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//  Voice - a collection of scales, the acoustic static data of an instrument
//============================================================================

#include "voice.h"

#include <QDebug>
#include <QThread>

#include "instrumentfilehandler.h"

#define STATUS  if (mVerbosity >= 4) qDebug() << "Voice:"
#define MESSAGE if (mVerbosity >= 3) qDebug() << "Voice:"
#define WARNING if (mVerbosity >= 2) qWarning() <<"Voice: WARNING:"
#define ERROR   if (mVerbosity >= 1) qCritical() << "Voice: ERROR:"

int Voice::mVerbosity = 2;

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

Voice::Voice()
    : mSampleRate(0)
    , mHighestDampedKey(0)
    , mDynamicVolume(true)
    , mScales()
    , mRecordedPitchInCents(128,0)
{}


//-----------------------------------------------------------------------------
//                 Compute artificial tones in standard ET
//-----------------------------------------------------------------------------

void Voice::generateArtificialSound (int samplerate)
{
    mScales.clear();                // Clear existing sound
    mSampleRate = samplerate;
    mHighestDampedKey=256;
    mDynamicVolume = true;
    emit setProgressText(tr("Please wait..."));
    mScales.resize(1);  // construct only one scale
    mScales[0].generateArtificialSound(this,samplerate);
    emit signalArtificialSoundGenerationComplete(true);
}



//-----------------------------------------------------------------------------
//                       Write complete data to device
//-----------------------------------------------------------------------------

bool Voice::write(QIODevice &iodevice)
{
    if (mScales.size()==0)
    {
        ERROR << "Voice::write: saving empty file.";
        return true;
    }
    if (mScales.size() > 128)
    {
        ERROR << "Voice::write: More than 128 scales.";
        return true;
    }
    if (iodevice.isOpen()) iodevice.close();
    if (not iodevice.open(QIODevice::WriteOnly))
    {
        WARNING << "Voice::read: Could not open device";
        return false;
    }
    char tag[16]="justintonation ";
    iodevice.write(tag,15);
    InstrumentFileHandler::write(iodevice,mScales.size());
    InstrumentFileHandler::write(iodevice,mSampleRate);
    InstrumentFileHandler::write(iodevice,mHighestDampedKey);
    InstrumentFileHandler::write(iodevice,mDynamicVolume);

    for (auto &scale : mScales)
    {
        if (not scale.write(iodevice,this)) return false;
    }
    for (auto &entry : mRecordedPitchInCents) 
        InstrumentFileHandler::write(iodevice,entry);
    char endtag[14]=" end of file ";
    iodevice.write(endtag,13);
    return true;
}


//-----------------------------------------------------------------------------
//                       Read complete data from device
//-----------------------------------------------------------------------------

bool Voice::read(QIODevice &iodevice)
{
    mScales.clear();
    if (iodevice.isOpen()) iodevice.close();
    if (not iodevice.open(QIODevice::ReadOnly))
    {
        WARNING << "read: Could not open device";
        return false;
    }
    QByteArray tag = iodevice.read(15);
    if (tag.size() != 15 or strncmp(tag.data(),"justintonation ",11)!=0)
    {
        WARNING << "read: Invalid voice tag at the beginning";
        return false;
    }
    int numberOfScales=0;
    InstrumentFileHandler::read(iodevice,numberOfScales);
    if (numberOfScales<0 or numberOfScales>255)
    {
        WARNING << "read: Unreasonable number of scales";
        return false;
    }
    mScales.clear();
    mScales.resize(numberOfScales);
    MESSAGE << "read: Now reading" << mScales.size() << "scales.";

    InstrumentFileHandler::read(iodevice,mSampleRate);
    InstrumentFileHandler::read(iodevice,mHighestDampedKey);
    InstrumentFileHandler::read(iodevice,mDynamicVolume);

    for (int scaleNumber=0; scaleNumber<mScales.size(); ++scaleNumber)
    {
        QString progress = tr("Reading scale") + " " + QString::number(scaleNumber+1)
                + "/" + QString::number(mScales.size());
        emit setProgressText(progress);
        if (not mScales[scaleNumber].read(iodevice,this)) return false;
        if (QThread::currentThread()->isInterruptionRequested()) return false;
    }
    STATUS << "Reading pitch corrections";
    mRecordedPitchInCents.resize(128);
    for (auto &entry : mRecordedPitchInCents) 
        InstrumentFileHandler::read(iodevice,entry);
    
    QByteArray endtag = iodevice.read(13);
    if (endtag.size() != 13 or strncmp(endtag.data()," end of file ",13)!=0)
    {
        WARNING << "Voice::read: Invalid voice tag and the end of file";
        return false;
    }
    MESSAGE << "Voice::read: we have read" << mScales.size() << "scales.";
    return true;
}



void Voice::cancel()
{
    for (Scale &scale : mScales) scale.cancel();
}


//-----------------------------------------------------------------------------
//                              Clear all data
//-----------------------------------------------------------------------------

void Voice::clear()
{
    MESSAGE << "Voice::Clear: Clearing all scales";
    mScales.clear();
}


//-----------------------------------------------------------------------------
//                      print information about this voice
//-----------------------------------------------------------------------------

void Voice::printInfo()
{
    qDebug() << "Voice containing" << mScales.size() << "scales";
    for (auto &e : mScales) e.printInfo();
}

