/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//  Voice - a collection of scales, the acoustic static data of an instrument
//============================================================================

#ifndef VOICE_H
#define VOICE_H

#include <QObject>
#include <QVariant>
#include <QVector>
#include <QIODevice>

#include "scale.h"
#include "system/shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Voice - a set of scales, keeping the acoustic data of an instrument
///////////////////////////////////////////////////////////////////////////////

class Voice : public QObject
{
    Q_OBJECT
public:
    Voice();

    void init() {}
    void exit() {}

    void generateArtificialSound (int samplerate);

    bool write (QIODevice &iodevice);
    bool read (QIODevice &iodevice);
    void cancel();

    void clear();
    bool insert (const int scale, const int key, const QVector<qint32> &L, const QVector<qint32> &R, const int sampleRate, const double amplification);

    void printInfo();

    Scale &getScale (int i) { return mScales[i]; }
    int getNumberOfScales() const { return mScales.size(); }
    int getSampleRate() const { return mSampleRate; }
    int getHighestDampedKey() const { return mHighestDampedKey; }
    bool isVolumeDynamic() const { return mDynamicVolume; }

    double getRecordedPitch(int key) const { return mRecordedPitchInCents[key]; }

    static int mVerbosity;          ///< Verbosity of qDebug() message
    void setVerbosity (int verbosity) { mVerbosity=verbosity; }
signals:
    void setProgressText (QVariant text);
    void setProgress (QVariant percent);
    void signalArtificialSoundGenerationComplete(bool);

protected:
    int mSampleRate;
    int mHighestDampedKey;
    bool mDynamicVolume;
    QVector<Scale> mScales;
    QVector<double> mRecordedPitchInCents;
};

#endif // VOICE_H
