/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                      Class describing an instrument
//=============================================================================

#include "instrument.h"

#include <QDebug>
#include <QFile>

#define STATUS  if (Voice::mVerbosity >= 4) qDebug() << "Instrument:"
#define MESSAGE if (Voice::mVerbosity >= 3) qDebug() << "Instrument:"
#define WARNING if (Voice::mVerbosity >= 2) qWarning()<<"Instrument: WARNING:"
#define ERROR   if (Voice::mVerbosity >= 1) qCritical() << "Instrument: ERROR:"


Instrument::Instrument()
    : ThreadBase()
    , mVoice()
    , mFileName()
    , mLoading(false)
    , mLoaded(false)
{
    setThreadName("Instrument");
}


//----------------------------------------------------------------------------
//                        Load or generate a voice
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////
/// \brief Stop the instrument thread
/// \return true if thread exited normally
//////////////////////////////////////////////////////////////////////////////

bool Instrument::stop()
{
    cancelLoading();
    return ThreadBase::stop();
}


//----------------------------------------------------------------------------
//                        Load or generate a voice
//----------------------------------------------------------------------------
//////////////////////////////////////////////////////////////////////////////
/// \brief Load or generate an instrument (voice)
/// \details This function loads an instrument file by starting the
/// corresponding thread. If the path is empty an artificial sound is
/// generated instead.
/// \param path : Path to the instrument file, empty for artificial sound
//////////////////////////////////////////////////////////////////////////////

void Instrument::loadInstrument (QString path)
{
    MESSAGE << "Loading" << path;
    mLoading = true;
    mFileName = path;
    if (mFileName.size()==0)
    {
        STATUS << "Generate artificial sound";
        emit showProgressBar(tr("Generating artificial sound."));
        mVoice.generateArtificialSound(44100);
        emit hideProgressBar();
    }
    else
    {
        STATUS << "Load instrument" << mFileName;
        QFile file(mFileName);
        if (not file.open(QIODevice::ReadOnly))
        {
            ERROR << "Cannot open file" << mFileName;
            emit signalLoadingFinished(false);
            mLoading = false;
            return;
        }
        mLoaded = false;
        emit showProgressBar(tr("Loading instrument. Please wait..."));
        mLoaded = mVoice.read(file);
        emit hideProgressBar();
        if (mLoaded) { MESSAGE << "File successfully read"; }
        else { WARNING << "Could not read file" << mFileName; }
        emit signalLoadingFinished(mLoaded);
    }
    mLoading = false;
}

