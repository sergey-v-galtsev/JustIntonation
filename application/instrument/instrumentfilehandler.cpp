/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include "instrumentfilehandler.h"

InstrumentFileHandler::InstrumentFileHandler()
{}


//-----------------------------------------------------------------------------
//                     Write an arbitrary object to device
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Template function: Write an object to a device
///////////////////////////////////////////////////////////////////////////////

template <class T>
bool InstrumentFileHandler::write(QIODevice &iodevice, const T &object)
{
    const char* objectptr = reinterpret_cast<const char *>(&object);
    size_t size = sizeof(object);
    size_t bytes = static_cast<size_t>(iodevice.write (objectptr,static_cast<qint64>(size)));
    return (bytes == size);
}


//-----------------------------------------------------------------------------
//                     Read an arbitrary object from device
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Template function: Read an object from a device
///////////////////////////////////////////////////////////////////////////////

template <class T>
bool InstrumentFileHandler::read(QIODevice &iodevice, T &object)
{
    char* objectptr = reinterpret_cast<char *>(&object);
    size_t size = sizeof(object);
    size_t bytes = static_cast<size_t>(iodevice.read (objectptr,static_cast<qint64>(size)));
    return (bytes == size);
}


//-----------------------------------------------------------------------------
//                 Various implementations of write and read
//-----------------------------------------------------------------------------

template bool InstrumentFileHandler::write<qint32> (QIODevice&,const qint32&);
template bool InstrumentFileHandler::read<qint32> (QIODevice&,qint32&);
template bool InstrumentFileHandler::write<quint32> (QIODevice&,const quint32&);
template bool InstrumentFileHandler::read<quint32> (QIODevice&,quint32&);
template bool InstrumentFileHandler::write<double> (QIODevice&,const double&);
template bool InstrumentFileHandler::read<double> (QIODevice&,double&);
template bool InstrumentFileHandler::write<bool> (QIODevice&,const bool&);
template bool InstrumentFileHandler::read<bool> (QIODevice&,bool&);
