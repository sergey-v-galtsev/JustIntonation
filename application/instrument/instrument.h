/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                      Class describing an instrument
//=============================================================================

#ifndef INSTRUMENT_H
#define INSTRUMENT_H

#include <QVariant>

#include "voice.h"
#include "modules/system/threadbase.h"

////////////////////////////////////////////////////////////////////////////////
/// \brief Class describing an instrument
////////////////////////////////////////////////////////////////////////////////

class Instrument : public ThreadBase
{
    Q_OBJECT
public:
    Instrument();
    bool stop() override final;

    bool isLoading() { return mLoading; }
    bool hasLoaded() { return mLoaded; }

    Voice* getVoice() { return &mVoice; }

public slots:
    void loadInstrument (QString path);
    void cancelLoading() { mVoice.cancel(); }

signals:
    void showProgressBar (QVariant);
    void hideProgressBar ();
    void signalLoadingFinished (bool);

protected:
    Voice mVoice;                   ///< The set of all samples
    QString mFileName;              ///< The file name from where the file is loaded
    bool mLoading;                  ///< Flag indicating that an instrument is being loaded
    bool mLoaded;                   ///< Flag indicating that an instrument has been loaded
};

#endif // INSTRUMENT_H
