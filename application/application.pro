#==============================================================================
#                  Just Intonation - Application Project File
#==============================================================================

QT += qml quick widgets multimedia network midi
TARGET = justintonation
TEMPLATE = app
CONFIG += c++14

INCLUDEPATH += modules  ../thirdparty
BUILDPATH = $$OUT_PWD/..
LIBS += -L$$BUILDPATH

# include other modules
include(modules/modules.pri)

HEADERS += \
    config.h \
    application.h \
    touchscreenkeyboard.h \
    midi.h \
    system/downloader.h \
    system/systemtools.h \
    instrument/scale.h \
    instrument/voice.h \
    instrument/wave.h \
    instrument/instrument.h \
    instrument/instrumentfilehandler.h \
    sampler/tone.h \
    sampler/soundgenerator.h \
    sampler/sampler.h \
    sampler/midihandler.h \
    system/logfile.h \
    system/filehandler.h \

SOURCES += \
    main.cpp \
    application.cpp \
    touchscreenkeyboard.cpp \
    midi.cpp \
    system/downloader.cpp \
    system/systemtools.cpp \
    instrument/scale.cpp \
    instrument/voice.cpp \
    instrument/wave.cpp \
    instrument/instrument.cpp \
    instrument/instrumentfilehandler.cpp \
    sampler/tone.cpp \
    sampler/soundgenerator.cpp \
    sampler/sampler.cpp \
    sampler/midihandler.cpp \
    system/logfile.cpp \
    system/filehandler.cpp \

# iOS wants these entries separately
RESOURCES += resources/qml/qml.qrc
RESOURCES += resources/media/media.qrc
RESOURCES += resources/translations/languages.qrc
    
#============================== Platform-specific =============================

#---------------------------------- Linux -------------------------------------

linux:!android {
    QMAKE_CXXFLAGS_DEBUG += -Wall -Wpedantic
    LIBS += -lasound
}

#---------------------------------- Mac-OSX -----------------------------------

macx {
    # Ignore SDK update (should be deleted later)
    CONFIG+=sdk_no_version_check

    # Problems occur frequently with XCode updates
    # In case of "targetconditionals.h not found" the following line might help
    # QMAKE_MAC_SDK = macosx10.12
    # However, this didn't work with the recent update to XCode 8.2 / Qt 5.9
    # In this case add the following line ‚
    # !host_build:QMAKE_MAC_SDK = macosx10.12
    # at the very end of Qt/5.9/clang_64/mkspecs/qdevice.pri

    LIBS += -framework CoreFoundation -framework CoreAudio -framework CoreMidi
    ICON = $$PWD/platforms/macosx/icon.icns

    # Icon file should also be specified in Info.plist
    QMAKE_INFO_PLIST = $$PWD/platforms/macosx/Info.plist

    # Code for copying the Midi plugin file on deployment
    PLUGIN_DIR = $$OUT_PWD/"$$TARGET".app/Contents/Plugins

    CONFIG(debug, debug|release) {
        PLUGIN_FILE=libqtpg_midi_debug.dylib
    }
    else {
        PLUGIN_FILE=libqtpg_midi.dylib
    }

    QMAKE_POST_LINK += \
        echo \'Creating directory for the deployment of QtMidi library $$PLUGIN_FILE\' $$escape_expand(\\n\\t) \
        $$sprintf($$QMAKE_MKDIR_CMD, $$quote($$PLUGIN_DIR/midi)) $$escape_expand(\\n\\t) \
        echo \'Copying QtMidi library $$PLUGIN_FILE\' $$escape_expand(\\n\\t) \
        $$QMAKE_COPY $$quote($$(QTDIR)/plugins/midi/$$PLUGIN_FILE) $$quote($$PLUGIN_DIR/midi/$$PLUGIN_FILE) $$escape_expand(\\n\\t)
}

#------------------------------------ iOS -------------------------------------

ios {
#    LIBS += -framework UIKit -framework Foundation
    QMAKE_MAC_SDK = iphoneos

    QMAKE_INFO_PLIST = $$PWD/platforms/ios/Info.plist

    assets_catalogs.files = $$files($$PWD/platforms/ios/*.xcassets)
    QMAKE_BUNDLE_DATA += assets_catalogs

    launch_xib.files = $$files($$PWD/platforms/ios/IntonationLaunchScreen.xib)
    QMAKE_BUNDLE_DATA += launch_xib

#    QMAKE_IOS_TARGETED_DEVICE_FAMILY = 2
}

#---------------------------------- Windows -----------------------------------

#    QMAKE_LFLAGS +=   -DYNAMICBASE -NXCOMPAT -APPCONTAINER


winrt {
    CONFIG(debug, debug|release) {
        PLUGIN_DIR = $$OUT_PWD/debug
        PLUGIN_FILE=qtwinrt_midid.dll
    }
    else {
        PLUGIN_DIR = $$OUT_PWD/release
        PLUGIN_FILE=qtwinrt_midi.dll
    }

    PLUGIN_DIR ~= s,/,\\,g


    QMAKE_POST_LINK += \
        $$sprintf($$QMAKE_MKDIR_CMD, $$quote($$PLUGIN_DIR\\midi)) $$escape_expand(\\n\\t) \
        $$QMAKE_COPY $$quote($$(QTDIR)\\plugins\\midi\\$$PLUGIN_FILE) $$quote($$PLUGIN_DIR\\midi\\$$PLUGIN_FILE) $$escape_expand(\\n\\t)
}
else:win32 {
    LIBS += -lwinmm
    # RC_FILE = platforms/windows/icons/icons.rc
    RC_ICONS = $$PWD/platforms/windows/icons/icons.ico
}

#---------------------------------- Android -----------------------------------

# If Android tries to deploy with the SDK build tools of an old version
# check the file grade.properties and make sure that it is excluded in .gitignore

android {
    QT  += androidextras

DISTFILES += \
    platforms/android/AndroidManifest.xml \
    platforms/android/gradle/wrapper/gradle-wrapper.jar \
    platforms/android/gradlew \
    platforms/android/res/values/libs.xml \
    platforms/android/build.gradle \
    platforms/android/gradle/wrapper/gradle-wrapper.properties \
    platforms/android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/platforms/android
}

#=============================== Translations =================================

TRANSLATIONS = \
    resources/translations/justintonation_de.ts \
    resources/translations/justintonation_fr.ts \
    resources/translations/justintonation_es.ts \
    resources/translations/justintonation_ru.ts \

# Include QML files as sources:
lupdate_only { SOURCES += resources/qml/*.qml }

# Default rules for deployment.
include(deployment.pri)

#=============================== Installation =================================

unix {
target.path = $$PREFIX/bin

pixmap.path = $$PREFIX/share/pixmaps
pixmap.files += $$PWD/resources/desktop/justintonation_icon.png

application.path = $$PREFIX/share/applications
application.files += $$PWD/resources/desktop/justintonation.desktop

# do not use += in the following line (duplicated targets)
INSTALLS = target pixmap application
}
