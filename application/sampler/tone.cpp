/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                   Tone
//=============================================================================

#include "tone.h"


//-----------------------------------------------------------------------------
//                                Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Defautl constructor, resetting the member variables
///////////////////////////////////////////////////////////////////////////////

Tone::Tone()
    : mState(IDLE)
    , mKey(0)
    , mTime(0)
    , mVolume(0)
    , left(0)
    , right(0)
    , pSample(nullptr)
    , mType(UNDEFINED)
    , mScaleIndex(0)
{}



//-----------------------------------------------------------------------------
//                      Constructor with arguments
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of a Tone which allows to set certain properties.
/// \param key : Number of the key in MIDI norm (0..127)
/// \param scaleindex : Number of the scale from which the waveform is loaded
/// \param sample : Reference to the waveform (redundant but for performance)
/// \param volume : Volume of the tone
/// \param type : Type of the tone
/// \see Type
///////////////////////////////////////////////////////////////////////////////

Tone::Tone(int key, int scaleindex, Wave &sample, double volume, Type type)
{
    mState = PLAYING;
    mKey = key;
    mTime = 0;
    mVolume = volume;
    left = right = 0;
    pSample = &sample;
    mType = type;
    mScaleIndex = scaleindex;
}
