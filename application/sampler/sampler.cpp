/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                  Sampler
//=============================================================================

#include "sampler.h"
#include "tone.h"
#include "soundgenerator.h"
#include "tuner/tuner.h"

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the Sampler without functionality
///////////////////////////////////////////////////////////////////////////////

Sampler::Sampler()
    : AudioGenerator()
    , pSoundGenerator(nullptr)
    , pTones(nullptr)
    , mLeft((uint)mMaxNumberOfFrames)
    , mRight((uint)mMaxNumberOfFrames)
{}


//-----------------------------------------------------------------------------
//                                    Init
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization of the sampler
/// \param soundgenerator : Pointer to the SoundGenerator from where the sampler
/// is controlled.
///////////////////////////////////////////////////////////////////////////////

void Sampler::init (SoundGenerator *soundgenerator)
{
    pSoundGenerator = soundgenerator;
    if (not soundgenerator)
    {
        qDebug() << "Sound generator pointer invalid";
        return;
    }
    pTones = soundgenerator->getTones();
}

//-----------------------------------------------------------------------------
//           Essential sound-generating function of the sampler
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Essential sound-generating function of the sampler
/// \details This is the essential time-critical function of the sampler
/// which generates the actual sound. The audio device calls this function
/// whenever the audio stream needs more PCM values.
/// \param buffer : Pointer to the array where the PCM values are stored.
/// \param maxSize : Maximal allowed number of bytes to be generated.
/// \return : size of the generated PCM stream.
///////////////////////////////////////////////////////////////////////////////

size_t Sampler::generateSound(char *buffer, size_t maxSize)
{
    //************ Determine parameters *************

    // Overall volume factor
    constexpr double volumeFactor = 32;

    // 2 bytes (PCM16, Linux) or 3 bytes (PCM24, MAC)
    uint bytes = (mParameters.sampleSize==16 ? 2:3);

    // number of frames to be generated
    size_t frames = std::min(maxSize/bytes/2,mMaxNumberOfFrames);
    if (frames==0) return 0;

    // get pointers to arrays used for superposition
    qint32* leftarray = mLeft.data();
    qint32* rightarray = mRight.data();

    // clear the superposition arrays
    memset(leftarray, 0, sizeof(qint32)*frames);
    memset(rightarray, 0, sizeof(qint32)*frames);


    //*************** Compute sound ****************

    // Loop over all tones computing the superposition
    pSoundGenerator->lockTones();
    for (Tone &tone : *pTones) if (tone.mState != Tone::State::TERMINATED)
    {
        qint32 volume = static_cast<qint32>(volumeFactor*tone.mVolume);

        Wave &sample = tone.getSample();
        const QVector<qint16>* pcm;
        if (tone.mType == Tone::Type::RELEASETONE)
        {
            pcm = sample.getReleaseSample();
            volume <<= sample.getReleaseShift();
        }
        else // main tone
        {
            pcm = sample.getSustainSample();
            volume <<= sample.getSustainShift();
        }
        const qint16* waveformpointer = pcm->data();
        quint32 arrayindexmax = pcm->size();
        if (arrayindexmax==0) continue;
        quint32 time = tone.mTime;
        int key = tone.mKey & 0x7F;
        quint32 increment = pSoundGenerator->getIncrement(key);
        quint32 repetitionTime = (tone.pSample->getRepetitionIndex()) << 11;
        double  repetitionFactor = tone.pSample->getRepetitionFactor();

        qint16 left = tone.left;
        qint16 right = tone.right;
        for (size_t j=0; j<frames; j++)
        {
            qint32 weight = (time & 0xFFFU) ^ 0xFFFU;
            qint32 norm = weight | 0x1000U;
            quint32 arrayindex = (time>>11) & 0x1FFFFEU;
            left  = ((waveformpointer[arrayindex]<<12) + weight*left)/norm;
            right = ((waveformpointer[arrayindex+1]<<12) + weight*right)/norm;
            leftarray[j]  += volume*left;
            rightarray[j] += volume*right;

            time += increment;
            if (arrayindex >= arrayindexmax-2)
            {
                // if a repetition is available
                if (repetitionTime and tone.mType != Tone::Type::RELEASETONE)
                {
                    time = (time & 0xFFFU) | repetitionTime;
                    tone.mVolume *= repetitionFactor;
                    volume = static_cast<qint32>(volumeFactor*tone.mVolume);
                    volume <<= sample.getSustainShift();
                    // qDebug() << "Volume = " << tone.mVolume;
                }
                else
                {
                    // qDebug() << "Sampler terminates key (treble shorter than 24sec)";
                    tone.mVolume = 0;
                    time = 0;
                    tone.mState = Tone::State::TERMINATED;
                    break;
                }
            }
            if ((j&0xF) == 0)
            {
                if (tone.mState == Tone::State::RELEASED)
                {
                    tone.mVolume *= 0.99;
                    volume = 99 * volume / 100;
                }
            }
        }
        tone.left = left;
        tone.right = right;
        tone.mTime = time;
    }
    pSoundGenerator->unlockTones();


    //*************** Copy sound to audio ****************

    // number of bytes to be generated
    size_t buffersize = 2*frames*bytes;

    // Clear audio device buffer
    memset(buffer, 0, buffersize);

    // Redefine pointer in order to transfer 16 bit in one package
    qint16* audioptr = (qint16*)buffer;

    size_t i=0;
    for (size_t j=0; j<frames; j++)
    {
        qint32 signalleft  = leftarray[j]>>7;
        qint32 signalright = rightarray[j]>>7;
        if (bytes==2)
        {
            audioptr[i++] += (qint16)(signalleft>>8);
            audioptr[i++] += (qint16)(signalright>>8);
        }
        else if (bytes==3)
        {
            audioptr[i++] += (qint16)(signalleft & 0xFFFFU);  // not clear whether addition is correct here
            audioptr[i++] += (qint16)((signalleft>>16) | ((signalright & 0xFFU)<<16));
            audioptr[i++] += (qint16)(signalright >> 8);
        }
    }
    //qDebug() << "sampler rendered buffer size = " << buffersize << "/" <<  maxSize;
    return buffersize;
}

