/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//            SoundGenerator - manages the tones that are being played
//=============================================================================

#include "soundgenerator.h"
#include "system/systemtools.h"

#include <QDebug>

#define STATUS  if (mVerbosity >= 4) qDebug() << "SoundGenerator:"
#define MESSAGE if (mVerbosity >= 3) qDebug() << "SoundGenerator:"
#define WARNING if (mVerbosity >= 2) qWarning()<<"SoundGenerator: WARNING:"
#define ERROR   if (mVerbosity >= 1) qCritical() << "SoundGenerator: ERROR:"

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------

SoundGenerator::SoundGenerator()
    : mQueue()
    , mQueueMutex()
    , mTones()
    , mToneMutex()
    , mIncrements(128,0x1000)
    , mIncrementMutex()
    , mSampler()
    , pVoice(nullptr)
    , pApplication(nullptr)
    , mSustainPedeal(false)
    , mSostenutoPedal(false)
{
    setModuleName("SoundGenerator");
    setThreadName("SoundGenerator");
}


//-----------------------------------------------------------------------------
//                        Initialize and start thread
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Initialize the SoundGenerator
/// \param application : Pointer to the main application
/// \param voice : Pointer to the Voice (the collection of scales describing
/// an instrument)
///////////////////////////////////////////////////////////////////////////////

void SoundGenerator::init(Application *application, Voice *voice)
{
    pApplication = application;
    pVoice = voice;                // Store the reference of the Voice in pVoice
    mSampler.init(this);
    connect(this,&SoundGenerator::forceImmediateProcessing,
            this,&SoundGenerator::timeout);
    setTimerInterval(1);
}




void SoundGenerator::suspend()
{
    mQueueMutex.lock();
    mQueue.clear();
    mQueueMutex.unlock();
    mToneMutex.lock();
    mTones.clear();
    mToneMutex.unlock();
    ThreadBase::suspend();
}



//-----------------------------------------------------------------------------
//            Register a single request to play a note in the queue
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Register a single request to play a note in the queue
/// \details This function is thread-safe and appends a request in the
/// local mQueue.
/// \param request : Reference to the Request to be registered
/// \see Request
///////////////////////////////////////////////////////////////////////////////

void SoundGenerator::registerRequest(Request &request)
{
    if (not isActive()) return;
    mQueueMutex.lock();
    mQueue.enqueue(request);
    mQueueMutex.unlock();
    forceImmediateProcessing();
}

void SoundGenerator::registerAllNotesOff()
{
    mToneMutex.lock();
    QList<Tone> copy = mTones;
    mToneMutex.unlock();
    mQueueMutex.lock();
    for (Tone &tone : copy)
    {
        Request request;
        request.command = Request::NOTE_OFF;
        request.key = tone.mKey;
        mQueue.enqueue(request);
    }
    mQueueMutex.unlock();
    mSustainPedeal = mSostenutoPedal = mSoftPedal = false;
    forceImmediateProcessing();
}


//-----------------------------------------------------------------------------
//                          Thread worker function
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Private thread worker function
/// \details

void SoundGenerator::timeout()
{
    if (isInterruptionRequested() or not isActive()) return;
    while (queueFilled())
    {
        mQueueMutex.lock();
        Request request = mQueue.dequeue();
        mQueueMutex.unlock();
        handleRequest(request);
    }
    mToneMutex.lock();
    manageToneDynamics();
    mToneMutex.unlock();
}


//-----------------------------------------------------------------------------
//                         Handle a queued request
//-----------------------------------------------------------------------------

void SoundGenerator::handleRequest (Request &request)
{
    //    available data:
    //    request.channel; // still ignored here
    //    request.command; // enum Command in request
    //    request.data;
    //    request.intensity;
    //    request.note
    if (not pVoice) return;

    switch(request.command)
    {
    case Request::NOTE_ON:
    {
        int key = request.key;
        double volume1 = 0.6, volume2 = 0;
        if (pVoice->isVolumeDynamic())
        {
            if (pVoice->getNumberOfScales()==1) volume1 = pow(request.intensity,0.75);
            else
            {
                volume2 = pow(request.intensity,0.5);
                volume1 = 2*pow(request.intensity,2.5);
             }
        }

        if (volume1 > 0 and pVoice->getNumberOfScales() > 0)
        {
            Wave &sample = pVoice->getScale(0).getWave(key);
            if (sample.waveFormExists())
            {
                Tone::Type type = (key > pVoice->getHighestDampedKey() ?
                     Tone::Type::SUSTAIN_NONENDING : Tone::Type::SUSTAIN_ENDING);
                Tone T(key,0,sample,volume1,type);
                mToneMutex.lock();
                mTones.push_back(T);
                mToneMutex.unlock();
            }
        }
        if (volume2 > 0 and pVoice->getNumberOfScales() > 1)
        {
            Wave &sample = pVoice->getScale(1).getWave(key);
            if (sample.waveFormExists())
            {
                Tone::Type type = (key > pVoice->getHighestDampedKey() ?
                     Tone::Type::SUSTAIN_NONENDING : Tone::Type::SUSTAIN_ENDING);
                Tone T(key,1,sample,volume2,type);
                mToneMutex.lock();
                mTones.push_back(T);
                mToneMutex.unlock();
            }
        }
        break;
    }
    case Request::NOTE_OFF:
    {
        int key = request.key;
        mToneMutex.lock();
        for (auto &tone : mTones)
            if (tone.mKey == key
                    and tone.mState == State::PLAYING
                    and tone.mType == Tone::Type::SUSTAIN_ENDING)  // no release above key 92 hard coded
                tone.mState = State::RELEASE_REQUESTED;
        mToneMutex.unlock();
        break;
    }
    case Request::ALL_NOTES_OFF:
        mToneMutex.lock();
        for (auto &tone : mTones)
            if (tone.mState == State::PLAYING)
                tone.mState = State::RELEASE_REQUESTED;
        mToneMutex.unlock();
        break;

    case Request::OVERALL_PITCH_UP:
        break;
    case Request::OVERALL_PITCH_DOWN:
        break;
    case Request::UNDEFINED:
    default:
        LOGWARNING << "Sound::run: Request undefined";
    }
}


//-----------------------------------------------------------------------------
//                              Manage envelope
//-----------------------------------------------------------------------------

void SoundGenerator::manageToneDynamics()
{
    double const releaseVolume = 0.005;
    double const terminationVolume = 0.0001;

    bool exit = false;

    // Check all tones for being below cutoff volume
    // Tone mutex is locked when calling the function
    for (auto &tone : mTones)
    {
        switch(tone.mState)
        {
        case State::PLAYING:
        case State::OFF_PLAYING:
            if (tone.mVolume < releaseVolume)
            {
                LOGSTATUS << "Released key" << tone.mKey <<  "below cutoff volume";
                tone.mState = State::RELEASED;
            }
            break;
        case State::RELEASE_REQUESTED:
            {
                int key = tone.mKey;

                // Play at least 1/10 seconds
                if (tone.mTime < getIncrement(key)*mSampler.getSampleRate()/10) break;

                // Do not release if sustain pedal is pressed
                if (mSustainPedeal and tone.mVolume > releaseVolume) break;

                LOGSTATUS << "Retriggering off key" << key;
                tone.mState = State::RELEASED;
                int scale = tone.mScaleIndex;
                Wave &sample = pVoice->getScale(scale).getWave(key);
                if (not sample.releaseExists()) break;

                quint32 arrayindex = (tone.mTime>>11) & 0x1FFFFEU;
                double env = tone.getSample().getEnvelope(arrayindex);
                double vol=0;

                // TODO volume control both here and in importer
                // hard-coded for Hofstallstrasse
                switch(tone.mScaleIndex)
                {
                case 0:
                    {
                        vol = pow(env/500.0*tone.mVolume,0.5);
                        if (key<30) vol *= pow(key/30.0,3);
                    }
                    break;
                case 1:
                    {
                        vol = pow(env/80.0*tone.mVolume,0.35);
                    }
                    break;
                default: {}
                }

                if (vol>5) vol=5;

                Tone T(key,scale,sample,vol,Tone::Type::RELEASETONE);
                T.mState = State::OFF_PLAYING;
                mTones.push_back(T);
                exit = true;
            }
            break;
        case State::RELEASED:
            {
                if (tone.mVolume < terminationVolume)
                {
                    LOGSTATUS << "Terminated key" << tone.mKey << "below infinitesimal volume";
                    tone.mState = State::TERMINATED;
                }
            }
            break;
        default:
            break;
        }
        if (exit) break;
        if (not mQueue.empty()) return;
    }


    // remove the terminated entries from the mTones vector
    for (auto it = mTones.begin(); it != mTones.end(); /* no increment */)
    {
        if (it->mState == State::TERMINATED)
        {
            LOGSTATUS << "Remove key" << it->mKey << "from mTones";
            it = mTones.erase(it);
            if (not mQueue.empty()) return;
        }
        else ++it;
    }
}




bool SoundGenerator::queueFilled()
{
    QMutexLocker locker (&mQueueMutex);
    return not mQueue.empty();
}

quint32 SoundGenerator::getIncrement(int key)
{
    QMutexLocker locker(&mIncrementMutex);
    return mIncrements[key & 0x7F];
}

void SoundGenerator::setIncrement(int key, quint32 increment)
{
    QMutexLocker locker(&mIncrementMutex);
    mIncrements[key&0x7F] = increment;
}

void SoundGenerator::clearIncrements()
{
    QMutexLocker locker(&mIncrementMutex);
    mIncrements.resize(128);
    mIncrements.fill(0x1000);
}



//-----------------------------------------------------------------------------
//                         Register a tuning correction
//-----------------------------------------------------------------------------

void SoundGenerator::receiveTuningCorrections(QMap<int,double> corrections)
{
    if (not pVoice) return;

    // fast function approximating 0x1000*2^(cent/1200)

    double factor = 4096.0 * (44100.0/getSampler().getSampleRate());
    auto f = [factor] (double cent) { return static_cast<int>( 0.5 + factor*(1. + 0.00057762265*cent + 1.6682396E-7*cent*cent)); };

    for (auto it = corrections.begin(); it!=corrections.end(); it++)
    {
        if (it.key()<0 or it.key()>127) continue;
        setIncrement(it.key(),f(it.value() - pVoice->getRecordedPitch(it.key())));
    }
}

void SoundGenerator::setSustainPedal(bool pressed)
{
    LOGSTATUS << "Sustain pedal =" << pressed;
    mSustainPedeal = pressed;
}

void SoundGenerator::setSostenutoPedal(bool pressed)
{
    LOGSTATUS << "Sostenuto pedal =" << pressed;
    mSostenutoPedal = pressed;
}

void SoundGenerator::setSoftPedal(bool pressed)
{
    LOGSTATUS << "Sostenuto pedal =" << pressed;
    mSoftPedal = pressed;
}



