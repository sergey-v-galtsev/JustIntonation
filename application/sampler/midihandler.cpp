/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                Midi handler
//=============================================================================

#include "midihandler.h"

#include <QDebug>

MidiHandler::MidiHandler(SoundGenerator &soundgenerator, AudioOutput &audiooutput)
    : ThreadBase()
    , pSoundGenerator(&soundgenerator)
    , pAudioOutput(&audiooutput)
    , mDrumChannels()
    , mModulationWheel(-1)
    , mPitchBendWheel(-1)
    , mTemperamentIndex(1)
{
    setThreadName("MidiHandler");
    //mDrumChannels = {0x09};
}


#define STATUS  if (mVerbosity >= 4) qDebug() << "MidiHandler:"
#define MESSAGE if (mVerbosity >= 3) qDebug() << "MidiHandler:"
#define WARNING if (mVerbosity >= 2) qWarning()<<"MidiHandler: WARNING:"
#define ERROR   if (mVerbosity >= 1) qCritical() << "MidiHandler: ERROR:"




//-----------------------------------------------------------------------------
//                    Receive and interpret a MIDI event
//-----------------------------------------------------------------------------

// https://www.midi.org/specifications/item/table-1-summary-of-midi-message

void MidiHandler::receiveMidiEvent (const QMidiMessage &event)
{
    if (not isActive()) return;
    //qDebug() << "Received midi: " << event;

    Request request;
    request.channel = event.byte0() & 0x0F;
    request.key = event.byte1();
    request.intensity = static_cast<double>(event.byte2())/127.0;
    request.data = 0;

    auto send = [&request,this] (Request::Command c)
    {
        request.command=c;
        if (pSoundGenerator) pSoundGenerator->registerRequest(request);
    };

    switch (event.byte0() & 0xF0)
    {
    case 0x80: // Note off
        send(Request::NOTE_OFF);
        break;

    case 0x90: // Note on
        // TODO hit the drum
        if (mDrumChannels.contains(event.byte0()&0x0F)) {}
        else if (event.byte2() == 0) send(Request::NOTE_OFF);
        else send(Request::NOTE_ON);
        break;

    case 0xA0: // Polyphonic key pressure (aftertouch)
        break;

    case 0xB0: // Control change
        switch (event.byte1())
        {
        case 1: // Modulation wheel
        {
            if (event.byte2() != mModulationWheel)
            {
                int tempindex = mTemperamentIndex;
                if (event.byte2() > 0x60 and tempindex != 1) tempindex = 1;
                if (event.byte2() < 0x20 and tempindex != 0) tempindex = 0;
                if (mTemperamentIndex!=tempindex)
                {
                    setQmlTemperamentIndex(tempindex);
                    mTemperamentIndex = tempindex;
                    qDebug() << "Modulation wheel switches to temperament " << tempindex;
                }
            }
            mModulationWheel = event.byte2();
            break;
        }
        case 7: // volume change
        {
            double volume = 0.3 + 0.7 * event.byte2() / 127.0;
            pAudioOutput->setVolume(volume);
            break;
        }
        case 64: // Sustain pedal (right)
            emit signalSustainPedal(event.byte2() != 0);
            break;
        case 66: // Sostenuto pedal (middle)
            emit signalSostenutoPedal(event.byte2() != 0);
            break;
        case 67: // Soft pedal (left)
            emit signalSoftPedal(event.byte2() != 0);
            break;
        case 123: // All notes off
            pSoundGenerator->registerAllNotesOff();
        }

        break;

    case 0xC0: // Program change
        if (event.byte1() > 113) // if a drum is set
        {
            mDrumChannels.push_back(event.byte0()&0x0F);
            LOGSTATUS << "Added drum on channel" << (event.byte0()&0x0F);
        }
        break;

    case 0xD0: // Channel pressure aftertouch
        break;

    case 0xE0: // Pitch bend change
        if (mPitchBendWheel != event.byte2())
        {
            if ((event.byte2() > mPitchBendWheel and event.byte2() > 0x40) or
                (event.byte2() < mPitchBendWheel and event.byte2() < 0x40))
                emit changeTargetFrequency(1.0/64*(event.byte2()-mPitchBendWheel));
        }
        mPitchBendWheel = event.byte2();
        break;


    case 0xF0: // System common messages
        break;
    }
}

