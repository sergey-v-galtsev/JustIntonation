/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef TOUCHSCREENKEYBOARD_H
#define TOUCHSCREENKEYBOARD_H

#include <QObject>
#include <QSet>
#include <QVariant>
#include <QMidiMessage>

#include "system/shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Touchscreen keyboard
/// \details This class manages the data flow related to the touchscreen
/// keyboard. The keyboard itself (its geometry and so on) is realized on
/// the Qml side. The Qml Keyboard object yields signals containing a list
/// with the MIDI-compliant indices of the pressed keys. The keyboard is
/// insensitive to the intensity of the touch, i.e., all notes are played
/// at constant volume.
///
/// The touchscreen keyboard can operate in two different modes. In the
/// touchscreen mode the key is pressed only as long as it is actually
/// touched. In the toggle mode one can toggle the keys by tapping or
/// clicking between 'pressed' and 'not pressed'.
///////////////////////////////////////////////////////////////////////////////

class TouchscreenKeyboard : public QObject
{
    Q_OBJECT
public:
    TouchscreenKeyboard();

    bool init()   { mActive = true; return true; }
    bool start()  { mActive = true; return true; }
    bool stop()   { clear(); mActive = false; return true; }
    bool exit()   { clear(); mActive = false; return true; }
    void suspend(){ mActive = false; }
    void resume() { mActive = true; }

public slots:
    void receiveTouchpadKeyboardEvent (QVariant keys);
    void setToggleMode (bool on) { clear(); mToggleMode=on; }
    void clear();

signals:
    /// Signal: On keypress send the corresponding Midi event to a different module
    void sendTouchpadMidiEvent (QMidiMessage event);
    /// Signal: Tells the Qml layer to highlight pressed keys.
    void highlightKey (QVariant key, QVariant on);

private:
    void switchNote (int key, bool on);

    QSet<int> mPressedKeys;             ///< Keeps track of pressed keys
    QVector<bool> mSoundActive;         ///< Flag wether the sound is turned on
    bool mToggleMode;                   ///< Flag for desktop toggling mode
    bool mActive;                       ///< Flag that allows to send data
};

#endif // TOUCHSCREENKEYBOARD_H
