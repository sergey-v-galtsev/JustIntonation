/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//       Midi wrapper class, connecting QtMidi with JustIntonation
//=============================================================================

#include "midi.h"

#include <QMidiSystemNotifier>


//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor without functionality
///////////////////////////////////////////////////////////////////////////////

Midi::Midi()
    : QObject()
    , pConnector()
    , pCurrentInputDevice(nullptr)
    , pCurrentOutputDevice(nullptr)
{
    setModuleName("Midi");

    // The notifier informs us about changes in the Midi devices and their connections
    auto notifier = new QMidiSystemNotifier(this);
    connect(notifier, &QMidiSystemNotifier::inputDeviceAttached, this, &Midi::inputDeviceAttached);
    connect(notifier, &QMidiSystemNotifier::outputDeviceAttached, this, &Midi::outputDeviceAttached);
    connect(notifier, &QMidiSystemNotifier::inputDeviceDetached, this, &Midi::inputDeviceDetached);
    connect(notifier, &QMidiSystemNotifier::outputDeviceDetached, this, &Midi::outputDeviceDetached);

    connect(notifier, &QMidiSystemNotifier::inputDeviceCreated, this, &Midi::inputDeviceCreated);
    connect(notifier, &QMidiSystemNotifier::outputDeviceCreated, this, &Midi::outputDeviceCreated);
    connect(notifier, &QMidiSystemNotifier::inputDeviceDeleted, this, &Midi::inputDeviceDeleted);
    connect(notifier, &QMidiSystemNotifier::outputDeviceDeleted, this, &Midi::outputDeviceDeleted);

    // The AutoConnector is a special instance which controlls automatic connections
    pConnector = new QMidiAutoConnector(this);
    pConnector->setForceSingleInputDevice();
    pConnector->setForceSingleOutputDevice();

    connect(pConnector, &QMidiAutoConnector::inputDeviceCreated,this,&Midi::acInputDeviceCreated);
    connect(pConnector, &QMidiAutoConnector::outputDeviceCreated,this,&Midi::acOutputDeviceCreated);
    connect(pConnector, &QMidiAutoConnector::deviceDeleted,this,&Midi::acDeviceDeleted);
}


//-----------------------------------------------------------------------------
//                Initialization, making connections with Qml
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialize the Midi wrapper and connect it with Qml
/// \param qml : Pointer to qml root object
/// \return true
///////////////////////////////////////////////////////////////////////////////

bool Midi::init(QObject* qml)
{
    // On device changes send an unpdated list of devices to Qml
    connect(this,SIGNAL(onInputDevicesChanged(QVariant)),
            qml,SLOT(setQmlMidiInputDeviceNames(QVariant)));
    connect(this,SIGNAL(onOutputDevicesChanged(QVariant)),
            qml,SLOT(setQmlMidiOutputDeviceNames(QVariant)));

    // If the user selects an entry of the Combo box select the corresponding device
    connect(qml,SIGNAL(onSelectedMidiInputDeviceNameChanged(QString)),
            this,SLOT(selectInputDevice(QString)));
    connect(qml,SIGNAL(onSelectedMidiOutputDeviceNameChanged(QString)),
            this,SLOT(selectOutputDevice(QString)));

    // If the currently connected device changes update the headline in blue
    connect(this,SIGNAL(onCurrentInputDeviceChanged(QVariant)),
            qml,SLOT(setQmlSelectedMidiInputDeviceName(QVariant)));
    connect(this,SIGNAL(onCurrentOutputDeviceChanged(QVariant)),
            qml,SLOT(setQmlSelectedMidiOutputDeviceName(QVariant)));

    // If the user selects/deslects automatic connection these slots will be called:
    connect(qml,SIGNAL(onAutomaticMidiInputModeChanged(bool)),
            this,SLOT(setAutomaticInputMode(bool)));
    connect(qml,SIGNAL(onAutomaticMidiInputModeChanged(bool)),
            pConnector,SLOT(setAutoConnectToInput(bool)));
    connect(qml,SIGNAL(onAutomaticMidiOutputModeChanged(bool)),
            this,SLOT(setAutomaticOutputMode(bool)));
    connect(qml,SIGNAL(onAutomaticMidiOutputModeChanged(bool)),
            pConnector,SLOT(setAutoConnectToOutput(bool)));

    // Initialize available devices
    updateAvailableDevices(QMidi::MidiInput);
    updateAvailableDevices(QMidi::MidiOutput);
    emit onStatusChanged();

    return true;
}


//-----------------------------------------------------------------------------
//                 Send a Midi message to the output device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Send a Midi message to the output device.
/// \details This function forwards a QMidiMessage to QMidi-Output.
/// Moreover, if the verbosity is high, the hexadecimal numbers of the
/// message will be sent to qDebug().
/// \param event : qMidiMessage structure holding a Midi message
///////////////////////////////////////////////////////////////////////////////

void Midi::sendMidiMessage(const QMidiMessage &event)
{
    emit sendMidiEventToOutputDevice(event);

    if (getVerbosity()>=4)
    {
        auto hexadec = [](int i) {
            QString s=QString("%1").arg(i , 0, 16);
            if (s.length() == 1) s.prepend('0');
            return s;
        };
        if (getVerbosity()>=4)
        qDebug() << "Midi send " << hexadec(event.byte0())
                  << hexadec(event.byte1()) << hexadec(event.byte2());
    }
}


//-----------------------------------------------------------------------------
//                     Private slot: Input device attached
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Input device attached
/// \param info : QMidiDeviceInfo holding information about the new device
///////////////////////////////////////////////////////////////////////////////

void Midi::inputDeviceAttached(const QMidiDeviceInfo info)
{
    LOGMESSAGE << "New Midi input device attached";
    LOGSTATUS << info;
    updateAvailableDevices(QMidi::MidiInput);
}


//-----------------------------------------------------------------------------
//                     Private slot: Output device attached
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Output device attached
/// \param info : QMidiDeviceInfo holding information about the new device
///////////////////////////////////////////////////////////////////////////////

void Midi::outputDeviceAttached(const QMidiDeviceInfo info)
{
    LOGMESSAGE << "New Midi output device attached";
    LOGSTATUS << info;
    updateAvailableDevices(QMidi::MidiOutput);
}


//-----------------------------------------------------------------------------
//                     Private slot: Input device detached
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Input device detached
/// \param info : QMidiDeviceInfo holding information about the removed device
///////////////////////////////////////////////////////////////////////////////

void Midi::inputDeviceDetached(const QMidiDeviceInfo info)
{
    LOGMESSAGE << "Midi input device detached";
    LOGSTATUS << info;
    updateAvailableDevices(QMidi::MidiInput);
}


//-----------------------------------------------------------------------------
//                     Private slot: Output device detached
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Output device detached
/// \param info : QMidiDeviceInfo holding information about the removed device
///////////////////////////////////////////////////////////////////////////////

void Midi::outputDeviceDetached(const QMidiDeviceInfo info)
{
    LOGMESSAGE << "Midi output device detached";
    LOGSTATUS << info;
    updateAvailableDevices(QMidi::MidiOutput);
}


//-----------------------------------------------------------------------------
//                      Private slot: Input device created
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Input device created (connected)
/// \details This slot is called when an input Midi device is connected.
/// \param device : Pointer to the device
///////////////////////////////////////////////////////////////////////////////

void Midi::inputDeviceCreated(const QMidiInput *device)
{
    QString devname = device->deviceInfo().deviceName();
    LOGMESSAGE << "New Midi Input device connected" << devname;
    emit onCurrentInputDeviceChanged(devname);
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//                      Private slot: Output device created
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Output device created (connected)
/// \details This slot is called when an output Midi device is connected.
/// \param device : Pointer to the device
///////////////////////////////////////////////////////////////////////////////

void Midi::outputDeviceCreated(const QMidiOutput *device)
{
    QString devname = device->deviceInfo().deviceName();
    LOGMESSAGE << "New Midi Output device connected" << devname;
    emit onCurrentOutputDeviceChanged(devname);
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//                      Private slot: Input device deleted
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Input device deleted (disconnected)
/// \details This slot is called when an input Midi device is connected.
/// \param info : Device invo
///////////////////////////////////////////////////////////////////////////////

void Midi::inputDeviceDeleted (QMidiDeviceInfo info)
{
    LOGMESSAGE << "Midi Input device disconnected" << info.deviceName();
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//                      Private slot: Output device deleted
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Output device deleted (disconnected)
/// \details This slot is called when an output Midi device is connected.
/// \param info : Device information
///////////////////////////////////////////////////////////////////////////////

void Midi::outputDeviceDeleted(QMidiDeviceInfo info)
{
    LOGMESSAGE << "Midi Output device disconnected" << info.deviceName();
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//         Private slot: Automatically connected input device created
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Automatically connected input device created
/// \param dev : Pointer to the created device
///////////////////////////////////////////////////////////////////////////////

void Midi::acInputDeviceCreated(const QMidiDevice *dev)
{
    QString devName = dev->deviceInfo().deviceName();
    LOGMESSAGE << "Automatically connected to Input" << devName;
    auto input = qobject_cast<const QMidiInput*>(dev);
    connect(input, &QMidiInput::notify, this, &Midi::receiveMessage);
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//         Private slot: Automatically connected output device created
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Automatically connected output device created
/// \param dev : Pointer to the created device
///////////////////////////////////////////////////////////////////////////////

void Midi::acOutputDeviceCreated(const QMidiDevice *dev)
{
    QString devName = dev->deviceInfo().deviceName();
    LOGMESSAGE << "Automatically connected to Output" << devName;
    auto output = qobject_cast<const QMidiOutput*>(dev);
    connect(this,&Midi::sendMidiEventToOutputDevice,output,&QMidiOutput::receiveMidiMessage);
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//    Private slot: Automatically connected device deleted (disconnected)
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Automatically connected device deleted (disconnected)
/// \param info : QMidiDeviceInfo containing information about the deleted device
/// \param mode : Input or output mode
///////////////////////////////////////////////////////////////////////////////

void Midi::acDeviceDeleted(const QMidiDeviceInfo info, QMidi::Mode mode)
{
    LOGMESSAGE << "Automatically disconnected from" << info.deviceName() << mode;
    emit onStatusChanged();
}


//-----------------------------------------------------------------------------
//     Private slot: Receiving a Midi message from an external Midi device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private slot: Receiving a Midi message from an external Midi device
/// \details Forwards the message to the corresponding signal. If the verbosity is
/// sufficiently high the message is sent in hexadecimal numbers to qDebug()
/// \param m : QMidiMessage that was received.
///////////////////////////////////////////////////////////////////////////////

void Midi::receiveMessage (const QMidiMessage &m)
{
    emit receivedMidiMessage(m);
    if (getVerbosity()>=4)
    {
        QString out(QString::fromLatin1("Midi message received. "));
        if (m.size() > 0) {
            out += QString("Cmd: %1").arg(m[0]);
        }
        for (int i = 1; i < m.size(); ++i) {
            out += QString(", Byte%1: %2").arg(QString::number(i), QString::number(m[i]));
        }
        LOGSTATUS << out;
    }
}


//-----------------------------------------------------------------------------
//                     Disconnect current input device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Disconnect current input device
///////////////////////////////////////////////////////////////////////////////

void Midi::disconnectInput()
{
    if (pCurrentInputDevice)
    {
        disconnect(pCurrentInputDevice, &QMidiInput::notify,
                   this, &Midi::receiveMessage);
        pCurrentInputDevice->deleteLater();
        pCurrentInputDevice = nullptr;
    }
}


//-----------------------------------------------------------------------------
//                  Disconnect current output device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Disconnect current output device
///////////////////////////////////////////////////////////////////////////////

void Midi::disconnectOutput()
{
    if (pCurrentOutputDevice)
    {
        disconnect(this,&Midi::sendMidiEventToOutputDevice,
                   pCurrentOutputDevice,&QMidiOutput::receiveMidiMessage);
        pCurrentOutputDevice->deleteLater();
        pCurrentOutputDevice = nullptr;
    }
}


//-----------------------------------------------------------------------------
//                           select input device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Select the input device from a given device name
/// \details This function is needed for Qml. In contrast to ordinary widgets,
/// where we can store the device info in the combo box as user data, the
/// Qml combo boxes can only store a 'model' in form of a QStringList.
/// This function selects the input device for a given deviceName.
/// \param deviceName : String of the device name
///////////////////////////////////////////////////////////////////////////////

void Midi::selectInputDevice (QString deviceName)
{
    LOGMESSAGE << "Selected input device" << deviceName;
    disconnectInput();
    auto devices = QMidiDeviceInfo::availableDevices(QMidi::MidiInput);
    QMidiDeviceInfo info;
    for (QMidiDeviceInfo &dev : devices)
        if (dev.deviceName()==deviceName) info=dev;
    if (info.isNull())
    {
        onCurrentInputDeviceChanged(tr("Inactive"));
    }
    else
    {
        LOGMESSAGE << "CONNECT" << info.deviceName();
        pCurrentInputDevice = new QMidiInput(info, this);
        connect(pCurrentInputDevice, &QMidiInput::notify,
                this, &Midi::receiveMessage);
    }
}


//-----------------------------------------------------------------------------
//                           select onput device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Select the output device from a given device name
/// \details This function is needed for Qml. In contrast to ordinary widgets,
/// where we can store the device info in the combo box as user data, the
/// Qml combo boxes can only store a 'model' in form of a QStringList.
/// This function selects the output device for a given deviceName.
/// \param deviceName : String of the device name
///////////////////////////////////////////////////////////////////////////////

void Midi::selectOutputDevice (QString deviceName)
{
    LOGMESSAGE << "Selected output device" << deviceName;
    disconnectOutput();
    auto devices = QMidiDeviceInfo::availableDevices(QMidi::MidiOutput);
    QMidiDeviceInfo info;
    for (QMidiDeviceInfo &dev : devices)
        if (dev.deviceName()==deviceName) info=dev;
    if (info.isNull())
    {
        onCurrentOutputDeviceChanged(tr("Inactive"));
    }
    else
    {
        pCurrentOutputDevice = new QMidiOutput(info, this);
        pCurrentOutputDevice->setForceTargetChannel(false,0);
        connect(this,&Midi::sendMidiEventToOutputDevice,
                pCurrentOutputDevice,&QMidiOutput::receiveMidiMessage);
    }

}


//-----------------------------------------------------------------------------
//                       Set automatic input mode
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Midi::setAutomaticInputMode
/// \param autoconnect : true = automatic, false = manual
///////////////////////////////////////////////////////////////////////////////

void Midi::setAutomaticInputMode (bool autoconnect)
{
    LOGMESSAGE << "Setting automatic Input mode =" << autoconnect;
    // TODO no action here?
}


//-----------------------------------------------------------------------------
//                       Set automatic output mode
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set automatic output mode
/// \param autoconnect : true = automatic, false = manual
///////////////////////////////////////////////////////////////////////////////

void Midi::setAutomaticOutputMode(bool autoconnect)
{
    LOGMESSAGE << "Setting automatic Output mode =" << autoconnect;
    // TODO no action here?
}


//-----------------------------------------------------------------------------
//                       Update the shown available devices
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Update the shown available devices
/// \param mode : Input or Output
///////////////////////////////////////////////////////////////////////////////

void Midi::updateAvailableDevices (QMidi::Mode mode)
{
    auto devices = QMidiDeviceInfo::availableDevices(mode);
    QStringList availableDevices;
    for (auto &dev : devices) availableDevices.append(dev.deviceName());
    availableDevices.prepend(tr("Inactive"));
    availableDevices.prepend(tr("[Choose Midi device]"));
    LOGSTATUS << "Update device names:" << availableDevices;
    if (mode==QMidi::MidiInput) emit onInputDevicesChanged (availableDevices);
    else if (mode==QMidi::MidiOutput) emit onOutputDevicesChanged (availableDevices);
    //if (mode==QMidi::MidiInput) mobileError(availableDevices.join('/'));
}

