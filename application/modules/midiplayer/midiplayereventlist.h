/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                      Midi Player Events and Event List
//=============================================================================

#ifndef MIDIPLAYEREVENTLIST_H
#define MIDIPLAYEREVENTLIST_H

#include <QObject>
#include <QMutex>
#include <QMultiMap>

#include "system/shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Structure used internally in the MidiPlayer to hold a Midi event.
/// \ingroup MidiPlayer
/// \details This structure holds the essential information of a Midi event,
/// namely, a command byte and two data bytes.
/// The codes of the command byte and the data bytes can be found e.g. in
/// https://www.midi.org/specifications
///////////////////////////////////////////////////////////////////////////////

struct MidiPlayerEvent
{
    MidiPlayerEvent(quint8 track=0, double deltaticks=0,
                    quint8 command=0, quint8 byte1=0, quint8 byte2=0);

    quint8 track;           ///< Midi track number
    double deltaticks;      ///< Time ticks elapsed since the last event
    quint8 command;         ///< Midi command byte
    quint8 byte1;           ///< Midi first argument byte
    quint8 byte2;           ///< Midi second argument byte (zero if none)
};


///////////////////////////////////////////////////////////////////////////////
/// \brief Class managing the EventList in the MidiPlayer.
/// \ingroup MidiPlayer
/// \details This class holds a time-ordered list of Midi events for playing.
/// The access is channeled through public functions in order to guarantee
/// thread-safety. The class also provides an iterator (cursor) that can be
/// advanded and controlled. This iterator is used for the actual playing
/// process.
///////////////////////////////////////////////////////////////////////////////

class MidiPlayerEventList
{
public:
    MidiPlayerEventList();

    void clear();                               // Clear all data
    void insert (double time,                  // Insert a new midi event
                 const MidiPlayerEvent &event);
    void rewind();                              // Reset iterator
    void advance();                             // Advance iterator
    bool atEnd() const;                         // Is iterator at the end?
    const MidiPlayerEvent getEvent() const;     // get iterator-pointed event
    quint32 getCumulativeTime() const;          // get cumulative time
    int getSize() const;                        // get total number of all events

    double getProgressInPercent() const;        // get actual playing process
    void setProgress (double percent);          // set iterator at a percentage

    void writeListInReadableForm (QString filename);
private:
    typedef QMultiMap<double,MidiPlayerEvent> MidiEventList;

    MidiEventList mEventList;                   ///< Instance of the event list
    quint32 mMaxTime;                           ///< Maximum time at the end of the list
    MidiEventList::Iterator pEventIterator;     ///< Iterator playing the notes
    mutable QMutex mAccessMutex;                ///< Mutex for access
};

#endif // MIDIPLAYEREVENTLIST_H
