/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Midi Player
//=============================================================================

#ifndef MIDIPLAYER_H
#define MIDIPLAYER_H

///////////////////////////////////////////////////////////////////////////////
/// \defgroup MidiPlayer Midi-Player module
/// \brief Simple MidiPlayer for reading Midi files in real time.
///
/// This module contains a simple Midi player which is able to read and
/// parse a Midi file (*.mid), rendering a sequence of Midi event signals
/// in real time. These Qt signals can be connected to a sampler or
/// synthesizer.
///
/// To include this library add the following lines to the project file:
/// \code
/// LIBS += -L<library-build-path>/ -lmidiplayer
///
/// # force relinking of the libraries
/// PRE_TARGETDEPS += <library-build-path>/libmidiplayer.a
/// \endcode
/// In the source code one needs to include the file midiplayer.h.
///
/// The Midi player interacts with other components exclusively via signals
/// and slots. To load a Midi file from a device call load(filename). Then
/// start playing by calling play(). The Player periodically calls the
/// signal sendMidiEvent (command,byte1,byte2,delta), transmitting the
/// Midi code to the synthesizer.
///
/// Note that the timing of this player is very simple. In addition, the use
/// of signals may introduce some latency and inaccuracy. However, these
/// inaccuracies seem to be acceptable in the context of the just intonation
/// project.
///
/// \note This library does depend on any other software components except Qt.
///////////////////////////////////////////////////////////////////////////////

#include <QVariant>
#include <QTimer>
#include <QMidiMessage>

#include "midiplayereventlist.h"
#include "midiplayerparser.h"
#include "../../application/modules/system/threadbase.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Midi Player
/// \ingroup MidiPlayer
/// \details The MidiPlayer reads a Midi file, stores it in an event list and
/// transmits them in real time to the synthesizer.
/// This class comprises all functions which are needed to actually
/// play the Midi file in real time. Its main part is the function timeout()
/// which calls itself again and again via timer signals. Whenever timeout()
/// is called it emits the corresponding Midi signals, calculates the timespan
/// between the current and the next event in milliseconds, and restarts the
/// timer. In addition, the class provides a basic control like play() and
/// pause().
/// \note This is a standalone module
/// independent of other libraries. It interacts with the outside world only
/// via Qt signals and slots in a thread-safe way.
///////////////////////////////////////////////////////////////////////////////

class MidiPlayer : public ThreadBase
{
    Q_OBJECT
public:
    MidiPlayer();                       // Constructor
    bool stop();                        // Stop thread
    bool isPlaying() const;             // returns true if playing
    void suspend();
    void resume();

signals:
    void signalMidiEvent (QMidiMessage event);
    void signalPlayingStatusChanged (QVariant playing);
    void signalProgressInPercent (QVariant percent);
    void signalError (QVariant msg);

public slots:
    void loadFile (QString filename, bool autostart=false);   // Load a song from a file
    void loadUrl (QString url);         // Load a song from a file (specify url)
    void setTempo (double factor);      // Change tempo by a scale factor (1=100%)
    void setMidiProgress(double percent);// Set progress
    void rewind();                      // Go back to beginning
    void togglePlayPause();             // Toggle between play and pause
    void setRepeatMode(bool on);        // Repeat midi file if ended
    void play();
    void pause();

    void receiveTempoData (bool smpte, double parameter);
    void reportError (QString msg);

private slots:
    void readAndParseMidiFile (QString filename, bool autostart);
signals:
    void helperSignalToReadMidiFile (QString filename, bool autostart);
    void setDisplayedMidiFilename (QVariant filename);

private:
    void allNotesOff();
    void setTempoFactor (double tempo);
    void midiTimeout();

    MidiPlayerEventList mEventList;
    bool mCurrentlyPlaying;             ///< Flag whether player is actually playing
    bool mRepeatMode;                   ///< Mode indicating that file shall be repeated
    bool mPlayAgainAfterSuspend;        ///< Flag for playing after resume
    bool mTimerActive;                  ///< Flag indicating that player is waiting for timeout
    double mOverallTempoFactor;         ///< Time scaling factor (1 = 100% = recorded tempo)
    quint64 mDeltaTicks;                ///< Elapsed time units (ticks) since the last event
    double mMillisecondsPerTick;        ///< Calculated number of milliseconds per tick
    double mTicksPerQuarterNote;        ///< Calculated number of ticks per quarter note
    double mAccumulatedTime;            ///< Accumulated time since last progress signal
};

#endif // MIDIPLAYER_H
