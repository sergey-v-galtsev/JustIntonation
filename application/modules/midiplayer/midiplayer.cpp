/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                              Midi Player
//=============================================================================

#include "midiplayer.h"
#include <QUrl>
#include <QFile>

//-----------------------------------------------------------------------------
//                                Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

MidiPlayer::MidiPlayer()
    : mEventList()
    , mCurrentlyPlaying(false)          // Flag whether player is actually playing
    , mRepeatMode(false)                // Flag indicating that playing should be repeated
    , mPlayAgainAfterSuspend(false)     // Flag for playing after resuming from suspend
    , mTimerActive(false)               // Flag indicating that player is waiting for timeout
    , mOverallTempoFactor(1)            // Time scaling factor (1 = 100% = recorded tempo)
    , mDeltaTicks(0)                    // Elapsed time units (ticks) since the last event
    , mMillisecondsPerTick(10)          // Calculated number of milliseconds per tick
    , mTicksPerQuarterNote(100)         // Calculated number of ticks per quarter note
    , mAccumulatedTime(0)               // Accumulated time since last progress signal

{
    setThreadName("MidiPlayer");
    setModuleName("Midiplayer");

    // The following helperSingal for reading Midi files is used to decouple the threads
    connect(this,&MidiPlayer::helperSignalToReadMidiFile,this,&MidiPlayer::readAndParseMidiFile);
}


//-----------------------------------------------------------------------------
//                                Stop player
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the player
/// \details This function stops the player so that it is no longer playing or
/// listening to its slots. Loaded MIDI data is not cancelled so that the
/// same music can be played again after restarting the player.
/// \return true if thread stopped normally
///////////////////////////////////////////////////////////////////////////////

bool MidiPlayer::stop()
{
    signalPlayingStatusChanged(false);
    mEventList.clear();
    return ThreadBase::stop();
}


//-----------------------------------------------------------------------------
//                              Load Midi file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Load a midi file (*.mid) from disk
/// \details This function requests the MidiPlayer to load a file.
/// The loading process is carried out in the independent thread, meaning
/// that this function returns immediately.
/// \param filename : Name of the file (including path)
/// \param autostart : Flag indicating that Midi player should start after loading
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::loadFile (QString filename, bool autostart)
{
    LOGMESSAGE << "Request for loading file" << filename;
    if (filename.size() == 0) return;
    allNotesOff();
    rewind();
    emit helperSignalToReadMidiFile(filename,autostart);
}

//-----------------------------------------------------------------------------
//                              Load Midi file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Load a midi file (*.mid) from disk (URL-Version)
/// \details This function requests the MidiPlayer to load a file.
/// The loading process is carried out in the independent thread, meaning
/// that this function returns immediately.
/// \param url : URL of the file ( file//:...)
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::loadUrl(QString url)
{
    QUrl URL(url);
    QString filename = URL.toLocalFile();
    if (filename.endsWith(".mid") or
            filename.endsWith(".MID")  or
            filename.endsWith(".midi"))
        loadFile (filename,true);
    else LOGMESSAGE << "No Midi file:" << filename;
}


//-----------------------------------------------------------------------------
//                          Set tempo scale factor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set tempo scale factor
/// \param factor : Scale factor (1 = 100% = neutral)
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::setTempo(double factor)
{
    LOGMESSAGE << "MidiPlayer: Set tempo factor to value" << factor;
    setTempoFactor(factor);
}


//-----------------------------------------------------------------------------
//                          Set progress manually
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set progress in pecent manually
/// \param percent
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::setMidiProgress(double percent)
{
    LOGMESSAGE << "Setting progress in percent manually:" << percent;
    allNotesOff();
    mEventList.setProgress(percent);
}


//-----------------------------------------------------------------------------
//                                  Rewind
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Rewind: Move the iterator of the event list to the beginning
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::rewind()
{
    allNotesOff();
    mEventList.rewind();
    emit signalProgressInPercent(0);
}


//-----------------------------------------------------------------------------
//                      Toggle between play and pause
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Toggle between play and pause
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::togglePlayPause()
{
    if (isPlaying()) pause();
    else play();
}


//-----------------------------------------------------------------------------
//                       Switch repeat mode on and off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Switch repeat mode on and off
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::setRepeatMode(bool on)
{
    LOGMESSAGE << "Repeat mode on =" << on;
    mRepeatMode = on;
}


//-----------------------------------------------------------------------------
//                             Start playing
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start or resume playing
/// \details This function starts the timer to begin playback after 20 msec.
/// Reading continues at the last position, e.g. after calling pause().
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::play()
{
    if (mEventList.atEnd()) rewind();
    mCurrentlyPlaying = true;
    // Start playing after 100 msec
    if (not mTimerActive)
    {
        QTimer::singleShot(100,this,&MidiPlayer::midiTimeout);
        mTimerActive = true;
    }
    signalPlayingStatusChanged(true);
}



//-----------------------------------------------------------------------------
//                                  Pause
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Pause: Interrupt playing
/// \details This function interrupts the playing loop without resetting the
/// iterator. In order to switch off all notes, the corresponding codes are
/// sent directly to the Midi device.
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::pause()
{
    LOGMESSAGE << "Pause";
    mCurrentlyPlaying = false;
    allNotesOff();
    signalPlayingStatusChanged(false);
}


//-----------------------------------------------------------------------------
//                                All notes off
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Turn all notes off
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::allNotesOff()
{
    // Send all notes off on all Midi channels
    for (quint8 command = 0xB0U; command < 0xC0U; command++)
    {
        LOGSTATUS << "signal Midi Event:"  << "\t\t\t"
               << QString::number(command,16) << QString::number(123,16) << "0";
        signalMidiEvent (QMidiMessage(command, 123, 0, 0.0));
    }
}

//-----------------------------------------------------------------------------
//                Find out whether the player is playing
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Find out whether the player is playing
/// \return True if playing
///////////////////////////////////////////////////////////////////////////////

bool MidiPlayer::isPlaying() const
{
    return mCurrentlyPlaying;
}

//-----------------------------------------------------------------------------
//                              Suspend playing
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Suspend (stop playing, thread idle)
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::suspend()
{
    LOGMESSAGE << "Entering suspend mode";
    mPlayAgainAfterSuspend = isPlaying();
    pause();
}


//-----------------------------------------------------------------------------
//                          Resume from suspend mode
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Resume from suspend mode
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::resume()
{
    LOGMESSAGE << "Resuming from suspend mode";
    if (mPlayAgainAfterSuspend) play();
    mPlayAgainAfterSuspend = false;
}


//-----------------------------------------------------------------------------
//                          Modify tempo factor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Modify the tempo scale factor
/// \param factor : Scale factor (1 = 100%)
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::setTempoFactor(double factor)
{ if (factor>0) mOverallTempoFactor = factor; }


//-----------------------------------------------------------------------------
//                            Report an error
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Report an error
/// \details If the verbosity level is nonzero the error is written to
/// qCritical(). Moreover, the function calls the signal sendError(msg) in
/// the MidiPlayer.
/// \param msg : Error string
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::reportError(QString msg)
{
    LOGERROR<< msg;
    emit signalError(msg);
}


//-----------------------------------------------------------------------------
//                       Read and parse a Midi file
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Read and parse a Midi file
/// \details This function reads a Midi file (*.mid) from the device, parses
/// its content and stores the extracted events in a time-ordered EventList.
/// \param filename : Name of the file
/// \param autostart : Flag for immediate playback after loading
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::readAndParseMidiFile (QString filename, bool autostart)
{
    LOGMESSAGE << "MidiPlayer load file:" << filename;

    // Open file and read its complete content into data
    QFile file(filename);
    if (not file.open(QIODevice::ReadOnly))
    { reportError("Cannot open Midi file "+filename); return; }
    mEventList.clear();
    QByteArray data = file.readAll();

    // Create the parser instance
    MidiPlayerParser parser (this,&mEventList,getVerbosity());

    // Parse file content
    LOGMESSAGE << "MidiPlayer start parsing...";
    parser.parse(data);
    LOGMESSAGE << "MidiPlayer parsing complete";

    // Disconnect signals, exit
    mEventList.rewind();
    if (autostart) play();
    const QStringList parts = filename.split("/");
    const QString barefilename = parts.at(parts.size()-1);
    emit setDisplayedMidiFilename(barefilename);
    mDeltaTicks = 0;
}


//-----------------------------------------------------------------------------
//                    Receive tempo data from the parser
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Receive tempo data from the parser
/// \details Most Midi files contain meta information about the tempo. When
/// the parser finds this information the present function is called. It
/// informs the MidiPlayer aboout the timing mode. In the Midi norm there
/// are two timing norms, an absolute so-called SMPTE timing and a quarter-note
/// varying timing. In both cases the transmitted parameter has a different
/// meaning.
/// \param smpte : True if SMPTE timing, false otherwise
/// \param parameter : For SMPTE milliseconds per ticks, otherwise ticks
/// per quarter note.
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::receiveTempoData(bool smpte, double parameter)
{
    if (smpte) mMillisecondsPerTick = parameter;
    else mTicksPerQuarterNote = parameter;
}


//-----------------------------------------------------------------------------
//                                midiTimeout
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Timer midiTimeout
/// \details This is the heart of the Midi player. It reads an event from the
/// EventList and transmits it by a signal to the Midi device. The time
/// difference between the current and the next event is calculated. If this
/// difference is zero, the function repeats itself recursively, handling the
/// next event in the queue. If the difference is non-zero, a QTimer is started
/// in the one-shot mode, returning to this function on midiTimeout.
///////////////////////////////////////////////////////////////////////////////

void MidiPlayer::midiTimeout()
{
    mTimerActive = false;
    if (not mCurrentlyPlaying) return;
    if (mEventList.atEnd()) { pause(); return; }

    quint32 currentTicks = mEventList.getCumulativeTime();
    MidiPlayerEvent event = mEventList.getEvent();

    // consider special meta-event for tempo change:
    if (event.command == 0xFFU)
    {
        double millisecondsPerQuarterNote = 256.0*event.byte1 + event.byte2;
        LOGMESSAGE << "MidiPlayer tempo change:"
                << millisecondsPerQuarterNote << "msec per quarter note";
        if (mTicksPerQuarterNote>0)
        {
            mMillisecondsPerTick = millisecondsPerQuarterNote / mTicksPerQuarterNote;
            LOGMESSAGE << "MidiPlayer: Now "
                     << mMillisecondsPerTick << "msec per delta tick";
        }
    }
    else // emit all other MIDI events to the player
        if (event.command < 0xE0 or event.command >= 0xF0) // except for pitch bends
        // because pitch bends are sometimes used to implement Midi in historical temperaments.
    {
        //double milliseconds = mOverallTempoFactor * mDeltaTicks * mMillisecondsPerTick;
//        qDebug() << "signal Midi Event:"  << "\t\t\t"
//               << QString::number (event.command,16)
//               << QString::number (event.byte1,16)
//               << QString::number (event.byte2,16);
        signalMidiEvent (QMidiMessage(event.command, event.byte1, event.byte2, 0.0));
    }

    mEventList.advance();
    if (mEventList.atEnd())
    {
        // if in repeat mode play the file again, else stop.
        if (mRepeatMode) play();
        else { pause(); return; }
    }

    quint32 nextTicks = mEventList.getCumulativeTime();
    mDeltaTicks = nextTicks - currentTicks;
    if (mDeltaTicks>0)
    {
        double milliseconds = mOverallTempoFactor * mDeltaTicks * mMillisecondsPerTick;
        LOGSTATUS << "Let the timer wait for" << milliseconds << "msec";
        QTimer::singleShot(milliseconds,this,&MidiPlayer::midiTimeout);
        mTimerActive = true;
        if (mAccumulatedTime > 500)
        {
            signalProgressInPercent(mEventList.getProgressInPercent());
            mAccumulatedTime = 0;
        }
        mAccumulatedTime += milliseconds;
    }
    else midiTimeout();
}

