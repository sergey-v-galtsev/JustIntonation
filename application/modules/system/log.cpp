/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                   Log - Base class managing system messages
//=============================================================================

#include "log.h"

int messageVerbosity = 4;       ///< Global verbosity level


int getVerbosity() { return messageVerbosity; }

//-----------------------------------------------------------------------------
//             Global function generating the log message string
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Global function generating the log message string
/// \param line : Line number
/// \param path : Full path of the function
/// \param function : Name of the function
/// \param level : Verbosity level
/// \return QString of the message
//////////////////////////////////////////////////////////////////////////////

QString createLogMessage (int line, QString path, QString function, int level)
{
    QStringList parts = path.split("/");
    QString filename = parts.at(parts.size()-1);
    QString str;
    QTextStream msg(&str);
    msg << "(" << function << " in " << filename << ":"  << line << "):";
    if (level>4) msg << "\nFull path: " << path << "\n";
    return str;
}


//-----------------------------------------------------------------------------
//                       Log - Base class constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the Log base class
/// \details If you would like to see the class name in the messages
/// derive your class from Log.
///////////////////////////////////////////////////////////////////////////////

Log::Log()
    : messageVerbosity(2)
    , mModule("Unknown Module")
{
}


//-----------------------------------------------------------------------------
//                            Set module name
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Specify the name of the class-specific module.
/// \details Usually several classes belong to a module. For classes derived
/// from Log it is possible to specify the module name. This should be done
/// in the constructor of the class.
/// \param name : Name of the module
///////////////////////////////////////////////////////////////////////////////

void Log::setModuleName (const QString &name) { mModule = name; }


//-----------------------------------------------------------------------------
//                          Get module name
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get module name
/// \return Name of the module, empty if it was not set before
///////////////////////////////////////////////////////////////////////////////

QString Log::getModuleName() const
{
    return mModule;
}


//-----------------------------------------------------------------------------
//                            Set verbosity
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the verbosity level of the class-specific messages
/// \param verbosity : Verbosity level (1...4)
///////////////////////////////////////////////////////////////////////////////

void Log::setVerbosity(int verbosity) { messageVerbosity = verbosity; }


//-----------------------------------------------------------------------------
//                            Get verbosity
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get the actual verbosity level
/// \return : Verbosity level (1...4)
///////////////////////////////////////////////////////////////////////////////

int Log::getVerbosity() { return messageVerbosity; }


//-----------------------------------------------------------------------------
//          Class-specific function generating the log message string
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Class-specific function generating the log message string
/// \param line : Line number
/// \param path : Full path of the function
/// \param function : Name of the function
/// \param level : Verbosity level
/// \return QString of the message
//////////////////////////////////////////////////////////////////////////////

QString Log::createLogMessage(int line, QString path, QString function, int level)
{
    QStringList parts = path.split("/");
    QString filename = parts.at(parts.size()-1);
    QString str;
    QTextStream msg(&str);
    msg << mModule << " (" << function << " in " << filename << ":"  << line << "):";
    if (level>4) msg << "\nFull path: " << path << "\n";
    return str;
}
