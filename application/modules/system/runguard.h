/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//        RunGuard, preventing multiple instances of the application
//============================================================================

#ifndef RUNGUARD_HPP
#define RUNGUARD_HPP

#include <QObject>
#include <QSharedMemory>
#include <QSystemSemaphore>

///////////////////////////////////////////////////////////////////////////////
/// \class RunGuard
/// \ingroup system
/// \brief Class for checking whether the application is already running.

/// Creating an instance of this class prevents the system from starting
/// serveral instances of the application.
///
/// Internally the instances communicate via a QSharedMemory labelled by a
/// given key. The access to this memory is mutexted by a QSystemSemaphore.
///
/// This mechanism is not needed on tablets and smartphones, where the
/// operating system itself makes sure that only one instance can be started.
/// Therefore, the essential code of this class is only compiled if
/// the Q_OS_DESKTOP label is set in the case of a desktop application.
///
/// The runguard is based on a similar one developed by Christoph Wick in
/// the Entropy Piano Tuner.
/// \see Q_OS_DESKTOP
///////////////////////////////////////////////////////////////////////////////

class RunGuard : public QObject
{
    Q_OBJECT
public:
    RunGuard (const QString &mKey);
    ~RunGuard();

    bool anotherInstanceIsRunning (bool showMessage=true);  // Is another instance running?

private:
    QString generateKeyHash (const QString& mKey, const QString& salt);
    void release();

    const QString mKey;          ///< Name assigned to the instance
    const QString mMemLockKey;   ///< Key labelling the semaphore lock
    const QString mSharedMemKey; ///< Key labelling the shared memory
    QSharedMemory mSharedMem;    ///< Shared memory labelled by a given key
    QSystemSemaphore mMemLock;   ///< Lock used when shared memory is accessed
};

#endif  // RUNGUARD_HPP

