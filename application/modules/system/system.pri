# Modules which are largely independent

HEADERS += \
    $$PWD/threadbase.h \
    $$PWD/threadworker.h \
    $$PWD/log.h \
    $$PWD/runguard.h \
    $$PWD/translator.h \     
    $$PWD/shared.h

SOURCES += \
    $$PWD/threadbase.cpp \
    $$PWD/threadworker.cpp \
    $$PWD/log.cpp \
    $$PWD/runguard.cpp \
    $$PWD/translator.cpp \


