/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//         Thread base - a universal base class for threaded modules
//=============================================================================

#include "threadbase.h"

#include <QDebug>

//-----------------------------------------------------------------------------
//                                Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
///////////////////////////////////////////////////////////////////////////////

ThreadBase::ThreadBase()
    : mThreadWorker(this)
    , mSuspended(false)
{
}


//-----------------------------------------------------------------------------
//                                 Destructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Destructor, stops the thread if it is still running.
///////////////////////////////////////////////////////////////////////////////

ThreadBase::~ThreadBase()
{ stop(); }


//-----------------------------------------------------------------------------
//                                Start thread
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start the thread
/// \details Starts the thread.
/// If the thread is suspended it will resume.
/// If the thread is already started the function does nothing.
/// \returns True on succes
///////////////////////////////////////////////////////////////////////////////

bool ThreadBase::start()
{
    ThreadBase::resume();
    return mThreadWorker.start();
}


//-----------------------------------------------------------------------------
//                                  Suspend
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Mark the thread as suspended
/// \details Calling this function suspends the thread and the event loop.
/// In addition the mSuspended flag is set.
///////////////////////////////////////////////////////////////////////////////

void ThreadBase::suspend()
{
    mThreadWorker.quit();
    mSuspended = true;
}


//-----------------------------------------------------------------------------
//                                  Resume
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Resume from the suspend mode
/// \details Restart the thread and clear the mSuspended flag
///////////////////////////////////////////////////////////////////////////////

void ThreadBase::resume()
{
    mThreadWorker.start();
    mSuspended = false;
}


//----------------------------- Setter functions ------------------------------

/// Set thread priority
void ThreadBase::setPriority(const QThread::Priority p)
{ mThreadWorker.setPriority(p); }

/// Set verbosity level of messages
void ThreadBase::setVerbosity(int verbosity)
{ Log::setVerbosity(verbosity); mThreadWorker.setVerbosity(verbosity); }

/// Set timer interval for the periodically called worker
void ThreadBase::setTimerInterval(const int msec, const int firstMsec)
{ mThreadWorker.setTimerInterval(msec,firstMsec); }

/// Set thread name (Linux only)
void ThreadBase::setThreadName(const QString name)
{ mThreadWorker.setThreadName(name); }


//------------------------------- Getter functions ----------------------------

/// Return true if the thread was requested to interrupt or terminate
bool ThreadBase::isInterruptionRequested() const
{ return mThreadWorker.isInterruptionRequested(); }

/// Get thread name.
QString ThreadBase::getThreadName() const
{ return mThreadWorker.getThreadName(); }

/// Return true if thread is running and not suspended.
bool ThreadBase::isActive() const
{ return mThreadWorker.isRunning() and not mSuspended; }


//-----------------------------------------------------------------------------
//                                 Stop thread
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Stop the thread.
/// \details Send a termination request to the execution loop. Wait for the
/// thread to terminate. The active components of the thread should call
/// isInterruptionRequested() and quit immediately if this function is true.
/// \return If the thread terminates regularly return true. If the thread does
/// not terminate after a timeout of 2 secs return false.
///////////////////////////////////////////////////////////////////////////////

bool ThreadBase::stop()
{
    mSuspended = false;
    return mThreadWorker.stop();
}


//-----------------------------------------------------------------------------
//                            Private slot: timeout
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Private timeout slot called by the worker
/// \details This slot is called periodically by the timer of the worker. If
/// the thread is not suspended it calles the user-defined virtual function
/// periodicallyCalledWorker();
///////////////////////////////////////////////////////////////////////////////

void ThreadBase::timeout()
{
    if (not mSuspended) periodicallyCalledWorker();
}
