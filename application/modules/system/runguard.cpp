/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//        RunGuard, preventing multiple instances of the application
//============================================================================

// The code is based on the RunGuard used in the Entropy Piao Tuner
// developed by Christoph Wick

#include "runguard.h"
#include "shared.h"

#include <QCryptographicHash>
#include <QMessageBox>

#include <iostream>

//-----------------------------------------------------------------------------
//                              Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor for a runguard.
///
/// The constructor creates a flag in the shared memory indicating that
/// the application is running. This allows other instances to detect an
/// already running application.
/// \param key : String naming the run guard (e.g. myapp_runguard)
///////////////////////////////////////////////////////////////////////////////

RunGuard::RunGuard (const QString& key)
    : mKey (key)
    , mMemLockKey   (generateKeyHash( key, "_memLockKey"  ))
    , mSharedMemKey (generateKeyHash( key, "_sharedmemKey"))
    , mSharedMem    (mSharedMemKey)
    , mMemLock      (mMemLockKey, 1)
{
    // Fix, see http://habrahabr.ru/post/173281/
    {
        QSharedMemory fix (mSharedMemKey);
        fix.attach();
    }

    mMemLock.acquire();
    const bool success = mSharedMem.create( sizeof( quint64 ) );
    mMemLock.release();
    if (not success) release();
}


//-----------------------------------------------------------------------------
//                              Destructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief RunGuard destructor
///
/// The destructor releases the blocking label. Since the destructor is
/// is called at the end of the main() function, this ensures that the
/// label is automatically removed as soon as the application stops.
///////////////////////////////////////////////////////////////////////////////

RunGuard::~RunGuard()
{
    release();
}


//-----------------------------------------------------------------------------
//                        Release current application
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Release the running application.
///
/// This function detaches the application from the shared memory,
/// removing the blocking label.
/// It has to be called when the application terminates.
/// \see ~RunGuard()
///////////////////////////////////////////////////////////////////////////////

void RunGuard::release()
{
    mMemLock.acquire();
    if (mSharedMem.isAttached()) mSharedMem.detach();
    mMemLock.release();
}


//-----------------------------------------------------------------------------
//                         Generate hash string
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Generate hash string for a given keys
///
/// This function converts a key (entropypianotuner_runguard) and a second
/// key extenstion (e.g._memLockKey) into a hash string.
/// \param key : String holding the key
/// \param salt : String holding the key extension
/// \return : String containg the hash
////////////////////////////////////////////////////////////////////////////////

QString RunGuard::generateKeyHash( const QString& key, const QString& salt )
{
    QByteArray data;

    data.append( key.toUtf8() );
    data.append( salt.toUtf8() );
    data = QCryptographicHash::hash( data, QCryptographicHash::Sha1 ).toHex();

    return data;
}


//-----------------------------------------------------------------------------
//                 Check whether antoher instance is running
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Check whether another instance of the application is running
///
/// This function first checks whether the own instance is already running.
/// Then it checks whether another application has already created the
/// shared memory, indicating that another instance of the same
/// applicaiton is running.
/// \return Boolean indicating whether another instance of the same
/// application is running.
///////////////////////////////////////////////////////////////////////////////

bool RunGuard::anotherInstanceIsRunning (bool showMessage)
{
    // If the shared memory is already attached to the own instance
    // there is certainly no other application running.
    if (mSharedMem.isAttached()) return false;

    // Otherwise, we have to check whether another instance has
    // already created the shared memory. To find that out we
    // try to attach ourselves. If this is possible
    // it indicates that another application has already created the
    // shared memory. If so, we detach immediately afterwards.
    mMemLock.acquire();
    const bool sharedMemAlreadyExists = mSharedMem.attach();
    if (sharedMemAlreadyExists) mSharedMem.detach();
    mMemLock.release();

    // Show an error message
    if (sharedMemAlreadyExists and showMessage)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Information"));
        msgBox.setText(tr("Just Intonation is already running."));
        msgBox.setInformativeText(tr("Please close the application before restarting."));
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setIcon(QMessageBox::Information);
        msgBox.exec();
    }
    return sharedMemAlreadyExists;
}

