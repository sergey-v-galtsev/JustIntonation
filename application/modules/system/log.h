/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                   Log - Base class managing log messages
//=============================================================================

#ifndef LOG_H
#define LOG_H

#include <qdebug.h>

//--------------- Workaround for errors on mobiles for debugging --------------

extern void mobileError(QString msg);

//------------- Helper macro calling the function message(...) ----------------

#ifdef __GNUC__
#define LOGMSG(level) createLogMessage(__LINE__ ,__FILE__,__PRETTY_FUNCTION__,level).toStdString().c_str()
#else
#define LOGMSG(level) createLogMessage(__LINE__ ,__FILE__,__FUNCTION__,level).toStdString().c_str()
#endif
//----------------------- Macros used by the user -----------------------------

#define LOGSTATUS  if (getVerbosity() >= 4) qDebug() << LOGMSG(4)
#define LOGMESSAGE if (getVerbosity() >= 3) qDebug() << LOGMSG(3)
#define LOGWARNING if (getVerbosity() >= 2) qWarning()  << "WARNING" << LOGMSG(2)
#define LOGERROR   if (getVerbosity() >= 1) qCritical() << "ERROR" << LOGMSG(1)

//--------------------------- Global defintions -------------------------------

extern int messageVerbosity;       ///< Verbosity of global error messages

//------------ Global function generating the log message string --------------

QString createLogMessage (int line, QString path, QString function, int level);

//------------------------ Non-global definitions -----------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Base class for managing log messages
/// \details
/// This class manages the console messages of the application. There are
/// four different types of messages:
/// - Status messages which appear frequently (e.g. timer timeout)
/// - Normal messages which appear on a certain action (e.g. open file)
/// - Warning messages
/// - Error messages
///
/// ### Usage:
/// Include log.h and use
/// \code
/// LOGSTATUS << ...
/// LOGMESSAGE << ...
/// LOGWARNING << ...
/// LOGERROR << ...
/// \endcode
/// with streamed arguments analog to qDebug(). The message will also include
/// the function name, the file name, and the line number. If the messages
/// originate from a class that is derived from Log, then the messages will
/// also contain the full class name. In addition, the name of the module
/// can be specified. The messages can be filtered by setting the
/// verbosity in the range from 1 (only errors) to 4 (all four types of messages).
///////////////////////////////////////////////////////////////////////////////

class Log
{
protected:
    Log();

public:
    void setVerbosity (int verbosity);
    int getVerbosity();

protected:
    /// \private
    QString createLogMessage (int line, QString path, QString function, int level);
    /// \private
    void setModuleName (const QString &name);
    /// \private
    QString getModuleName() const;

private:
    int messageVerbosity;                   ///< Verbosity of error messages
    QString mModule;                        ///< Name of the module
};

#endif // LOG_H
