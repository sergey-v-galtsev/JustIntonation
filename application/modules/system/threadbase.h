/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//         Thread base - a universal base class for threaded modules
//=============================================================================

#ifndef THREADBASE_H
#define THREADBASE_H

#include <QObject>

#include "log.h"
#include "threadworker.h"
#include "shared.h"

///////////////////////////////////////////////////////////////////////////////
/// \class ThreadBase
/// \brief Universal base class for threaded modules
///
/// This wrapper class provides an easy way to run derived classes in an
/// independent thread in an exec loop. It ensures that slots are automatically
/// executed in the new thread.
///
/// ## Usage:
/// - Optionally implemement init()/exit() functions (empty here)
/// - Optionally extend the start()/stop() and suspend/resume functions
/// - Implement signals and slots as usual
/// There are three different worker functions which may be overloaded (empty here):
/// - initiallyCalledWorker() called on start() in the new thread
/// - finallyCalledWorker() called on stop() in the new thread
/// - periodicallyCalledWorker() called on timeout in the new thread
/// The latter requires that the timer is started by calling
/// setTimerInterval(msec). You can also set the priority and the thread name.
///
/// ## Logic:
/// - init/exit is like installing a component and establishing all its connections
/// - start/stop is like on/off, starting the thread and stopping it
/// - suspend/resume is like mute, the thread remains active
///
/// \see ThreadWorker
///////////////////////////////////////////////////////////////////////////////

class ThreadBase : public QObject, public Log
{
    Q_OBJECT
public:
    /// Allow ThreadWorker to access the private elements of this class
    friend class ThreadWorker;

    ThreadBase();
    ~ThreadBase();

    virtual bool init() { return true; }  ///< Virtual initialization function (no functionality here)
    virtual bool exit() { return true; }  ///< Virtual exit function (no functionality here)

    virtual bool start();
    virtual bool stop();

    virtual void suspend();
    virtual void resume();

    bool isActive() const;
    void setVerbosity (int verbosity);
    int getVerbosity() { return mThreadWorker.getVerbosity(); } ///< Get verbosity level
    QString getThreadName() const;

protected:
    /// Virtual worker function called when the thread is starting
    virtual void initiallyCalledWorker() {}
    /// Virtual worker function called when the thread stops
    virtual void finallyCalledWorker() {}
    /// Virtual worker function called periodically from the timer
    virtual void periodicallyCalledWorker() {}

    void setPriority (const QThread::Priority p);
    void setTimerInterval (const int msec, const int firstMsec=0);
    void setThreadName (const QString name);
    bool isInterruptionRequested() const;

private slots:
    virtual void timeout();
private:
    ThreadWorker mThreadWorker;         ///< Instance of the worker
    bool mSuspended;                    ///< Flag indicating suspended state
};

#endif // THREADBASE_H
