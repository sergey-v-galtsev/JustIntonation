/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//////////////////////////////////////////////////////////////////////////////
/// \defgroup tuner Tuner Module
/// \brief Module containing the main algorithm for instant tuning in
/// just intonation.
/// \details The algorithm for tuning is modularized and largely
/// independent of the main application. It interacts with the outside world
/// solely via Qt signals and slots.
///
/// Usage:
/// ------
/// To use the tuner module in your application proceed as follows:
///
/// - Include the header file and create an instance of the tuner
/// \code
/// #include "tuner.h"
/// ...
/// Tuner myTuner;
/// \endcode
/// - Connect your Midi source with the tuner
/// \code
/// connect(&myMidiInput,&MidiInput::signalMidiEvent,&myTuner,&Tuner::receiveMidiEvent);
/// \endcode
/// This signal contains the three Midi bytes followed by a delta time marker,
/// following the usual Midi specifications.
/// - Connect the tuner with the microtonal synthesizer
/// \code
/// connect(&myTuner,&Tuner::signalTuningCorrections,&mySoundGenerator,&SoundGenerator::receiveTuningCorrections);
/// \endcode
/// This signal sends a QMap<int,double> to the sound-generating device,
/// containing key numbers mapped to the
/// corresponding tuning corrections in cents (relative to ET). This signal is
/// sent frequently (triggered roughly every 20 ms).
/// - Initialize and start the tuner
/// \code
/// myTuner.init();
/// myTuner.setEnvelopeParameters(10,2,1); // Approximate piano envelope
/// myTuner.start();
/// \endcode
///
/// Options:
/// --------
/// - You may interrupt the tuner by calling stop() and start().
/// - You
/// may also call enalbeTuning(false) to hear in between equal temperament
/// (zero pitch corrections) for comparison.
/// - You may set an overall target pitch by calling setFrequencyOfA4(),
/// specifying the targeted pitch in cents relative to the recorded pitch
/// of the sample. The algorithm will try to yield cent values around the
/// - You may allow the algorithm to deviate from the prescribed average pitch,
/// setting the setPitchCorrectionParameter(lambda) to a value less than 1.
/// In this case the resulting pitch drift will be sent periodically via
/// the signalAveragePitchDrift().
///
//////////////////////////////////////////////////////////////////////////////

