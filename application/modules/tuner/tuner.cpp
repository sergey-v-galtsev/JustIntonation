/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                  Tuner: Module for adaptive tuning
//=============================================================================

#include "tuner.h"
#include <math.h>

//-----------------------------------------------------------------------------
//                                Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor of the Tuner
/// \details Constructor of the Tuner, resetting its member variables
///////////////////////////////////////////////////////////////////////////////

Tuner::Tuner() : ThreadBase()
    , mTunerAlgorithm()
    , mGlobalTargetPitchInCents(0)
    , mTuningMode (1)
    , mStaticReferenceKey(0)
    , mWolfsIntervalShift(0)
    , mTuningEnabled(true)
    , mLastMessage("")
    , mIntervalSizes(12,0)
    , mIntervalWeights(12,1)
    , mKeyDataVector()
    , pElapsedTimer(nullptr)
    , mStaticTuningModeEnabled(false)
    , mPitchAutoCorrectionParameter(0.5)
    , mDelayParameter(0)
    , mMemoryOffFactor(0)
    , mWaitingTimeMsec(0)

{
    setThreadName("Tuner");
    setModuleName("Tuner");
    setMemoryLength(3);
}


//-----------------------------------------------------------------------------
//                                  Init
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialize the Tuner
/// \details Initialize the thread and clear all keys and pitch corrections.
///////////////////////////////////////////////////////////////////////////////

bool Tuner::init()
{
    ThreadBase::init();
    setTimerInterval(updateIntervalMsec);   // Initialize envelope timer
    mKeyDataVector.resize(128);
    for (int n=0; n<128; n++)               // Reset all keys
    {
        mKeyDataVector[n].clear();
        mKeyDataVector[n].key = n;
    }
    emitPitchCorrections();                 // Emit zero pitch corrections
    signalAveragePitchDrift(0);             // Pitch drift zero
    return true;
}


//-----------------------------------------------------------------------------
//                       Public slot: Start the tuner
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Start the tuner, starting the tuner thread.
/// \details Start the thread of the tuner and tell the GUI that the tuner
/// is ready for setting parameters such as the temperament and the weights
/// \return True on success
///////////////////////////////////////////////////////////////////////////////

bool Tuner::start()
{
    if (not ThreadBase::start()) return false;
    signalReadyForReceivingParameters(); // Ask the GUI to set the tuning parameters
    return true;
}


//-----------------------------------------------------------------------------
//                      Public slot: Set frequency of A4
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the desired target frequency of A4 (concert pitch)
/// \param f : Frequency in Hz (standard value is 440 Hz)
///////////////////////////////////////////////////////////////////////////////

void Tuner::setFrequencyOfA4 (double f)
{
    if (f<=0) return;
    double cents = 1200 * log(f/440.0) / log(2.0);
    // Do not accept pitch changes of more than two semitones
    if (cents > 200 or cents < -200)
    {
        LOGWARNING << "Unreasonable global pitch =" << cents;
        return;
    }
    LOGMESSAGE << "Setting global ptich to" << cents;
    mGlobalTargetPitchInCents = cents;
}


//-----------------------------------------------------------------------------
//                      Public slot: Set tuning mode
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set tuning mode
/// \param mode : Tuning mode (see TuningMode enum)
/// \param wolfsShift: Index shift determining the location of wolfs interval
///////////////////////////////////////////////////////////////////////////////

void Tuner::setTuningMode (int mode, int wolfsShift)
{
    mTuningMode = mode;
    mWolfsIntervalShift = wolfsShift;

    // If the tuning mode changes all sounding keys are declared
    // as being newly pressed. This ensures renewed tuning of those keys
    for (int i=0; i<128; ++i) if (mKeyDataVector[i].intensity>0)
        mKeyDataVector[i].newlyPressed=true;
    switch(mode)
    {
    case 0:
        LOGMESSAGE << "Tuning mode: ET: Equal temperament";
        break;
    case 1:
        LOGMESSAGE << "Tuning mode: JI: Dynamically tuned optimized just intonation";
        break;
    case 2:
        LOGMESSAGE << "Tuning mode: UT: Custom unequal temperament given by table";
        break;
    default:
        LOGERROR << "Undefined value for tuning mode";
    }
    LOGSTATUS << "Wolfs interval shift =" << mWolfsIntervalShift;
}


//-----------------------------------------------------------------------------
//                     Enable or disable tuning process
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public Slot: Enable or disable adaptive tuning
/// \details With this function it is possible to temporarily disable the
/// active tuning process. When disabled, the tuner is not stopped, rather it
/// remains active, sending zero pitches when necessary. This means that the
/// synthesizer plays in equal temperament. In the application this function
/// may be connected e.g. with the left pedal of the keyboard in order to
/// easily demonstrate the difference between just and equal temperament.
/// \param enable : True for active tuning, false otherwise
///////////////////////////////////////////////////////////////////////////////

void Tuner::enableTuning (bool enable)
{
    LOGMESSAGE << (enable ? "enable" : "disable") << " tunging algorithm";
    mTuningEnabled = enable;
    for (int i=0; i<128; ++i) if (mKeyDataVector[i].intensity>0)
        mKeyDataVector[i].newlyPressed=true;
}


//-----------------------------------------------------------------------------
//        Public slot: Set parameter for pitch progression compensation
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set the parameter for pitch progression compensation
/// \details One of the problems of non-ETs is that the global average pitch
/// begins to slide if certain sequences of intervals are being played
/// (this is also the reason why even good choirs tend to get out
/// of pitch after some time). The algorithm tries to compensate this
/// progression by a continuous adiabatic correction of the global
/// average pitch. The parameter $lambda$
/// which you can set here allows you to control the time scale of this
/// correction, varying from $lambda=0$ (none) to $lambda=1$ (instant).
/// \param lambda : parameter in the range from 0 (no correction) to
/// 1 (instant correction)
///////////////////////////////////////////////////////////////////////////////

void Tuner::setPitchProgressionCompensationParameter (double lambda)
{
    if (lambda<0 or lambda>1)
    {
        LOGWARNING << "Unreasonable correction parameter =" << lambda;
        LOGWARNING << "Value should be between 0 and 1.";
        return;
    }
    LOGMESSAGE << "Setting pitch progression parameter lambda =" << lambda;
    mPitchAutoCorrectionParameter = lambda;
}



//-----------------------------------------------------------------------------
//                       Public slot: Set delay parameter
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public Slot: Set delay parameter
/// \details This experimental option allows the user to specify a timespan
/// after which the tuning sets it. This means that a tone is first played
/// in ordinary equal temperament and then switches to the computed tune.
/// This may be used to demonstrate the difference between ET and UT.
////////////////////////////////////////////////////////////////////////////////

void Tuner::setDelayParameter (double delay)
{
    if (delay<0 or delay>10)
    {
        LOGWARNING << "Unreasonable delay parameter =" << delay;
        LOGWARNING << "Value should be between 0 and 10.";
        return;
    }
    LOGMESSAGE << "Setting delay parameter: delay=" << delay;
    mDelayParameter = delay;
}


//-----------------------------------------------------------------------------
//                       Public slot: Set memory parameter
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Set memory parameter
/// \param seconds : Memory duration in seconds
///////////////////////////////////////////////////////////////////////////////

void Tuner::setMemoryLength (double seconds)
{
    if (seconds<0 or seconds>10)
    {
        LOGWARNING << "Unreasonable memory duration parameter =" << seconds;
        LOGWARNING << "Value should be between 0 and 10.";
        return;
    }
    LOGMESSAGE << "Setting memory duration parameter: delay=" << seconds;
    mMemoryOffFactor = exp(-0.005*updateIntervalMsec/seconds);
}


//-----------------------------------------------------------------------------
//                   Main input slot: Receive Midi event
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public slot: Receive Midi event (Main input of the tuner)
/// \details This slot receives and interprets Midi keyboard data in real time.
/// \note Call this function exclusively via signal-slot. Otherwise the
/// tuning process would be executed in the calling thread.
/// \param event : Midi event of the type QMidiMessage (see library QMidi)
///////////////////////////////////////////////////////////////////////////////

void Tuner::receiveMidiEvent(QMidiMessage event)
{
    if (not isActive()) return;

    // get key number and intensity from the Midi message
    int key = event.byte1() & 0x7F;
    double intensity = static_cast<double>(event.byte2())/127.0;

    switch (event.byte0() & 0xF0)
    {
    case 0x90: // Press key: Note on
        if (intensity>0)
        {
            // Reset data of the newly pressed key
            mKeyDataVector[key].pressed = true;
            mKeyDataVector[key].newlyPressed = true;
            mKeyDataVector[key].emitted = false;
            mKeyDataVector[key].intensity = intensity;
            mKeyDataVector[key].memory = 0;
            mKeyDataVector[key].timeStamp = getNow();
            // Reset time variable to enforce immediate tuning
            mWaitingTimeMsec=0;
        }
        else mKeyDataVector[key].pressed = false;
        break;

    case 0x80: // Release key: Note off
        mKeyDataVector[key].pressed = false;
        break;

    case 0xB0: // Control command:
        {
            switch(event.byte1())
            {
            case 67: // Pressing the left pedal will turn off tuning
                enableTuning(event.byte2()==0);
                break;

            case 123: // Command to switch all notes off
                for (int k=0; k<128; ++k) mKeyDataVector[k].pressed = false;
                break;
            default:
                break;
            }
        }
        break;
    default:
        break;
    }
}


//-----------------------------------------------------------------------------
//                           Slot: Set interval size
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public Slot: Set interval size
/// This function allows the user to specify the temperament. To this end one
/// specifies the interval sizes relative to the equal temperament in cents.
/// The complete definition of the temperament requires to set all interval
/// sizes, meaning that this slot has to be called 11 times.
/// \param semitones : Number of halftones, specifying the interval.
/// For example, a perfect fourth consists of 5 halftones, a perfect fifth
/// of seven halftones.
/// \param cents : Deviation of the interval size relative to the equal
/// temperament in the range (-100..100).
///////////////////////////////////////////////////////////////////////////////

void Tuner::setIntervalSize (int semitones, double cents)
{
    if (semitones<1 or semitones>12)
    { LOGERROR << "Set cents: Number of halftones" << semitones << "must be between 1 and 12"; }
    else if (cents < -50 or cents > 50)
    { LOGERROR << "Set cents: Value" << cents << "out of allowed range (-50..50)"; }
    else mIntervalSizes[semitones%12] = cents;
}


//-----------------------------------------------------------------------------
//                           Slot: Set interval weight
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Public Slot: Set interval weight
/// The human perception of various intervals depends on their size. For example,
/// the human sense of hearing is very sensitive if a perfect fifth is out of
/// tune while deviations of a halftone, tritone, or a sept are less are more
/// likely to be tolerated. This depends on the number of matching partials
/// in the harmonic series. For key patterns that cannot be tuned in just
/// intonation the algorithm establishes a compromise based on a linear set of
/// equations. In this set each interval is weighet differently, allowing the
/// user to make a difference between "more important" and "less important"
/// intervals.
/// \param semitones : Number of halftones, specifying the interval.
/// For example, a perfect fourth consists of 5 halftones, a perfect fifth
/// of seven halftones.
/// \param weight : Weight parameter in the range 0..1
///////////////////////////////////////////////////////////////////////////////

void Tuner::setIntervalWeight (int semitones, double weight)
{
    if (semitones < 1 or semitones > 12)
    { LOGERROR << "Set weights: Number of halftones" << semitones << "must be between 1 and 12"; }
    else if (weight < 0 or weight > 1)
    { LOGERROR << "Set weights: Value" << weight << "out of allowed range (0..1)"; }
    else mIntervalWeights[semitones%12] = (weight==0 ? 0.000000001 : weight);
}


//-----------------------------------------------------------------------------
//                Emit the newly calculated pitch corrections
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Emit the pitch corrections
/// This functions checks to what extent the calculated pitches differ from
/// the previous ones. If the difference is larger than the centQuantization
/// the changes are sent to the sampler or to the external Midi device.
///////////////////////////////////////////////////////////////////////////////

void Tuner::emitPitchCorrections()
{
    const double centQuantization = 0.05;
    QMap<int,double> changes;
    qint64 now = getNow();
    for (KeyData &keydata : mKeyDataVector) if (keydata.intensity > 0)
    {
        // Handle delayed tuning if necessary
        bool tune = now - keydata.timeStamp >= 1000 * mDelayParameter;
        double pitch = mGlobalTargetPitchInCents + (tune ? keydata.pitch : 0);

        if (not keydata.emitted or
            fabs(pitch-keydata.previousPitch) > centQuantization)
            changes[keydata.key] =  keydata.previousPitch = pitch;
        keydata.emitted = true;
    }
    if (changes.size()>0)
    {
        emit signalTuningCorrections(changes);

        // Report the emitted signal:
        QString message = "|  ";
        int firstkey=0; double lastpitch=0;
        for (KeyData &key : mKeyDataVector)  if (key.pressed)
        {
            firstkey=key.key; lastpitch=key.pitch; break;
        }
        for (KeyData &key : mKeyDataVector)  if (key.pressed and key.key > firstkey)
        {
            message += QString::number(((int)(10*(key.pitch-lastpitch)))/10.0) + "  |  ";
            lastpitch = key.pitch;
        }
        if (message != mLastMessage)
        {
            emit signalIntervalString(message);
            mLastMessage = message;
        }
        if (getVerbosity() >= 4) if (message.size() > 0) { LOGSTATUS << message; }
        LOGSTATUS << "INTERVALS:" << message;
    }
}


//-----------------------------------------------------------------------------
//            Get current elapsed tuner runtime in milliseconds
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Private function: Get current elapsed tuner runtime in milliseconds
/// \return Time elapsed since thread intitialization in milliseconds
///////////////////////////////////////////////////////////////////////////////

qint64 Tuner::getNow()
{
    if (pElapsedTimer) return pElapsedTimer->elapsed();
    else return 0;
}


//-----------------------------------------------------------------------------
//              Public slot: Enable or disable static tuning mode
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Slot: Enable or disable static tuning mode
/// \param enable : True if static tuning mode is enabled, false otherwise
/// \param reference : Integer index (Midi) of the reference key (C=0)
///////////////////////////////////////////////////////////////////////////////

void Tuner::setStaticTuningMode (bool enable, int reference)
{
    LOGMESSAGE << "Setting static tuning =" << enable
               << "with reference key" << reference;
    mStaticTuningModeEnabled = enable;
    mStaticReferenceKey = reference%128;
}


//-----------------------------------------------------------------------------
//                 Set the essential parameters of the envelope
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the essential parameters of the envelope
/// \details The tuner is controlled by Midi commands. This means that the
/// tuner has no information about the type of the instrument. Since the
/// weight of a tone in the tuning process depends on its volume (a tone
/// that is not audible does not need to be tuned) one can use this function
/// to provide the tuner with a rough information about the envelope as a
/// function of time. When the key is pressed (sustain) the volume is assumed
/// to decay exponentially on a time scale varying linearly from the
/// first argument to the second argument along the keyboard. After releasing
/// the key, the volume is assumed to decay exponentially on a time scale
/// according to the third parameter. For an organ use very large sustain time.
/// For an imstrument with instant decay after release use secondsRelease=0.
/// For a piano typical parameters are 5 secs in the bass and 1 sec in the
/// treble with a decay time of 0.2 secs. These parameters are empirical and
/// it is advised to test various values.
/// \param secondsBass : Sustain exponential decay time scale of the lowest tone
/// \param secondsTreble : Sustain exponential decay time scale of the highest tone
/// \param secondsRelease : Release exponential decay time scale.
///////////////////////////////////////////////////////////////////////////////

void Tuner::setEnvelopeParameters (double secondsBass,
                            double secondsTreble,
                            double secondsRelease)
{
    LOGMESSAGE << "Envelope: Bass =" << secondsBass << " Treble ="
               << secondsTreble << " Release =" << secondsRelease;
    auto factor = [this] (double seconds) {
        return (seconds==0 ? 0 :  exp(-0.001*updateIntervalMsec/seconds)); };
    for (int key=0; key<128; key++)
    {
        double sedondsSustain = secondsBass +
                (108.0-key)/88.0*(secondsTreble-secondsBass);
        mKeyDataVector[key].sustainDampingFactor = factor(sedondsSustain);
        mKeyDataVector[key].releaseDampingFactor = factor(secondsRelease);
    }
}



//-----------------------------------------------------------------------------
//                   Initialization of the starting thread
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initialization of the starting thread
///////////////////////////////////////////////////////////////////////////////

void Tuner::initiallyCalledWorker()
{
    pElapsedTimer = new QElapsedTimer;
    QTimer::singleShot(1,this,&Tuner::tune);
}


//-----------------------------------------------------------------------------
//                     Cleanup of the terminating thread
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Cleanup of the terminating thread
///////////////////////////////////////////////////////////////////////////////

void Tuner::finallyCalledWorker()
{
    if (pElapsedTimer) delete pElapsedTimer;
    pElapsedTimer = nullptr;
}

//-----------------------------------------------------------------------------
//                   Update procedure triggered by the timer
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Periodically called worker function: Update key data
/// \details This function is frequently called to emulate the envelope (ADSR)
/// of the intensity I(t) and the psychoacoustic memory M(t).
///////////////////////////////////////////////////////////////////////////////

void Tuner::periodicallyCalledWorker()
{
    // Update the key data to emulate the envelope and memory
    for (KeyData &key : mKeyDataVector) if (key.memory > 0 or key.intensity > 0)
    {
        if (key.pressed)
        {
            key.memory += (1-memoryOnFactor)*(key.intensity-key.memory);
            key.intensity *= key.sustainDampingFactor;

            // If volume decreases below cutoff turn the key off
            if (key.intensity < cutoffIntensity or
                 getNow()-key.timeStamp > 1000*noteTimeoutSeconds)
                    key.pressed = false;
        }
        else // if the key has been released
        {
            key.memory += (1-mMemoryOffFactor)*(key.intensity-key.memory);
            key.intensity *= key.releaseDampingFactor;
            if (key.intensity < cutoffIntensity) key.intensity = 0;
            if (key.memory < cutoffMemory)       key.memory = 0;
        }
    }

    // Pitch drift compensation mechanism
    handlePitchDrift();
}


//-----------------------------------------------------------------------------
//                             Tune the pitches
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Tune the pitches of the pressed keys (call TunerAlgorithm)
/// \details This function tunes the pitches of the active keys.
/// It chooses the corresponding function in the TunerAlgorithm class
/// depending on the paremeters (ET, static tuning, and dynamical tuning)
///////////////////////////////////////////////////////////////////////////////

void Tuner::tune()
{
    if (isInterruptionRequested()) return;
    if ((--mWaitingTimeMsec) > 0)  // wait until mWaitingTimeMsec == 0
        { QTimer::singleShot(1,this,&Tuner::tune); return; }

    if (not mTuningEnabled or mTuningMode==0)
        for (auto &key : mKeyDataVector) key.pitch = 0;
    else if (mStaticTuningModeEnabled)
        mTunerAlgorithm.tuneStatically (mKeyDataVector,mIntervalSizes,
                                        mStaticReferenceKey,mWolfsIntervalShift);
    else
    {
        double tension = mTunerAlgorithm.tuneDynamically (mKeyDataVector,
                         mIntervalSizes,mIntervalWeights,mTuningMode==1);
        emit signalTension(exp(-0.01*std::abs(tension)));
    }
    emitPitchCorrections();

    mWaitingTimeMsec = tuningIntervalMsec; // Trigger next tuning event
    QTimer::singleShot(1,this,&Tuner::tune);
}


//-----------------------------------------------------------------------------
//                             Handle pitch drift
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief  Handle pitch drift
/// \details This function compensates the migration of the pitch. To this end
/// it first determines the average pitch of the keys with volume > 0.
/// Then it adiabatically corrects the pitch controlled by the
/// mPitchCorrectionParameter and finally sends the pitch deviation
/// to the circular gauge in the expert mode
///////////////////////////////////////////////////////////////////////////////

void Tuner::handlePitchDrift()
{
    if (not mTuningEnabled or mStaticTuningModeEnabled)
    {
        signalAveragePitchDrift(0);
        return;
    }
    // Determine arithmetic mean of pitch weighted by intensity
    double avsum = 0, norm = 0;
    for (KeyData &key : mKeyDataVector) if (key.intensity > 0)
        { avsum += key.pitch * key.intensity; norm += key.intensity; }
    if (norm > 0)
    {
        double averagePitch = avsum/norm;
        if (not mStaticTuningModeEnabled)
        {
            const double dP = - averagePitch * pow(mPitchAutoCorrectionParameter,2.5);
            for (KeyData &key : mKeyDataVector) key.pitch += dP;
            averagePitch += dP;
        }
        if (std::abs(averagePitch) < 1E-6) averagePitch=0;
        signalAveragePitchDrift(averagePitch);
    }
}


//-----------------------------------------------------------------------------
//                 Reset pitch progression to a given value
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Reset average pitch progression to a given value
/// \details In a dynamical tuning scheme the average pitch may begin to
/// drift away from zero. This function allows you to set the average pitch
/// progression to a given value by force.
///////////////////////////////////////////////////////////////////////////////

void Tuner::resetPitchProgression()
{
    LOGMESSAGE << "Resetting global pitch correction";
    double av=0,sum=0;
    for (KeyData &e : mKeyDataVector)
    {
        sum += e.intensity;
        av += e.intensity * e.pitch;
    }
    for (KeyData &e : mKeyDataVector)
    {
        if (e.intensity > 0 and sum > 0) e.pitch -= av/sum;
        else e.pitch=0;
    }
    signalAveragePitchDrift(0);
    emitPitchCorrections();
}
