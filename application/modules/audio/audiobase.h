/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//              Abstract base class for audio input and output
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \defgroup audio Audio interface
/// \brief Module for platform-independent audio input and output
/// based on Qt.
///////////////////////////////////////////////////////////////////////////////

#ifndef AUDIOBASE_H
#define AUDIOBASE_H

#include <QMutex>
#include <QAudioDeviceInfo>

#include "audioparameters.h"
#include "system/threadbase.h"

//=============================================================================
//                           Audio Base Class
//=============================================================================
///////////////////////////////////////////////////////////////////////////////
/// \ingroup audio
/// \brief Abstract base class for audio input and output
/// \details This class defines the basics properties of an audio input or output
/// interface. It manages the sampling rate, the number of used channels
/// and the name of the audio device. To work smoothly it runs in an independent
/// thread.
/// \see AudioOutput
///////////////////////////////////////////////////////////////////////////////

class AudioBase : public ThreadBase
{
    Q_OBJECT
public:
    AudioBase();                        ///< Constructor

    /// Get the actual audio device parameters
    const AudioParameters getActualDeviceParameters() const;
    /// Get the intended audio device parameters
    const AudioParameters getWantedDeviceParameters() const;
    /// Set the actual audio device parameters
    void setActualParameters (const AudioParameters &parameters);
    /// Set the wanted audio device parameters
    void setWantedParameters (const AudioParameters &parameters);

    /// Update the list of devices, called by the DeviceGuard
    virtual void updateListOfDevices() = 0;
    /// Get the current list of devices from the last update
    QStringList getListOfDevices() const { return mAudioDeviceNames; }

public slots:
    /// Connect/disconnect the audio device
    /// \param active : connect=true, disconnect=false
    virtual void connectDevice (const bool active) = 0;

signals:
    /// Signal indicating that a new audio device was plugged in (e.g. a USB headphone)
    void onAudioDevicesAdded(const QStringList &devices);
    /// Signal indicating that an audio device has been removed
    void onAudioDevicesRemoved(const QStringList &devices);
    /// Signal indicating that the list of available devices has been changed
    void onChangeOfAvailableAudioDevices(const QStringList &devices);
    /// Signal indicating that the actually used audio device has changed
    void onCurrentDeviceParametersChanged();
    /// Signal indicating that successful of failed connection
    void onConnectionSuccessfullyEstablished (bool success);

private:
    AudioParameters mWantedDeviceParameters;    ///< Structure holding the device parameters
    AudioParameters mActualDeviceParameters;    ///< Actual device parameters
    mutable QMutex mAudioParameterMutex;        ///< Access mutex
protected:
    QStringList mAudioDeviceNames;              ///< Current list of devices
};

#endif // AUDIOBASE_H
