/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//             Virtual base class for a sound-generating module
//=============================================================================

#include "audiogenerator.h"

#include <QDebug>
#include <cmath>

//#include "system/log.h"


//-----------------------------------------------------------------------------
//                                 Constructor
//-----------------------------------------------------------------------------

AudioGenerator::AudioGenerator()
    : mParameters()
    , mMaxNumberOfFrames(4096)
{}


//-----------------------------------------------------------------------------
//                              Set parameters
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Set audio device parameters
///////////////////////////////////////////////////////////////////////////////

void AudioGenerator::setParameters(const AudioParameters &parameters)
{
    if (mParameters==parameters) return;
    mParameters = parameters;
    const int minimal = 1;
    const int maximal = 1000000;
    if (mParameters.sampleRate < minimal)
    {
        LOGWARNING << "Sample rate is too small" << mParameters.sampleRate;
    }
    else if (mParameters.sampleRate > maximal)
    {
        LOGWARNING << "Sample rate is too large" << mParameters.sampleRate;
    }
    else
    {
        LOGMESSAGE << "Setting sample rate " << mParameters.sampleRate
                 << "buffersize = " << mParameters.bufferSize;
    }
}


//-----------------------------------------------------------------------------
//                    Virtual function for sound generation
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Virtual function for sound generation. This function generates
/// an A440 on the right and an A220 on the left channel as a test tone.
/// Override this function in the actual implementation.
///
/// This function generates the sound in form of a vector of
/// PCMType data packages. It is called internally by AudioOutputDevice.
/// Whenever the sound system is ready to read new data. This call is executed
/// in the local thread of the audio system.
///
/// The parameter maxSize specifies the maximal
/// number of PCMType entries that may be generated and equals the available
/// array size of data.
///
/// Each sample consists of two PCMType values (signed int from -32768 to +32767).
/// The implementation should write at most maxSize values (maxSize/2 samples)
/// to the data array and return the actual number of written PCMType values
/// (two times the number of samples).
///
/// By default this function generates an A440 sine wave for testing purposes.
///
/// \param data : Pointer to the buffer of bytes of the output device.
/// \param maxSize : Maximal number of bytes to be written.
/// \return Actual number of written PCMType values.
///////////////////////////////////////////////////////////////////////////////

size_t AudioGenerator::generateSound (char *data, size_t maxSize)
{
    static int t=0;
    const double pi = 3.141592655;
    quint16* buffer = (quint16*)data;

    if (mParameters.sampleSize==16)
    {
        if (maxSize<4) return 0;
        const size_t N=std::min(maxSize/4,mMaxNumberOfFrames);
        const double amplitude = 1024; // max 32768
        for (size_t i=0; i<N; i++)
        {
            buffer[2*i]=amplitude*sin(440.0*2*pi*t/mParameters.sampleRate);
            buffer[2*i+1]=amplitude*sin(220.0*2*pi*t/mParameters.sampleRate);
            t++;
        }
        return N*4; // return number of written PCMType values
    }
    else if (mParameters.sampleSize==24)
    {

        if (maxSize<6) return 0;
        const size_t N=std::min(maxSize/6,mMaxNumberOfFrames);
        const double amplitude = 1024*256; // max 32768
        for (size_t i=0; i<N; i++)
        {
            qint32 left=amplitude*sin(440.0*2*pi*t/mParameters.sampleRate);
            qint32 right=amplitude*sin(220.0*2*pi*t/mParameters.sampleRate);
            buffer[3*i]=static_cast<quint16>(left&0xFFFFU);
            buffer[3*i+1]=static_cast<quint16>(((left>>16)|(right<<16))&0xFFFFU);
            buffer[3*i+2]=static_cast<quint16>((right>>8)&0xFFFFU);
            t++;
        }
        return N*6; // return number of written PCMType values
    }
    else qDebug() << "AudioGenerator::generateSound: Sample size "
                  << mParameters.sampleSize << " is not yet implemented.";
    return 0;
}
