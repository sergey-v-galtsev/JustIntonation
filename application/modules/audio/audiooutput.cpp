/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                       Interface for audio output
//=============================================================================

#include "audiooutput.h"

//-----------------------------------------------------------------------------
//                  Constructor, resetting member variables
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Constructor
/// \details The constructor connects the audio output with a default
/// testtone generator.
///////////////////////////////////////////////////////////////////////////////

AudioOutput::AudioOutput()
    : AudioBase()
    , mAudioOutputDevice(this)
    , defaultGenerator()
    , pAudioGenerator(&defaultGenerator)
    , mAudioDeviceGuard(this)
    , mDefaultBufferSize(4096)
{
    setThreadName("AudioOutput");
}


//-----------------------------------------------------------------------------
//                            Init audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Init audio device
///////////////////////////////////////////////////////////////////////////////

bool AudioOutput::init()
{
    AudioBase::init();
    return true;
}


//-----------------------------------------------------------------------------
//                            Start audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start audio device
/// \details Start the audio device and connect with the last set of parameters
///////////////////////////////////////////////////////////////////////////////

bool AudioOutput::start()
{
    mAudioOutputDevice.setVerbosity(getVerbosity());
    mAudioDeviceGuard.setVerbosity(getVerbosity());
    mAudioDeviceGuard.startMonitoring();
    AudioBase::start(); // Starts the thread
    return true;
}

//-----------------------------------------------------------------------------
//            Initially called worker function in the new thread
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Initially called worker function
/// \details Start the audio device and connect with the last set of parameters
///////////////////////////////////////////////////////////////////////////////


void AudioOutput::initiallyCalledWorker()
{
    LOGMESSAGE << "Starting audio device with previous settings";
    AudioParameters parameters;
    parameters.deviceName=mSettings.value("audiooutput/devicename","default").toString();
    parameters.channelCount=mSettings.value("audiooutput/channelcount",2).toInt();
    parameters.bufferSize=mSettings.value("audiooutput/buffersize",mDefaultBufferSize).toInt();
    parameters.sampleRate=mSettings.value("audiooutput/samplerate",44100).toInt();
    parameters.active=mSettings.value("audiooutput/active",true).toBool();
    parameters.muted = false;
    setWantedParameters(parameters);
    connectDevice(true);
}

void AudioOutput::finallyCalledWorker()
{
    connectDevice(false);
}

//-----------------------------------------------------------------------------
//                            Stop audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Stop audio device
///////////////////////////////////////////////////////////////////////////////

bool AudioOutput::stop()
{
    mAudioDeviceGuard.stop();
    AudioBase::stop();
    return true;
}


//-----------------------------------------------------------------------------
//                            Exit audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Exit audio device
///////////////////////////////////////////////////////////////////////////////

bool AudioOutput::exit()
{
    stop();
    AudioBase::exit();
    return true;
}


//-----------------------------------------------------------------------------
//                            Suspend audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Suspend audio device
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::suspend()
{
    AudioBase::suspend();
    mAudioDeviceGuard.suspend();
}


//-----------------------------------------------------------------------------
//                            Resume audio device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Resume audio device
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::resume()
{
    AudioBase::resume();
    mAudioDeviceGuard.resume();
}


//-----------------------------------------------------------------------------
//                            Set audio generator
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Select an AudioGenerator for output
/// \details The AudioOutput can be connected with a sound-generating module
/// subclassed from AudioGenerator. This is usually the synthesizer or the
/// sampler in the application. Only one generator can be connected at a
/// given time. Call this function to change the generator.
/// \param generator : Reference to the AudioGenerator
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::setAudioGenerator (AudioGenerator &generator)
{
    // inform the generator (synthesizer) about the current parameters
    generator.setParameters(getActualDeviceParameters());
    // attach the gernator to the output
    pAudioGenerator = &generator;
}


//-----------------------------------------------------------------------------
//                            Set device parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set audio device parameters (thread safe)
/// \details Set the audio parameters for the device, inform the AudioGenerator
/// (the synthesizer) about the new parameter, and call a function that
/// updates the GUI.
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::setActualParameters (const AudioParameters &p)
{
    LOGMESSAGE << "Setting parameters for device " << p.deviceName;
    AudioBase::setActualParameters(p);
    pAudioGenerator->setParameters(p);
    emit onCurrentDeviceParametersChanged();
}


//-----------------------------------------------------------------------------
//                             Connect the device
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Connect the audio output to a device (e.g. soundcard) with a given
/// set of device parameters.
/// \details The device parameters will be stored as mWantedDeviceParameters so
/// that this information is available when entering the worker function run().
/// Moreover, the audio device thread is started with high priority.
/// \param active : connect=true, disconnect=false
/// \note This function does not return a boolean, indicating the success
/// of the connection attempt, because the connection is running in an
/// independent thread.
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::connectDevice (const bool active)
{
    if (active)
    {
        AudioParameters actual = getActualDeviceParameters();
        AudioParameters wanted = getWantedDeviceParameters();
        if (actual.active)
        {
            LOGMESSAGE << "First disconnect audio device";
            connectDevice(false);
        }
        LOGMESSAGE << "Connect device " << wanted.deviceName;
        if (mAudioOutputDevice.connect(wanted))
        {
            // At this point the audio output device has alread
            // the actual parameters
            LOGMESSAGE << "Audio device successfully connected";
            actual = getActualDeviceParameters();
            mSettings.setValue("audiooutput/active",actual.active);
            mSettings.setValue("audiooutput/devicename",actual.deviceName);
            mSettings.setValue("audiooutput/channelcount",actual.channelCount);
            mSettings.setValue("audiooutput/buffersize",actual.bufferSize);
            mSettings.setValue("audiooutput/samplerate",actual.sampleRate);
            emit onConnectionSuccessfullyEstablished (true);
        }
        else
        {
            LOGWARNING << "AudioOutputDevice could not be connected, aborting.";
            setActualParameters(AudioParameters());
            emit onConnectionSuccessfullyEstablished (false);
        }
    }
    else
    {
        LOGMESSAGE << "Disconnect current device";
        mAudioOutputDevice.disconnect();
    }
}


//-----------------------------------------------------------------------------
//                        Update list of devices
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Update the list of devices
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::updateListOfDevices()
{
    LOGSTATUS << "Update list of devices";
    QList<QAudioDeviceInfo> infoAboutDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    QStringList currentDeviceNames;
    for (const QAudioDeviceInfo &dev : infoAboutDevices)
    {
        QString deviceName = dev.deviceName();
        // Suppress direct hardware entries in Linux ALSA
        if (not deviceName.contains(":CARD="))
        {
            currentDeviceNames.append(deviceName);
        }
    }
    LOGSTATUS << currentDeviceNames;

    if (currentDeviceNames != mAudioDeviceNames)
    {
        LOGMESSAGE << "Recognized a change in the list of audio devices";
        LOGSTATUS << "The current device names are:" << currentDeviceNames;
        // Compare the old and the new device list
        QStringList newDevices,removedDevices;
        for (const QString &entry : currentDeviceNames)
            if (mAudioDeviceNames.indexOf(entry)<0) newDevices.append(entry);
        for (const QString &entry : mAudioDeviceNames)
            if (currentDeviceNames.indexOf(entry)<0) removedDevices.append(entry);

        // emit the corresponding signals
        if (newDevices.size()>0 and mAudioDeviceNames.size()>=0)
            emit onAudioDevicesAdded(newDevices);
        if (removedDevices.size()>0)
            emit onAudioDevicesRemoved(removedDevices);

        mAudioDeviceNames = currentDeviceNames;
        emit onChangeOfAvailableAudioDevices(mAudioDeviceNames);
    }
}


//-----------------------------------------------------------------------------
//                             Set global volume
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set the global volume of the audio device
/// \param volume : Volume between 0 and 1
///////////////////////////////////////////////////////////////////////////////

void AudioOutput::setVolume(double volume)
{
    LOGMESSAGE << "Setting volume to" << volume;
    mAudioOutputDevice.setVolume(volume);
}


