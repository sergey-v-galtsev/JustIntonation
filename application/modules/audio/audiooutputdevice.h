/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                           Audio output device
//=============================================================================

#ifndef AUDIOOUPUTDEVICE_H
#define AUDIOOUPUTDEVICE_H

#include <QtMultimedia/QAudioOutput>

#include "audiobase.h"

class AudioOutput;

///////////////////////////////////////////////////////////////////////////////
/// \ingroup audio
/// \brief Audio Output Device, supporting 16 and 24 bit signed integer PCM.
/// \details This is the link between the audio classes of the module and
/// the Qt audio system, inheriting QIODevice.
///////////////////////////////////////////////////////////////////////////////

class AudioOutputDevice : public QIODevice, public Log
{
public:
    AudioOutputDevice (AudioOutput* audiooutput);

    bool connect (const AudioParameters &parameters);
    void disconnect();

    void setVolume(double volume);

private:
    qint64 readData  (char * data, qint64 maxSize) override final;
    qint64 writeData (const char * data, qint64 maxSize) override final;

    int mSampleSize;                        ///< Sample size in bits (16/24)
    AudioOutput*  pAudioOutput;             ///< Pointer back to AudioOutput
    QAudioOutput* pQtAudioOutputStream;     ///< Pointer to System-Qt audio driver
};

#endif // AUDIOOUPUTDEVICE_H
