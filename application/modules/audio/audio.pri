SOURCES += \
    $$PWD/audiobase.cpp \
    $$PWD/audiodeviceguard.cpp \
    $$PWD/audiogenerator.cpp \
    $$PWD/audiooutput.cpp \
    $$PWD/audiooutputdevice.cpp \
    $$PWD/audioparameters.cpp

HEADERS += \
    $$PWD/audiobase.h \
    $$PWD/audiodeviceguard.h \
    $$PWD/audiogenerator.h \
    $$PWD/audiooutput.h \
    $$PWD/audiooutputdevice.h \
    $$PWD/audioparameters.h
