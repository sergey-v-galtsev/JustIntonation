/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                       Interface for audio output
//=============================================================================

#ifndef AUDIOOUTPUT_H
#define AUDIOOUTPUT_H

#include <QObject>
#include <QSettings>

#include "audiobase.h"
#include "audiooutputdevice.h"
#include "audiogenerator.h"

////////////////////////////////////////////////////////////////////////////////
/// \ingroup audio
/// \brief Class for audio output.
/// \details This is the principal class providing the user interface for
/// audio output. It is derived from AudioBase which holds the device parameters
/// and instantiates a private AudioOutputThread. The interface is activated
/// by calling connect with the wanted parameters. Moreover, the class allows
/// the user to connect an AudioGenerator (a sound-generating unit such as
/// a synthesizer), passing this request to the AudioOutputThread.
////////////////////////////////////////////////////////////////////////////////

class AudioOutput : public AudioBase
{
public:
    AudioOutput ();
    virtual bool init() override;
    virtual bool start() override;

    virtual bool stop() override;
    virtual bool exit() override;

    virtual void suspend() override;
    virtual void resume() override;

    virtual void initiallyCalledWorker() override final;
    virtual void finallyCalledWorker() override final;

    void setAudioGenerator (AudioGenerator &generator);
    AudioGenerator* getAudioGenerator() const { return pAudioGenerator; }

    void updateListOfDevices() override final;

    void setVolume (double volume);
    void setActualParameters (const AudioParameters &p);
    void setDefaultBufferSize (int n) { mDefaultBufferSize=n; }

public slots:
    void connectDevice (const bool active) override final;

private:
    AudioOutputDevice mAudioOutputDevice;       ///< Instance of the device
    AudioGenerator defaultGenerator;            ///< Default sine generator
    AudioGenerator* pAudioGenerator;            ///< Pointer to synthesizer
    AudioDeviceGuard mAudioDeviceGuard;         ///< Audio device guard
    QSettings mSettings;                        ///< Settings object

protected:
    int mDefaultBufferSize;                     ///< Default audio buffer size
};

#endif // AUDIOOUTPUT_H
