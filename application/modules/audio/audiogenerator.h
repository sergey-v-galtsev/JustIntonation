/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef AUDIOGENERATOR_H
#define AUDIOGENERATOR_H

#include "audiobase.h"
#include "audiodeviceguard.h"

//=============================================================================
//              Virtual base class for a sound-generating module
//=============================================================================

///////////////////////////////////////////////////////////////////////////////
/// \brief Virtual base class for audio-generating modules
/// \ingroup audio
/// \details This virtual base class has to be inherited by the actual
/// sound-producing implementation such as a synthesizer or audio player.
///
/// The derived class should override the virtual function
/// AudioGenerator::generateSound. If this function is not overridden it
/// is implemented here a simple generator producing a
/// 440 Hz sine wave for testing purposes.
///////////////////////////////////////////////////////////////////////////////

class AudioGenerator : public Log
{
public:
    AudioGenerator();                       ///< Constructor
    ~AudioGenerator(){}                     ///< Destructor without functionality

    void setParameters (const AudioParameters &parameters);
    void setMaxPacketSize (int n) { mMaxNumberOfFrames = n; }
    size_t getMaxPacketSize() { return mMaxNumberOfFrames; }

    // -------- Reimplement this function in the derived class: ----------
    virtual size_t generateSound (char* data, size_t maxSize);

    int getSampleRate() { return mParameters.sampleRate; }

protected:
    AudioParameters mParameters;  ///< Local copy of the audio parameters
    size_t mMaxNumberOfFrames;          ///< Maximal number of frames per buffer
};

#endif // AUDIOGENERATOR_H
