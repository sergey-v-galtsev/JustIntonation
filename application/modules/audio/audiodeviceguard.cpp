/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                            Audio Device Guard
//=============================================================================

#include "audiodeviceguard.h"

//-----------------------------------------------------------------------------
//                             Constructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Construtor of the AudioDeviceGuard
/// \param audiobase : Pointer back to the AudioBase object (input or output)
///////////////////////////////////////////////////////////////////////////////

AudioDeviceGuard::AudioDeviceGuard (AudioBase *audiobase)
    : pAudioBase(audiobase)
{
    LOGSTATUS << "Creating AudioDeviceGuard";
    setModuleName("AudioDevGuard");
    setThreadName("AudioDevGuard");
}


//-----------------------------------------------------------------------------
//                              Destructor
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Destructor: Stop the timer, stop the thread and exit
///////////////////////////////////////////////////////////////////////////////

AudioDeviceGuard::~AudioDeviceGuard()
{
    LOGSTATUS << "Destroying AudioDeviceGuard";
    stop();
}


//-----------------------------------------------------------------------------
//                      Start the AudioDeviceGuard
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Start the AudioDeviceGuard
/// \param seconds : Time interval in seconds for the update of the device list.
/// \return true
///////////////////////////////////////////////////////////////////////////////

bool AudioDeviceGuard::startMonitoring (int seconds)
{
    LOGSTATUS << "Starting AudioDeviceGuard";
    setTimerInterval(1000*seconds,2000);
    setPriority(QThread::IdlePriority);
    return ThreadBase::start();
}


//-----------------------------------------------------------------------------
//                          Thread worker function
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Thread worker function, calling updateListOfDevices() of the audio
/// object. This thread is executed at lowest possible priority.
///////////////////////////////////////////////////////////////////////////////

void AudioDeviceGuard::periodicallyCalledWorker()
{
    if (pAudioBase) pAudioBase->updateListOfDevices();
}
