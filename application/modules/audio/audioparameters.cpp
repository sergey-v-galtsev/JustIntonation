/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//        Structure holding the parameters and status of an audio device
//=============================================================================

#include "audioparameters.h"

//-----------------------------------------------------------------------------
//                         Compare audio parameters
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Compare two sets of audio parameters
/// \param p : Right-hand-side argument of the == operator (the left argument
/// is the object attached to the function itself.
/// \return True if equal in the sense of the definition below.
///////////////////////////////////////////////////////////////////////////////

bool AudioParameters::operator==(const AudioParameters &p) const
{
    return  deviceName == p.deviceName and
            channelCount == p.channelCount and
            sampleSize == p.sampleSize and
            bufferSize == p.bufferSize and
            sampleRate == p.sampleRate and
            supportedSampleRates == p.supportedSampleRates and
            active == p.active;
}


//-----------------------------------------------------------------------------
//                  Default constructor for audio paramters
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// \brief Default constructor for void audio parameters.
///////////////////////////////////////////////////////////////////////////////

AudioParameters::AudioParameters()
    : deviceName("")
    , channelCount(0)
    , sampleSize(16)
    , bufferSize(0)
    , sampleRate(0)
    , supportedSampleRates()
    , active(false)
    , muted(false)
{}





