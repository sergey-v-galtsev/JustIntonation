/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                             Audio base class
//=============================================================================

#include "audiobase.h"

//-----------------------------------------------------------------------------
//                               Constructor
//-----------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////
/// Constructor, resetting member variables to default values
///////////////////////////////////////////////////////////////////////////////

AudioBase::AudioBase ()
    : mWantedDeviceParameters()
    , mActualDeviceParameters()
    , mAudioParameterMutex()
    , mAudioDeviceNames()
{
    setModuleName("Audio");
    setPriority(QThread::HighPriority);

    // Thread name is set in the derived class
}


//-----------------------------------------------------------------------------
//                       Get actual device parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get the actual audio device parameters (thread safe)
/// \return Copy of the audio device parameters
///////////////////////////////////////////////////////////////////////////////

const AudioParameters AudioBase::getActualDeviceParameters() const
{
    QMutexLocker lock(&mAudioParameterMutex);
    return mActualDeviceParameters;
}


//-----------------------------------------------------------------------------
//                     Get the wanted device parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Get the wanted audio device parameters (thread safe)
/// \return Copy of the audio device parameters
///////////////////////////////////////////////////////////////////////////////

const AudioParameters AudioBase::getWantedDeviceParameters() const
{
    QMutexLocker lock(&mAudioParameterMutex);
    return mWantedDeviceParameters;
}


//-----------------------------------------------------------------------------
//                             Set actual parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set actual audio device parameters (thread safe)
/// \param parameters : Reference to the device parameters
///////////////////////////////////////////////////////////////////////////////

void AudioBase::setActualParameters (const AudioParameters &parameters)
{
    QMutexLocker lock(&mAudioParameterMutex);
    mActualDeviceParameters = parameters;
}


//-----------------------------------------------------------------------------
//                             Set wanted parameters
//-----------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
/// \brief Set wanted audio device parameters (thread safe)
/// \param parameters : Reference to the device parameters
///////////////////////////////////////////////////////////////////////////////

void AudioBase::setWantedParameters (const AudioParameters &parameters)
{
    QMutexLocker lock(&mAudioParameterMutex);
    mWantedDeviceParameters = parameters;
}


