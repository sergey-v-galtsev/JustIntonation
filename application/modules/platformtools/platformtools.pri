# Platform-specific tools written in C++

HEADERS += $$PWD/platformtools.h

################################################

android {
}

################################################

ios {
    HEADERS += $$PWD/ios/iosplatformtools.h\
               $$PWD/ios/iosnativewrapper.h

    SOURCES += $$PWD/ios/iosplatformtools.cpp

    OBJECTIVE_SOURCES += $$PWD/ios/iosnativewrapper.mm
}

################################################

macx {
}

################################################

win32 {
}

################################################

winrt {
# TODO The winrtplatformtools in the present form produce an UWP error message
# saying that the application is NOT instanciated in the main trhread.
# Therefore, we commented them out. It's not clear whether we need them at all.

#    HEADERS += $$PWD/winrt/winrtplatformtools.h
#    SOURCES += $$PWD/winrt/winrtplatformtools.cpp
}
