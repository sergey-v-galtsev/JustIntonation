/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

/*****************************************************************************
 * Copyright 2016-2017 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//============================================================================
//                  Platform-specific tools for IOS
//============================================================================


#include "iosplatformtools.h"
#include "iosnativewrapper.h"

#include <QDebug>

PlatformToolsInstanciator<IOSPlatformTools> IOSPlatformToolsInstance;

IOSPlatformTools::IOSPlatformTools()
{
    qDebug() << "IOS PLATFORM TOOLS WERE INSTANCIATED";
}



void IOSPlatformTools::disableScreensaver() {
    iosDisableScreensaver();
}

void IOSPlatformTools::enableScreensaver() {
    iosReleaseScreensaverLock();
}

