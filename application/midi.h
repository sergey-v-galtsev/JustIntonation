/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//       Midi wrapper class, connecting QtMidi with JustIntonation
//=============================================================================

#ifndef MIDI_H
#define MIDI_H

#include <QObject>
#include <QtMidi/QMidiInput>
#include <QtMidi/QMidiOutput>
#include <QtMidi/QMidiDeviceInfo>
#include <QMidiAutoConnector>

#include "system/log.h"

///////////////////////////////////////////////////////////////////////////////
/// \brief Class handling Midi input and output
///////////////////////////////////////////////////////////////////////////////

class Midi : public QObject, public Log
{
    Q_OBJECT
public:
    explicit Midi();
    ~Midi(){}

    bool init (QObject* qml);

public slots:
    void sendMidiMessage (const QMidiMessage &event);

signals:
    void receivedMidiMessage (const QMidiMessage &event);

private slots: // for signals coming from Qml
    void selectInputDevice (QString deviceName);
    void selectOutputDevice (QString deviceName);
    void setAutomaticInputMode (bool autoconnect);
    void setAutomaticOutputMode (bool autoconnect);

signals: // signals sent to Qml
    void onInputDevicesChanged (QVariant devices);
    void onOutputDevicesChanged (QVariant devices);
    void onCurrentInputDeviceChanged (QVariant device);
    void onCurrentOutputDeviceChanged (QVariant device);

signals: // for resetting sound-generating modules
    void onStatusChanged();

private slots: // for signals coming from QMidi
    void inputDeviceAttached (const QMidiDeviceInfo info);
    void outputDeviceAttached (const QMidiDeviceInfo info);
    void inputDeviceDetached (const QMidiDeviceInfo info);
    void outputDeviceDetached (const QMidiDeviceInfo info);

    void inputDeviceCreated (const QMidiInput* device);
    void outputDeviceCreated (const QMidiOutput* device);
    void inputDeviceDeleted (QMidiDeviceInfo info);
    void outputDeviceDeleted (QMidiDeviceInfo info);

    void acInputDeviceCreated(const QMidiDevice* dev);
    void acOutputDeviceCreated(const QMidiDevice* dev);
    void acDeviceDeleted(const QMidiDeviceInfo info, QMidi::Mode mode);

    void receiveMessage(const QMidiMessage &m);
signals:
    void sendMidiEventToOutputDevice (const QMidiMessage &event);


private:
    QMidiAutoConnector* pConnector;

    void disconnectInput();
    void disconnectOutput();
    void updateAvailableDevices (QMidi::Mode mode);

    QMidiInput* pCurrentInputDevice;
    QMidiOutput* pCurrentOutputDevice;
};

#endif // MIDI_H
