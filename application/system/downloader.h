/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                               Downloader
//=============================================================================

#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QObject>
#include <QByteArray>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QVariant>
#include <QFile>

#include "system/shared.h"
#include "system/log.h"

///////////////////////////////////////////////////////////////////////////////
/// \ingroup system
/// \brief Class managing the download of large files from a repository
/// in the background.
/// \details This module download files from a remote location (http server)
/// to the local applicaition directory. The files that are downloaded are
/// listed in an index file (index.txt) on the server. The downloading process
/// is singal-slot-driven and is carried out silently in the background.
///////////////////////////////////////////////////////////////////////////////

class Downloader : public QObject , public Log
{
    Q_OBJECT
public:

    explicit Downloader(QString url, QObject *parent = 0);
    virtual ~Downloader() {}

    void start(bool forced);
    void stop();
    bool isDownloading() { return mIsDownloading; }

    QStringList getPathsOfDownloadedFiles();

signals:
    void signalNoInternetConnection (QVariant forced);
    void signalDownloading (QVariant downloading);
    void signalProgress (QVariant filesRemaining, QVariant percent);
    void signalNewFileDownloaded (QString localpath);
    void signalAllFilesDownloaded();

private slots:
    void downloadIndexFile();
    void indexFileDownloadFinished (QNetworkReply* pReply);
    void initiateDownloadOfNextFile();
    void checkWhetherFileExistsAndDownload();
    void downloadProgress (qint64 bytesReceived, qint64 bytesTotal);
    void downloadComplete (QNetworkReply* pReply);
    void finalize();

private:
    QString fullPath (const QString &path, const QString &file) const;
    bool isConnectedToInternet();

private:
    QString mLocalPath;                     ///< Local path of the file to be downloaded
    QString mRemotePath;                    ///< Remote path of the file to be downloaded
    QString mIndexFileName;                 ///< Name of the remote index file
    QNetworkAccessManager mNetworkManager;  ///< Instance of the Qt network access manager
    QStringList mIndexOfFiles;              ///< The content of the index file as a QStringList
    QNetworkReply *pNetworkReply;           ///< Pointer to the network reply structure
    QFile mFile;                            ///< File to be written
    bool mIsWaitingForConnection;           ///< Flag indicating waiting status
    bool mForcedDownload;                   ///< Flag for forced download
    bool mIsDownloading;                    ///< Flag indicating active download
};

#endif // DOWNLOADER_H
