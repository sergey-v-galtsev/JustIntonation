iOS:
====

The conversion of png files did not work after the Xcode update.
As a solution we modified the file copying in the Xcode application,
replacing line 18 (xcrun -f pngcrush) by the hard-coded path
my $PNGCRUSH = "/Applications/Xcode.app/Contents/Developer/usr/bin/pngcrush";

