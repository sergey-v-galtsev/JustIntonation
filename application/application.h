/*****************************************************************************
 * Copyright 2016-2018 Karolin Stange, Christoph Wick, and Haye Hinrichsen
 *
 * This file is part of JustIntonation.
 *
 * JustIntonation is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * JustIntonation is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with JustIntonation. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                            Application Class
//=============================================================================

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QSettings>
#include <QTranslator>

#include "system/downloader.h"
#include "instrument/instrument.h"
#include "audio/audiooutput.h"
#include "sampler/soundgenerator.h"
#include "sampler/midihandler.h"
#include "midiplayer/midiplayer.h"
#include "midi.h"
#include "midimicrotone/midimicrotonal.h"
#include "audio/audiooutput.h"
#include "tuner/tuner.h"
#include "system/logfile.h"
#include "touchscreenkeyboard.h"
#include "system/filehandler.h"
#include "platformtools/platformtools.h"

extern void mobileError(QString msg);

///////////////////////////////////////////////////////////////////////////////
/// \brief Main application
/// \details This class represents the main application which is the base
/// QObject in the code. It manages the flow of init-start-supend-resume-stop-exit
/// as well as certain system-related features. Moreover, it holds the
/// instances of various modules. All signal-slot connections are channeled
/// through this class.
///////////////////////////////////////////////////////////////////////////////

class Application : public QObject, public Log
{
    Q_OBJECT
public:
    Application (QApplication &app);
    void init (QQmlApplicationEngine &engine);
    void start();
    void stop();
    void exit();

    void suspend();
    void resume();

public:
    Downloader* getDownloader() { return &mDownloader; }
    bool loadMidiFile (QString filename);

    void mobileMessage(QString msg);

public slots:
    // Discover feedback loop
    void onClosedMidiLoopDetected();
    void onInputOutputStatusChanged();

private slots:
    // System
    void handleApplicationStates(Qt::ApplicationState state);

    // Internet download
    void cancelLoading();
    void loadingFinished (bool success);
    void loadInstrument (int selectedindex);
    void newInstrumentDownloaded (QString localpath);
    void allInstrumentsDownloaded ();
    void startDownload (bool forced=false);

    // Audio
    void selectAudioParameters (int devIndex, int srIndex,
                                int buffersize, int packetsize);
    void resetAudioParameters();
    void onAudioDevicesAdded(QStringList list);
    void onAudioDevicesRemoved(QStringList list);
    void onChangeOfAvailableAudioDevices(QStringList list);
    void onCurrentDeviceParametersChanged();
    void onConnectionSuccessfullyEstablished(bool success);

    // Various
    void hearExample(int number);
    void emitAveragePitchProgression (double pitchProgression);
    void disableScreenSaver() { PlatformTools::getSingleton().disableScreensaver(); }
    void enableScreenSaver()  { PlatformTools::getSingleton().enableScreensaver(); }
    void updateInstruments() { updateShownInstrumentNames(); }

signals:
    // System
    void setQmlLocale(QVariant string);
    void showDownloadDialog();
    void getSettingsFromQml();
    void sendMobileMessage(QVariant string);

    // Instrument selection
    void setQmlInstrumentNames (QVariant stringlist);
    void setQmlSelectedInstrument (QVariant index);
    void startLoadingInstrument (QString name);

    // Audio
    void setAudioDeviceName (QVariant name);
    void setAudioDeviceList (QVariant list, QVariant index);
    void setAudioSampleRates (QVariant list, QVariant index);
    void setAudioBufferSizes (QVariant buffer, QVariant packet);
    void connectAudioDevice (bool active);

    // Tuning
    void signalCircularGauge (QVariant progression);

    // Various
    void setSelectedExample (QVariant number);

private:
    void setInstrumentSelector (int index=0);
    QStringList updateShownInstrumentNames();
    QString shorten(const QString &device, int truncation=30) const;

private:
    // System
    QApplication* pApplication;
    bool mStarted;
    QSettings mSettings;
    QString mLocale;
    Downloader mDownloader;
    FileHandler mFileHandler;

    MidiPlayer mMidiPlayer;
    Midi mMidi;
    MidiMicrotonal mMidiMicrotonalConverter;
    AudioOutput mAudioOutput;
    int mNumberOfExample;
    Instrument mInstrument;
    SoundGenerator mSoundGenerator;
    MidiHandler mMidiHandler;
    Tuner mTuner;
    LogFile mTuningLog;
    TouchscreenKeyboard mKeyboard;
    QStringList mAvailableMidiDevices;
    QStringList mAvailableMidiOutputDevices;
    QString mCurrentMidiDevice;
    bool mMidiAutomaticRecognition;
    bool mMidiPlayerWasPlaying;
    bool mSuspended;
    double mLastEmittedPitchProgression;
};

#endif // APPLICATION_H
