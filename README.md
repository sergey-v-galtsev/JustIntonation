<center>
# Just Intonation
### Karolin Stange, Christoph Wick, and Haye Hinrichsen 
Institute for Physics and Astronomy <br>
Department of Theoretical Physics III<br>
University of Würzburg<br> D-97074 Würzburg, Germany
</center><br><center>


| Resources:     | Link:  |
| :-------------:| :----: |
|Project Website | [justintonation.tp3.app](http://justintonation.tp3.app) |
|Source Code     | [sourcecode.just-intonation.org](http://sourcecode.just-intonation.org) |
|Documentation   | [doxygen.just-intonation.org](http://doxygen.just-intonation.org) |
|E-Mail          | [info at just-intonation.org](mailto:info@just-intonation.org) |

<br>

</center>
## JustIntonation is an open-source application for various platforms that allows the user to hear and play music in just intonation and other temperaments, independent of the musical key.

### The problem
The Western musical scale is based on the Equal Temperament (ET),
which is defined by constant semitone ratios of 2^(1/12). As a
geometrically organized temperament the ET has the great advantage
to be scale-invariant, i.e., the interval frequency ratios are the
same in all scales, no matter whether you play music e.g. in C-major
or D-major.

The problem is that all intervals except for the ocatave are characterized
by irrational frequency ratios. However, because of the ubiquitious 
harmonic series of higher partials in the spectra of musical instruments, 
which are characterized by integer multiples of the ground frequencies,
intervals are perceived as well-tuned if they exhibit a rational frequency
ratios. This means that in the ET all intervals except for the octave are
more or less out of tune. Over the last two centuries, however, humanity
got used to these discrepancies.

### Static unequal temperaments
With the increasing awareness of harmony and the renewed focus on 
historical performance practices there is currently a great interest
in so-called unequal temperaments. Historically there has been a 
fascinating development of musical temperaments, ranging from 
Pytharorean tuning over Meantone temperaments to the ET, to name only
a few. All these temperaments represent static tuning schemes, reconciling
the incompatibility of perfect harmony and scale invaraince to a different 
extent. The two extremes in this spectrum are on the one hand the 
Western ET (perfect scale invariance, lowest harmony) and the so-called
just intonation, which has the highest degree of harmony but only in a
single reference scale.

### Dynamical tuning
To overcome the problem of the broken scale symmetry in static just 
intonation, a possible way out would be to tune dynamically, that is,
each tone is tune instantly when the key of the instrument is hit. This
is of course impossible with traditional mechanical instruments but it
is very easy with electronic oscillators. The idea is to recognize the
scale just in time and to tune the current chord in just intonation.

### This project
In the past there has been a large variety of attempts to implement 
dynamical tuning schemes, most of them on a proprietary basis and
adapted to the limited computer power in old times. Here we propose 
a new method as an open-source project that could be a basis for
further development. This application allows one to test the method
and to experience how it would be to hear Music in just intonation.

### What is new
Whenever a key is pressed (or a Midi signal is received) the 
application sets up a linear system of equations to work out the
optimal tuning. This system is instantly solved using the "eigen"
library. The application comes with an inbuilt microtunal sampler
that allows you to hear real instruments in just intonation. 
Our method has the great advantage that it does not only tune
simple chords perfectly but it also finds reasonable compromises
for self-contradicting chords such as the diminished Seventh
that cannot be tuned consistently in just intonation.


## Compilation
JustIntonation is a multi-platform application based on Qt. It depends on the QMidi library developed by Christoph Wick. It includes the linear algebra package "Eigen" in the thirdparty folder.
### Getting started on Windows
- Download and install the community version of VisualC++ 2017
-- In the installer select UWP-C++ development and C++-Desktop development.
-- After installation start start VisualC++ and check whether it works
--- Register yourself 
--- Make sure that you can compile and execute a UWP-C++ sample project (not C#). The system may ask for installing UWP-tools for C++.
- Download and install git for Windows from [here](https://git-scm.com/download/win)
- Download and install Strawberry Perl from [here](http://strawberryperl.com/)
- Download and install Cmake from [here](https://cmake.org/download/)
- Download and install Python from [here](https://www.python.org/downloads/windows/)
- Download and install the open source version of Qt from [qt.io](https://www.qt.io/)
-- In the Qt installer select the components Qt 5.9: 
--- MinGW
--- UWP x64 (MSVC2017)
--- msvc2017 64-bit
--- You may deselect Qt-charts...Qt-scripts
-- In the Qt installer  select the components Tools: 
--- QtCreator
--- Qt Installer framework
-- If Qt is already install run the Maintenance tool in order to check whether the above components are installed.
- Start Qt, go to Extras-Settings, choose Build-and-Run, and check the Kits
- Download and build QtMidi with Qt:
-- Start git bash and go to your favorite working directory
-- Type `git clone http:/gitlab.com/tp3/qtmidi`
-- Method 1: Execute the Python script build.py
-- Method 2: Open qtmidi.pro in QtCreator, add "make install", and build


### General preparations
Before starting one needs to download and install Qt:
- Download and install the open source version of Qt and Qt Installer Framework from [qt.io](https://www.qt.io/)
-- For intallation select the components Desktop-GCC, Android-ARM, QtCreator and Installer Framework
- 
### Linux
To compile the code on a Linux machine proceed as follows:
- Make sure that the packages git, g++, cmake, libgl1-mesa-dev and libasound2-dev are installed.
- Download the open-source version of Qt and Qt Installer Framework  from https://www.qt.io/
- Make Qt ready for Android
    - Install openjdk-devel
    - Get JDK installation path with sudo update-alternatives --config javac
    - Add JDK installation path (e.g. /usr/lib64/jvm/java-1.8.0-openjdk) to Qt-Settings-Devices
- Get the qtmidi package from https://gitlab.com/tp3/qtmidi
- Open qtmidi project, add "make install" in the build settings
- Compile qtmidi, this will install the qtmidi libraries in Qt
  (alternatively you may run a pearl script, as explained in the readme of qtmidi)
- Compile JustIntonation, it should run out of the box.


### MAC OSX / iOS
- Compile source
    - Get QtMidi: clone https://gitlab.com/tp3/qtmidi
    - Get Just Intonation: clone https://gitlab.com/tp3/justintonation
    - Configure QtMidi in QtCreator, add "make install" (see QtMidi instructions)
    - Build QtMidi in Release mode, then check that QtMidi.framework in generated in the Qt/5.x/lib folder
    - Build and run JustIntonation. If there are no errors the application should start out of the box.
- If the compilation fails because of the wrong XCode version please proceed as follows:
    - Add the following line to Qt/5.x/clang_64/mkspecs/qdevice.pri (replace version number)
    - `!host_build:QMAKE_MAC_SDK = macosx10.12`
- Repeat the same steps for iOS

## Structure of the software


### Modularity
The application folder contains the following subfolders:
- instrument: Parts of the code which define a musical instrument (Piano, Organ, Harpsichord etc.), holding the sampled data.
- sampler: Parts of the code responsable for real-time sampling and sound generation.
- platforms: Platform-dependent additions (Android etc)
- resources: Wave file, QML files, images and icons
- system: Various system-related parts of the software
- modules: Various parts of the software which could also be compiled separately, having a well-defined meaning even without the reset of the code.

To see all modules follow <a href="modules.html"> this link</a>

### Tuner module
The essential module is te 'Tuner'. This module is connected to the application exclusively via Qt signals and slots. This allows you
to use the tuner in different projects very easily. It also
makes it easier to understand how it works.

